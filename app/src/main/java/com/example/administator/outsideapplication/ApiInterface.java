package com.example.administator.outsideapplication;

import com.example.administator.outsideapplication.model.AboutResponse;
import com.example.administator.outsideapplication.model.AddAddressResponse;
import com.example.administator.outsideapplication.model.AddCartResponse;
import com.example.administator.outsideapplication.model.AddressBookResponse;
import com.example.administator.outsideapplication.model.AreaResponse;
import com.example.administator.outsideapplication.model.CancelOrderResponse;
import com.example.administator.outsideapplication.model.CartCountResponse;
import com.example.administator.outsideapplication.model.CartProductDeleteResponse;
import com.example.administator.outsideapplication.model.CartResponse;
import com.example.administator.outsideapplication.model.ChangePasswordResponse;
import com.example.administator.outsideapplication.model.CityResponse;
import com.example.administator.outsideapplication.model.ContactUsResponse;
import com.example.administator.outsideapplication.model.DeleteAddressReponse;
import com.example.administator.outsideapplication.model.EditAddressResponse;
import com.example.administator.outsideapplication.model.EditProfileResponse;
import com.example.administator.outsideapplication.model.ForgotOtpResponse;
import com.example.administator.outsideapplication.model.ForgotPasswordResponse;
import com.example.administator.outsideapplication.model.HomeResponse;
import com.example.administator.outsideapplication.model.LoginResponse;
import com.example.administator.outsideapplication.model.MyOrdereResponse;
import com.example.administator.outsideapplication.model.OrderDetailsResponse;
import com.example.administator.outsideapplication.model.PaymentResponse;
import com.example.administator.outsideapplication.model.PaymentTypeResponse;
import com.example.administator.outsideapplication.model.PlaceOrderResponse;
import com.example.administator.outsideapplication.model.ProductDetailsResponse;
import com.example.administator.outsideapplication.model.ProductListingResponse;
import com.example.administator.outsideapplication.model.ProfileResponse;
import com.example.administator.outsideapplication.model.SignUpResponse;
import com.example.administator.outsideapplication.model.SignupOtpResponse;
import com.example.administator.outsideapplication.model.TermsResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @GET("home/cities")
    Call<CityResponse> CityListing();

    @FormUrlEncoded
    @POST("home/areas")
    Call<AreaResponse> AreaData(@Field("city_id") String city_id);

    @FormUrlEncoded
    @POST("home")
    Call<HomeResponse> HomeData(@Field("city_id") String city_id, @Field("area_id") String area_id);

    @GET("pages/about_us")
    Call<AboutResponse> About_us();

    @FormUrlEncoded
    @POST("home/productdetails")
    Call<ProductDetailsResponse> ProductDetails(@Field("city_id") String city_id,
                                                @Field("area_id") String area_id,
                                                @Field("product_id") String product_id,
                                                @Field("user_id") String user_id);

    @GET("pages/terms_and_conditions")
    Call<TermsResponse> Terms();

    @GET("pages/contact_us")
    Call<ContactUsResponse> ContactUs();

    @FormUrlEncoded
    @POST("register")
    Call<SignUpResponse> Signup(@Field("name") String name, @Field("email") String email, @Field("phone") String phone, @Field("password") String password, @Field("status") String status,
                                @Field("refference_code") String refference_code);

    @FormUrlEncoded
    @POST("register/otp_verify")
    Call<SignupOtpResponse> SignUpOtpVerify(@Field("user_id") String user_id, @Field("otp") String otp);

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> Login(@Field("city_id") String city_id, @Field("area_id") String area_id, @Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("users/myprofile")
    Call<ProfileResponse> Profile(@Field("city_id") String city_id, @Field("area_id") String area_id, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("home/productlist_cat")
    Call<ProductListingResponse> ProductListing(@Field("city_id") String city_id, @Field("area_id") String area_id, @Field("service") String service);

    @FormUrlEncoded
    @POST("cart/address")
    Call<AddressBookResponse> AddressBook(@Field("city_id") String city_id, @Field("area_id") String area_id, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("cart1/add_addressWithDefaultNew")
    Call<AddAddressResponse> AddAddress(@Field("user_id") String user_id,
                                        @Field("city_id") String city_id,
                                        @Field("area_id") String area_id,
                                        @Field("title") String title,
                                        @Field("firstname") String firstname,
                                        @Field("lastname") String lastname,
                                        @Field("email") String email,
                                        @Field("phone") String phone,
                                        @Field("address") String address,
                                        @Field("address1") String address1,
                                        @Field("default") String default1);

    @FormUrlEncoded
    @POST("cart/delete_address")
    Call<DeleteAddressReponse> DeleteAddress(@Field("city_id") String city_id,
                                             @Field("area_id") String area_id,
                                             @Field("address_id") String address_id);

    @FormUrlEncoded
    @POST("cart1/edit_addressWithDefault")
    Call<EditAddressResponse> EditAddress(@Field("address_id") String address_id,
                                          @Field("user_id") String user_id,
                                          @Field("city_id") String city_id,
                                          @Field("area_id") String area_id,
                                          @Field("title") String title,
                                          @Field("firstname") String firstname,
                                          @Field("lastname") String lastname,
                                          @Field("email") String email,
                                          @Field("phone") String phone,
                                          @Field("address") String address,
                                          @Field("address1") String address1,
                                          @Field("defalut") int defalut,
                                          @Field("comments") String comments);

    @FormUrlEncoded
    @POST("cart1/getusercart_products")
    Call<CartResponse> CartListing(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("cart1/delete")
    Call<CartProductDeleteResponse> CartProductDelete(@Field("cart_id") String cart_id, @Field("action") String action);

    @FormUrlEncoded
    @POST("cart1/usercart")
    Call<AddCartResponse> AddToCart(@Field("quantity") String quantity,
                                    @Field("user_id") String user_id,
                                    @Field("pid") String pid,
                                    @Field("action") String action);

    @FormUrlEncoded
    @POST("users/changepassword")
    Call<ChangePasswordResponse> ChangePassword(@Field("user_id") String user_id,
                                                @Field("oldpassword") String oldpassword,
                                                @Field("password") String password);

    @FormUrlEncoded
    @POST("users/myorders")
    Call<MyOrdereResponse> MyOrders(@Field("city_id") String city_id,
                                    @Field("area_id") String area_id,
                                    @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("users/orderdetails")
    Call<OrderDetailsResponse> OrderDetails(@Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("cart1/cart_total")
    Call<CartCountResponse> CartCount(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("cart1/checkout")
    Call<PaymentResponse> PaymentData(@Field("user_id") String user_id);

    @GET("cart1/payment_mode")
    Call<PaymentTypeResponse> PaymentType();

    @Multipart
    @POST("users/editprofile")
    Call<EditProfileResponse> EditProfile(@Part MultipartBody.Part file,
                                          @Part("fname") RequestBody fname,
                                          @Part("lname") RequestBody lname,
                                          @Part("city_id") RequestBody city_id,
                                          @Part("area_id") RequestBody area_id,
                                          @Part("user_id") RequestBody user_id);

    @FormUrlEncoded
    @POST("cart1/placeorder_new")
    Call<PlaceOrderResponse> PlaceOrder(@Field("payment_typeid") String payment_typeid,
                                    @Field("address_id") String address_id,
                                    @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("users/cancelorder")
    Call<CancelOrderResponse> CancelOrder(@Field("order_id") String order_id,
                                          @Field("reason") String reason,
                                          @Field("comments") String comments);

    @FormUrlEncoded
    @POST("users/forgotpassword")
    Call<ForgotPasswordResponse> ForgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("users/forgot_otp_verify")
    Call<ForgotOtpResponse> ForgotOtp(@Field("user_id") String user_id,
                                      @Field("phone") String phone,
                                      @Field("otp") String otp);

}
