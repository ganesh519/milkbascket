package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class OrderBookingDataResponse {

    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("address_id")
    @Expose
    public String addressId;
    @SerializedName("amount")
    @Expose
    public String amount;
    @SerializedName("sub_total")
    @Expose
    public String subTotal;
    @SerializedName("additional_charges")
    @Expose
    public String additionalCharges;
    @SerializedName("gst")
    @Expose
    public String gst;
    @SerializedName("coupon_amount")
    @Expose
    public String couponAmount;
    @SerializedName("payment_mode")
    @Expose
    public String paymentMode;
    @SerializedName("date_time")
    @Expose
    public String dateTime;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("mode")
    @Expose
    public String mode;
    @SerializedName("pickup_location")
    @Expose
    public String pickupLocation;
    @SerializedName("drop_location")
    @Expose
    public String dropLocation;
    @SerializedName("appointment_date")
    @Expose
    public String appointmentDate;
    @SerializedName("appointment_time")
    @Expose
    public String appointmentTime;
    @SerializedName("item_type")
    @Expose
    public String itemType;
    @SerializedName("delivery_category")
    @Expose
    public String deliveryCategory;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("distance")
    @Expose
    public String distance;
    @SerializedName("booking_type")
    @Expose
    public String bookingType;
    @SerializedName("pickup_address")
    @Expose
    public String pickupAddress;
    @SerializedName("drop_address")
    @Expose
    public String dropAddress;
    @SerializedName("pickup_number")
    @Expose
    public String pickupNumber;
    @SerializedName("drop_number")
    @Expose
    public String dropNumber;
    @SerializedName("weight")
    @Expose
    public String weight;
    @SerializedName("helper_id")
    @Expose
    public String helperId;
    @SerializedName("wallet_amount")
    @Expose
    public String walletAmount;
    @SerializedName("giftwrap")
    @Expose
    public String giftwrap;
    @SerializedName("train_no")
    @Expose
    public String trainNo;
    @SerializedName("train_name")
    @Expose
    public String trainName;
    @SerializedName("train_coach_no")
    @Expose
    public String trainCoachNo;
    @SerializedName("train_breath_no")
    @Expose
    public String trainBreathNo;
    @SerializedName("helper_name")
    @Expose
    public String helperName;
    @SerializedName("helper_phone")
    @Expose
    public String helperPhone;
    @SerializedName("helper_photo")
    @Expose
    public String helperPhoto;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userId", userId).append("addressId", addressId).append("amount", amount).append("subTotal", subTotal).append("additionalCharges", additionalCharges).append("gst", gst).append("couponAmount", couponAmount).append("paymentMode", paymentMode).append("dateTime", dateTime).append("status", status).append("mode", mode).append("pickupLocation", pickupLocation).append("dropLocation", dropLocation).append("appointmentDate", appointmentDate).append("appointmentTime", appointmentTime).append("itemType", itemType).append("deliveryCategory", deliveryCategory).append("message", message).append("distance", distance).append("bookingType", bookingType).append("pickupAddress", pickupAddress).append("dropAddress", dropAddress).append("pickupNumber", pickupNumber).append("dropNumber", dropNumber).append("weight", weight).append("helperId", helperId).append("walletAmount", walletAmount).append("giftwrap", giftwrap).append("trainNo", trainNo).append("trainName", trainName).append("trainCoachNo", trainCoachNo).append("trainBreathNo", trainBreathNo).append("helperName", helperName).append("helperPhone", helperPhone).append("helperPhoto", helperPhoto).toString();
    }

}
