package com.example.administator.outsideapplication.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.model.AreaDataDetailsResponse;
import com.example.administator.outsideapplication.model.Category_spinner;

import java.util.List;

@SuppressWarnings("unchecked")
public class CustomListAdapter extends ArrayAdapter<String> {

    private Context activity;
    private int data;
    public Resources res;
    String mSelected;
    Category_spinner tempValues = null;
    LayoutInflater inflater;
    List<AreaDataDetailsResponse> areaDataDetailsResponses;
    TextView label;
    List<Category_spinner> category_spinner;


    public CustomListAdapter(Context context, int viewterm, List category_spinner, String selected) {
        super(context, viewterm, category_spinner);
        this.activity = context;
        this.category_spinner = category_spinner;
        this.mSelected = selected;

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {

        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        View row = inflater.inflate(R.layout.spinner_rows, parent, false);

        /***** Get each Model object from Arraylist ********/
        tempValues = null;
        tempValues = (Category_spinner) category_spinner.get(position);

        label = (TextView) row.findViewById(R.id.qantity);

        // Set values for spinner each row
        label.setText(tempValues.getName());
        label.setTag(tempValues.getId());

        if (mSelected.equals(tempValues.getName())) {
            label.setTextColor(Color.parseColor("#2596c8"));

        } else {
            label.setTextColor(Color.parseColor("#000000"));

        }

//
//        if (mSelected.equals(tempValues.getQuantiti()+tempValues.getPricee())){
//            row.setBackgroundColor(Color.parseColor("#E97824"));
//            label.setTextColor(Color.parseColor("#ffffff"));
//        }else {
//            row.setBackgroundColor(Color.parseColor("#ffffff"));
//
//        }
        return row;
    }
}