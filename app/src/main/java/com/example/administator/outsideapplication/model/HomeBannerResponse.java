package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class HomeBannerResponse {
    @SerializedName("banner_id")
    @Expose
    public String bannerId;
    @SerializedName("banner_image")
    @Expose
    public String bannerImage;
    @SerializedName("alt")
    @Expose
    public String alt;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("url")
    @Expose
    public String url;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("bannerId", bannerId).append("bannerImage", bannerImage).append("alt", alt).append("type", type).append("url", url).toString();
    }

}
