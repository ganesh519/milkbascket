package com.example.administator.outsideapplication.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.model.AreaDataDetailsResponse;

import java.util.List;

public class RecyclerAreaAdapter extends RecyclerView.Adapter<RecyclerAreaAdapter.Holder> {
    List<AreaDataDetailsResponse> areaDataDetailsResponses;
    Context context;

    public RecyclerAreaAdapter(List<AreaDataDetailsResponse> areaDataDetailsResponse, Context context) {
        this.areaDataDetailsResponses = areaDataDetailsResponse;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerAreaAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_area, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAreaAdapter.Holder holder, final int i) {
        holder.area_text.setText(areaDataDetailsResponses.get(i).areaName);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!areaDataDetailsResponses.get(i).isAdded) {
                    holder.area_text.setTextColor(Color.parseColor("#2596c8"));
                }
                areaDataDetailsResponses.get(i).isAdded = true;

            }
        });

    }

    @Override
    public int getItemCount() {
        return areaDataDetailsResponses.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView area_text;

        public Holder(@NonNull View itemView) {
            super(itemView);
            area_text = itemView.findViewById(R.id.area_text);
        }
    }
}
