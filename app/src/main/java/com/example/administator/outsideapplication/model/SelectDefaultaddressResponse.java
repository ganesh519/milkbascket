package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class SelectDefaultaddressResponse {


    @SerializedName("address_id")
    @Expose
    public String addressId;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("firstname")
    @Expose
    public String firstname;
    @SerializedName("lastname")
    @Expose
    public String lastname;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("area")
    @Expose
    public String area;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("comments")
    @Expose
    public String comments;
    @SerializedName("address1")
    @Expose
    public String address1;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("addressId", addressId).append("title", title).append("firstname", firstname).append("lastname", lastname).append("email", email).append("address", address).append("area", area).append("city", city).append("phone", phone).append("comments", comments).append("address1", address1).toString();
    }

}
