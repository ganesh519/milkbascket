package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class OrderBookingDetailResponse {
    @SerializedName("product_id")
    @Expose
    public String productId;
    @SerializedName("vendor_id")
    @Expose
    public String vendorId;
    @SerializedName("product_name")
    @Expose
    public String productName;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("quantity")
    @Expose
    public String quantity;
    @SerializedName("total_price")
    @Expose
    public String totalPrice;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("product_image")
    @Expose
    public String productImage;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("specifications")
    @Expose
    public String specifications;
    @SerializedName("gift_price")
    @Expose
    public String giftPrice;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("productId", productId).append("vendorId", vendorId).append("productName", productName).append("price", price).append("quantity", quantity).append("totalPrice", totalPrice).append("image", image).append("productImage", productImage).append("type", type).append("specifications", specifications).append("giftPrice", giftPrice).toString();
    }

}
