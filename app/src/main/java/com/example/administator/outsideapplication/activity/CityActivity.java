package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.adapters.RecyclerCityAdapter;
import com.example.administator.outsideapplication.model.CityDataDetailsResponse;
import com.example.administator.outsideapplication.model.CityDataResponse;
import com.example.administator.outsideapplication.model.CityResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CityActivity extends AppCompatActivity {
    RecyclerView recycler_city;
    ApiInterface apiInterface;
    Context context;
    ImageView cancel;
    public static RecyclerView recycler_area;
   public static ListView list_area;
   AppController appController;
    String activity;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.city);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        cancel=findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        recycler_area=findViewById(R.id.recycler_area);
        list_area=findViewById(R.id.list_area);

        recycler_city=findViewById(R.id.recycler_city);

        appController = (AppController) getApplication();

        if (appController.isConnection()) {

            getData();

        }
        else {

            setContentView(R.layout.internet);
            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }

        if (getIntent().getExtras() != null) {
            activity=getIntent().getStringExtra("Activity");
        }
    }

    private void getData() {

        final ProgressDialog progressDialog = new ProgressDialog(CityActivity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        apiInterface=ApiClient.getClient().create(ApiInterface.class);
        Call<CityResponse> cityResponseCall=apiInterface.CityListing();
        cityResponseCall.enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                if (response.isSuccessful());
                CityResponse cityResponse=response.body();
                if (cityResponse.status == 1){
                    progressDialog.dismiss();
                    CityDataResponse cityDataResponse=cityResponse.data;
                    List<CityDataDetailsResponse> cityDataDetailsResponses=cityDataResponse.city;
                    recycler_city.setAdapter(new RecyclerCityAdapter(cityDataDetailsResponses, context));
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(CityActivity.this, 4);
                    recycler_city.setLayoutManager(gridLayoutManager);
                    recycler_city.setHasFixedSize(true);
                    recycler_city.setNestedScrollingEnabled(false);
                }
                else if (cityResponse.status == 0){
                    progressDialog.dismiss();
                    Toast.makeText(CityActivity.this, cityResponse.message, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(CityActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (activity.equals("MainActivity")){
            finish();
        }
        if (activity.equals("Splash")){
            finish();
        }
    }
}
