package com.example.administator.outsideapplication.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.model.ContactUsDataDetailsResponse;
import com.example.administator.outsideapplication.model.ContactUsDataResponse;
import com.example.administator.outsideapplication.model.ContactUsResponse;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Contact_us extends AppCompatActivity implements OnMapReadyCallback {
    ApiInterface apiInterface;
    TextView address;
    LinearLayout linear;
    ImageView back;
    LatLngBounds bounds;
    LatLngBounds.Builder boundsBuilder;
    Marker marker;
    float zoomLevel;
    private GoogleMap mMap;
    double lat;
    double lon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_us);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        address = findViewById(R.id.address);
        linear = findViewById(R.id.linear);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        boundsBuilder=new LatLngBounds.Builder();

        final ProgressDialog progressDialog = new ProgressDialog(Contact_us.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ContactUsResponse> call = apiInterface.ContactUs();
        call.enqueue(new Callback<ContactUsResponse>() {
            @Override
            public void onResponse(Call<ContactUsResponse> call, Response<ContactUsResponse> response) {
                if (response.isSuccessful()) ;
                ContactUsResponse contactUsResponse = response.body();
                if (contactUsResponse.status == 1) {
                    linear.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    ContactUsDataResponse contactUsDataResponse = contactUsResponse.data;
                    ContactUsDataDetailsResponse contactUsDataDetailsResponse = contactUsDataResponse.page;
                    lat =  Double.parseDouble(contactUsDataDetailsResponse.latitude);
                    lon =  Double.parseDouble(contactUsDataDetailsResponse.longitude);

                    final LatLng sydney = new LatLng(lat, lon);
                    boundsBuilder.include(sydney);
                    bounds=boundsBuilder.build();
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                    marker=mMap.addMarker(new MarkerOptions().position(sydney));

                    mMap.getUiSettings().setZoomControlsEnabled(false);
                    mMap.getUiSettings().setCompassEnabled(true);
                    mMap.getUiSettings().setMyLocationButtonEnabled(true);
                    mMap.getUiSettings().setMapToolbarEnabled(true);
                    mMap.getUiSettings().setZoomGesturesEnabled(true);
                    mMap.getUiSettings().setScrollGesturesEnabled(true);
                    mMap.getUiSettings().setTiltGesturesEnabled(true);
                    mMap.getUiSettings().setRotateGesturesEnabled(true);

                    try {
                        zoomLevel = (float) 18.0;
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, zoomLevel));
                    } catch (IllegalStateException e) {
// layout not yet initialized
                        final View mapView = getFragmentManager()
                                .findFragmentById(R.id.map).getView();
                        if (mapView.getViewTreeObserver().isAlive()) {
                            mapView.getViewTreeObserver().addOnGlobalLayoutListener(
                                    new ViewTreeObserver.OnGlobalLayoutListener() {
                                        @SuppressWarnings("deprecation")
                                        @SuppressLint("NewApi")
// We check which build version we are using.
                                        @Override
                                        public void onGlobalLayout() {
                                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                                                mapView.getViewTreeObserver()
                                                        .removeGlobalOnLayoutListener(this);
                                            } else {
                                                mapView.getViewTreeObserver()
                                                        .removeOnGlobalLayoutListener(this);
                                            }
                                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, zoomLevel));
                                        }
                                    });
                        }
                    }

                    String data = contactUsDataDetailsResponse.description;
                    String result = Html.fromHtml(data).toString();
                    address.setText(result);

                } else if (contactUsResponse.status == 0) {
                    linear.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    Toast.makeText(Contact_us.this, contactUsResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ContactUsResponse> call, Throwable t) {
                linear.setVisibility(View.GONE);
                progressDialog.dismiss();
                Toast.makeText(Contact_us.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
