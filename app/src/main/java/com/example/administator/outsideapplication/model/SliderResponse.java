package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class SliderResponse {
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("alternate_text")
    @Expose
    public String alternateText;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("sort_order")
    @Expose
    public String sortOrder;
    @SerializedName("description")
    @Expose
    public String description;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("title", title).append("image", image).append("alternateText", alternateText).append("url", url).append("sortOrder", sortOrder).append("description", description).toString();
    }

}
