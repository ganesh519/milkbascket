package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.model.TermsDataDetailsResponse;
import com.example.administator.outsideapplication.model.TermsDataResponse;
import com.example.administator.outsideapplication.model.TermsResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Terms_conditions extends AppCompatActivity {
    TextView terms_text;
    ApiInterface apiInterface;
    LinearLayout linear;
    ImageView back;
    AppController appController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_condition);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        terms_text = findViewById(R.id.terms_text);
        linear = findViewById(R.id.linear);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        appController = (AppController) getApplication();
        if (appController.isConnection()) {

            getData();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }
    }

    private void getData() {
        final ProgressDialog progressDialog = new ProgressDialog(Terms_conditions.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<TermsResponse> call = apiInterface.Terms();
        call.enqueue(new Callback<TermsResponse>() {
            @Override
            public void onResponse(Call<TermsResponse> call, Response<TermsResponse> response) {
                if (response.isSuccessful()) ;
                TermsResponse termsResponse = response.body();
                if (termsResponse.status == 1) {
                    linear.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    TermsDataResponse termsDataResponse = termsResponse.data;
                    TermsDataDetailsResponse termsDataDetailsResponse = termsDataResponse.page;
                    String data = termsDataDetailsResponse.description;
                    String result = Html.fromHtml(data).toString();
                    terms_text.setText(result);


                } else if (termsResponse.status == 0) {
                    linear.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    Toast.makeText(Terms_conditions.this, termsResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TermsResponse> call, Throwable t) {
                linear.setVisibility(View.GONE);
                progressDialog.dismiss();
                Toast.makeText(Terms_conditions.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }
}
