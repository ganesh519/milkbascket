package com.example.administator.outsideapplication.activity;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.CircleAnimationUtil;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.adapters.VeiewAdapter;
import com.example.administator.outsideapplication.model.AddCartResponse;
import com.example.administator.outsideapplication.model.CartCountDataResponse;
import com.example.administator.outsideapplication.model.CartCountResponse;
import com.example.administator.outsideapplication.model.ProductDetailsData1Response;
import com.example.administator.outsideapplication.model.ProductDetailsDataResponse;
import com.example.administator.outsideapplication.model.ProductDetailsResponse;
import com.example.administator.outsideapplication.model.ProductImageResponse;
import com.rd.PageIndicatorView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Productdetails_Activity extends AppCompatActivity {
    ViewPager pager;

    TextView P_name, discount_price, actual_price, descr, cart_qty_txt, cart_count_txt;
    ApiInterface apiInterface;
    RelativeLayout linear_add, relative_hide, img_cart;
    ImageView back, cart, cart_add_img, cart_minus_img;
    RelativeLayout relative_add;
    int count = 1;
    ProgressBar progressbar_add, progressbar_addincrease;
    String qty, p_id, user_id;
    AppController appController;
    int count1;
    PageIndicatorView pageIndicatorView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        linear_add = findViewById(R.id.linear_add);
        pager = findViewById(R.id.pager);
//        indicator = findViewById(R.id.indicator);
        P_name = findViewById(R.id.P_name);
        discount_price = findViewById(R.id.discount_price);
        actual_price = findViewById(R.id.actual_price);
        descr = findViewById(R.id.descr);
        cart = findViewById(R.id.cart);
        pageIndicatorView = findViewById(R.id.pageIndicatorView);
        cart_count_txt = findViewById(R.id.cart_count_txt);
        img_cart = findViewById(R.id.img_cart);

        progressbar_add = findViewById(R.id.progressbar_add);
        progressbar_addincrease = findViewById(R.id.progressbar_addincrease);

        relative_add = findViewById(R.id.relative_add);

        cart_qty_txt = findViewById(R.id.cart_qty_txt);
        cart_add_img = findViewById(R.id.cart_add_img);


        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Productdetails_Activity.this, MainActivity.class));
            }
        });
        relative_hide = findViewById(R.id.relative_hide);

        appController = (AppController) getApplication();
        if (appController.isConnection()) {

            getData();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }

        relative_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (user_id.equals("")) {
                    Intent intent = new Intent(Productdetails_Activity.this, LoginActivity.class);
                    intent.putExtra("signup", "ProductDetailsActivity");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Add();
                }

            }
        });

        if (user_id.equals("")) {
            img_cart.setVisibility(View.GONE);
        } else {
            img_cart.setVisibility(View.VISIBLE);
            CartCount(user_id);
        }

        cart_add_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCart();
            }
        });
        cart_minus_img = findViewById(R.id.cart_minus_img);
        cart_minus_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteToCart();
            }
        });
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (count1 == 0) {
                    Toast.makeText(Productdetails_Activity.this, "Their is products in cart", Toast.LENGTH_SHORT).show();
                } else {
                    startActivity(new Intent(Productdetails_Activity.this, CartActivity.class));
                }


            }
        });
    }

    private void getData() {

        String city_id = MainActivity.city_id;
        String area_id = MainActivity.area_id;

        SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        user_id = sharedPreferences1.getString("user_id", "");

        final String product_id = getIntent().getStringExtra("Product_id");

        final ProgressDialog progressDialog = new ProgressDialog(Productdetails_Activity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProductDetailsResponse> call = apiInterface.ProductDetails(city_id, area_id, product_id, user_id);
        call.enqueue(new Callback<ProductDetailsResponse>() {
            @Override
            public void onResponse(Call<ProductDetailsResponse> call, Response<ProductDetailsResponse> response) {
                if (response.isSuccessful()) ;
                ProductDetailsResponse productDetailsResponse = response.body();
                if (productDetailsResponse.status == 1) {
                    relative_hide.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    ProductDetailsDataResponse productDetailsDataResponse = productDetailsResponse.data;
                    ProductDetailsData1Response productDetailsData1Response = productDetailsDataResponse.productDetails;

                    P_name.setText(productDetailsData1Response.name);
                    discount_price.setText("₹" + productDetailsData1Response.discountPrice);
                    actual_price.setText("₹" + productDetailsData1Response.price);
                    actual_price.setPaintFlags(actual_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    String data = productDetailsData1Response.description;

                    p_id = productDetailsData1Response.prodId;

                    String no = productDetailsData1Response.cartCount;

                    if (no.equals("0")) {
                        relative_add.setVisibility(View.VISIBLE);
                        linear_add.setVisibility(View.GONE);
                    } else {
                        linear_add.setVisibility(View.VISIBLE);
                        relative_add.setVisibility(View.GONE);

                        cart_qty_txt.setText(no);

                    }

                    String result = Html.fromHtml(data).toString();

                    descr.setText(result);

                    List<ProductImageResponse> productImageResponse = productDetailsData1Response.dataimages;
                    SingleImages(productImageResponse);


                } else if (productDetailsResponse.status == 0) {
                    relative_hide.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    Toast.makeText(Productdetails_Activity.this, productDetailsResponse.message, Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ProductDetailsResponse> call, Throwable t) {
                relative_hide.setVisibility(View.GONE);
                progressDialog.dismiss();
                Toast.makeText(Productdetails_Activity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }




    private void Add() {
        qty = "1";
        String action = "add";
        progressbar_add.setVisibility(View.VISIBLE);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddCartResponse> cartResponseCall = apiInterface.AddToCart(qty, user_id, p_id, action);
        cartResponseCall.enqueue(new Callback<AddCartResponse>() {
            @Override
            public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                if (response.isSuccessful()) ;
                AddCartResponse addCartResponse = response.body();
                if (addCartResponse.status == 1) {
                    relative_add.setVisibility(View.GONE);
                    progressbar_add.setVisibility(View.GONE);
                    linear_add.setVisibility(View.VISIBLE);
                    CartCount(user_id);
                    Toast.makeText(Productdetails_Activity.this, addCartResponse.message, Toast.LENGTH_SHORT).show();
                } else if (addCartResponse.status == 0) {
                    progressbar_add.setVisibility(View.GONE);
                    Toast.makeText(Productdetails_Activity.this, addCartResponse.message, Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<AddCartResponse> call, Throwable t) {
                progressbar_add.setVisibility(View.GONE);
                Toast.makeText(Productdetails_Activity.this, t.getMessage(), Toast.LENGTH_SHORT).show();


            }
        });


    }

    private void addToCart() {
        String qtystr = cart_qty_txt.getText().toString();
        int qty = Integer.parseInt(qtystr);
        if (qty != 0)
            qty++;
        final String value = String.valueOf(qty);
        String action = "add";
        progressbar_addincrease.setVisibility(View.VISIBLE);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddCartResponse> cartResponseCall = apiInterface.AddToCart(value, user_id, p_id, action);

        cartResponseCall.enqueue(new Callback<AddCartResponse>() {
            @Override
            public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                if (response.isSuccessful()) ;
                AddCartResponse addCartResponse = response.body();
                if (addCartResponse.status == 1) {
                    cart_qty_txt.setText(value);
                    progressbar_addincrease.setVisibility(View.GONE);
                    CartCount(user_id);
                } else if (addCartResponse.status == 0) {
                    progressbar_addincrease.setVisibility(View.GONE);
                    Toast.makeText(Productdetails_Activity.this, addCartResponse.message, Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<AddCartResponse> call, Throwable t) {
                progressbar_addincrease.setVisibility(View.GONE);
                Toast.makeText(Productdetails_Activity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void deleteToCart() {
        final String qtystr = cart_qty_txt.getText().toString();
        int qty = Integer.parseInt(qtystr);

        if (qty > 1)
            qty--;
        final String value = String.valueOf(qty);
        String action = "dec";
        progressbar_addincrease.setVisibility(View.VISIBLE);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddCartResponse> cartResponseCall = apiInterface.AddToCart(value, user_id, p_id, action);

        cartResponseCall.enqueue(new Callback<AddCartResponse>() {
            @Override
            public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                if (response.isSuccessful()) ;
                AddCartResponse addCartResponse = response.body();
                if (addCartResponse.status == 1) {
                    cart_qty_txt.setText(value);
                    int qq = Integer.valueOf(qtystr);
                    if (qq == 1) {
                        relative_add.setVisibility(View.VISIBLE);
                        linear_add.setVisibility(View.GONE);
                    }
                    CartCount(user_id);
                    progressbar_addincrease.setVisibility(View.GONE);

                } else if (addCartResponse.status == 0) {
                    progressbar_addincrease.setVisibility(View.GONE);
                    Toast.makeText(Productdetails_Activity.this, addCartResponse.message, Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<AddCartResponse> call, Throwable t) {
                progressbar_addincrease.setVisibility(View.GONE);
                Toast.makeText(Productdetails_Activity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void SingleImages(List<ProductImageResponse> homeProductimageResponse) {
        VeiewAdapter viewPagerAdapter = new VeiewAdapter(homeProductimageResponse, Productdetails_Activity.this);
        pager.setAdapter(viewPagerAdapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pageIndicatorView.setSelection(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void CartCount(String user_id) {
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CartCountResponse> cartCountResponseCall = apiInterface.CartCount(user_id);
        cartCountResponseCall.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()) ;
                CartCountResponse cartCountResponse = response.body();
                if (cartCountResponse.status == 1) {
                    CartCountDataResponse cartCountDataResponse = cartCountResponse.data;
                    count1 = cartCountDataResponse.cartCount;
                    cart_count_txt.setText(String.valueOf(count1));
//                    makeFlyAnimation(cart_count_txt);
                    img_cart.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (count1 == 0) {
                                Toast.makeText(Productdetails_Activity.this, "No Products in Cart", Toast.LENGTH_SHORT).show();
                            } else {
                                startActivity(new Intent(Productdetails_Activity.this, CartActivity.class));
                            }
                        }
                    });
                } else if (cartCountResponse.status == 0) {
                    Toast.makeText(Productdetails_Activity.this, cartCountResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {
                Toast.makeText(Productdetails_Activity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void makeFlyAnimation(final TextView targetView) {

        ImageView destView = findViewById(R.id.cart);
        new CircleAnimationUtil().attachActivity(this).setTargetView(targetView)
                .setMoveDuration(1000).setDestView(destView).setAnimationListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                targetView.setVisibility(View.VISIBLE);
                //getCartCount();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).startAnimation();
    }


}
