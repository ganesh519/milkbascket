package com.example.administator.outsideapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.model.SliderResponse;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

public class SliderAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    List<SliderResponse> upSliderResponse;

    public SliderAdapter(List<SliderResponse> upSliderResponse, Context context) {
        this.context = context;
        this.upSliderResponse = upSliderResponse;
    }

    @Override
    public int getCount() {
        return upSliderResponse.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.upslider_layout, container, false);

        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);

        Picasso.get().load("https://www.testocar.in/boloadmin/" + upSliderResponse.get(position).image).placeholder(R.drawable.default_loading).into(imageView);

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);


        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }

}
