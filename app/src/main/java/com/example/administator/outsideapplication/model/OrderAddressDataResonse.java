package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class OrderAddressDataResonse {

    @SerializedName("firstname")
    @Expose
    public String firstname;
    @SerializedName("lastname")
    @Expose
    public String lastname;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("address1")
    @Expose
    public String address1;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("area")
    @Expose
    public String area;
    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("comments")
    @Expose
    public String comments;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("firstname", firstname).append("lastname", lastname).append("email", email).append("phone", phone).append("address", address).append("address1", address1).append("city", city).append("area", area).append("country", country).append("comments", comments).toString();
    }

}
