package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class SignUpDataResponse {


    @SerializedName("user_id")
    @Expose
    public Integer userId;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("otp")
    @Expose
    public Integer otp;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userId", userId).append("firstName", firstName).append("lastName", lastName).append("phone", phone).append("otp", otp).toString();
    }
}
