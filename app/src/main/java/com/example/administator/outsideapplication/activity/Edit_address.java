package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.model.EditAddressResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Edit_address extends AppCompatActivity {
    TextInputEditText edit_title, edit_fname, edit_lname, edit_email, edit_phone, edit_addres_line1,edit_addres_line2;
    CheckBox checkbox;
    Button submit;
    ImageView back;
    int type;
    String title, fname, lname, email, phone, address, Activity;
    ApiInterface apiInterface;
    String address_id, Daddress, addres, f_name, titlee, mobilee, l_name, emaill,address1,activity_tag;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_address);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        init();

        back = findViewById(R.id.back);

        submit = findViewById(R.id.submit);

        checkbox = findViewById(R.id.checkbox);

        edit_title = findViewById(R.id.edit_title);
        edit_fname = findViewById(R.id.edit_fname);
        edit_lname = findViewById(R.id.edit_lname);
        edit_email = findViewById(R.id.edit_email);
        edit_phone = findViewById(R.id.edit_phone);
        edit_addres_line1 = findViewById(R.id.edit_addres_line1);
        edit_addres_line2=findViewById(R.id.edit_addres_line2);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Daddress = bundle.getString("Default");
            address_id = bundle.getString("Address_id");
            addres = bundle.getString("Address");
            f_name = bundle.getString("Fname");
            titlee = bundle.getString("Title");
            mobilee = bundle.getString("Mobile");
            l_name = bundle.getString("Lname");
            emaill = bundle.getString("Email");
            address1=bundle.getString("Address1");
            activity_tag=bundle.getString("Activity");
        }

        edit_title.setText(titlee);
        edit_fname.setText(f_name);
        edit_lname.setText(l_name);
        edit_email.setText(emaill);
        edit_addres_line1.setText(addres);
        edit_phone.setText(mobilee);
        edit_addres_line2.setText(address1);

        if (Daddress.equalsIgnoreCase("1")) {
            checkbox.setChecked(true);
            type = 1;
        } else {
            checkbox.setChecked(false);
            type = 0;

        }

        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    type = 1;
                } else {
                    type = 0;
                }
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               getData();
            }
        });
    }

    private void getData() {
        title = edit_title.getText().toString();
        fname = edit_fname.getText().toString();
        lname = edit_lname.getText().toString();
        email = edit_email.getText().toString();
        phone = edit_phone.getText().toString();
        address = edit_addres_line1.getText().toString();
        address1=edit_addres_line2.getText().toString();

        if (fname.equals("")) {
            Toast.makeText(Edit_address.this, "Enter First Name", Toast.LENGTH_SHORT).show();
        } else if (lname.equals("")) {
            Toast.makeText(Edit_address.this, "Enter Last Name", Toast.LENGTH_SHORT).show();

        } else if (email.equals("")) {
            Toast.makeText(Edit_address.this, "Enter Email", Toast.LENGTH_SHORT).show();
        } else if (phone.equals("")) {
            Toast.makeText(Edit_address.this, "Enter Mobile Number", Toast.LENGTH_SHORT).show();
        } else if (address.equals("")) {
            Toast.makeText(Edit_address.this, "Enter Address line1", Toast.LENGTH_SHORT).show();
        }
        else if (address1.equals("")) {
            Toast.makeText(Edit_address.this, "Enter Address line2", Toast.LENGTH_SHORT).show();
        }
        else {

            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("CityandArea", Context.MODE_PRIVATE);
            final String city_id = sharedPreferences.getString("City_id", "");
            final String area_id = sharedPreferences.getString("Area", "");

            SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
            final String user_id = sharedPreferences1.getString("user_id", "");
            final ProgressDialog progressDialog = new ProgressDialog(Edit_address.this);
            progressDialog.setMessage("wait...");
            progressDialog.show();
            apiInterface = ApiClient.getClient().create(ApiInterface.class);

            Call<EditAddressResponse> call = apiInterface.EditAddress(address_id, user_id, city_id, area_id, title, fname, lname, email, phone, address, address1, type,"hhh");
            call.enqueue(new Callback<EditAddressResponse>() {
                @Override
                public void onResponse(Call<EditAddressResponse> call, Response<EditAddressResponse> response) {
                    if (response.isSuccessful()) ;
                    EditAddressResponse editAddressResponse = response.body();
                    if (editAddressResponse.status == 1) {
                        progressDialog.dismiss();
                        Toast.makeText(Edit_address.this, editAddressResponse.message, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Edit_address.this, AddressActivity.class);
                        intent.putExtra("Activity",activity_tag);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    } else if (editAddressResponse.status == 0) {
                        progressDialog.dismiss();
                        Toast.makeText(Edit_address.this, editAddressResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<EditAddressResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(Edit_address.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void init() {
//        final List<String> list = new ArrayList<String>();
//        list.add("Home");
//        list.add("Office");
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
//                android.R.layout.simple_spinner_item, list);
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(dataAdapter);
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                pos1=position;
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });


    }


}
