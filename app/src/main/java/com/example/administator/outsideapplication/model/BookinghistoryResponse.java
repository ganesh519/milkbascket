package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class BookinghistoryResponse {
    @SerializedName("orderlogs_id")
    @Expose
    public String orderlogsId;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("date_time")
    @Expose
    public String dateTime;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("orderlogsId", orderlogsId).append("status", status).append("dateTime", dateTime).toString();
    }

}
