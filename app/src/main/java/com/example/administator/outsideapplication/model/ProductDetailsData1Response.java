package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class ProductDetailsData1Response {

    @SerializedName("prod_id")
    @Expose
    public String prodId;
    @SerializedName("service_id")
    @Expose
    public String serviceId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("discount_price")
    @Expose
    public String discountPrice;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("availability")
    @Expose
    public String availability;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("tags")
    @Expose
    public String tags;
    @SerializedName("vendor_name")
    @Expose
    public String vendorName;
    @SerializedName("vendor_email")
    @Expose
    public String vendorEmail;
    @SerializedName("vendor_phone")
    @Expose
    public String vendorPhone;
    @SerializedName("vendor_city")
    @Expose
    public String vendorCity;
    @SerializedName("vendor_area")
    @Expose
    public String vendorArea;
    @SerializedName("vendor_id")
    @Expose
    public String vendorId;
    @SerializedName("service_name")
    @Expose
    public String serviceName;
    @SerializedName("pricename")
    @Expose
    public String pricename;
    @SerializedName("specifname")
    @Expose
    public String specifname;
    @SerializedName("variation_title")
    @Expose
    public String variationTitle;
    @SerializedName("spname")
    @Expose
    public String spname;
    @SerializedName("appointment_price")
    @Expose
    public String appointmentPrice;
    @SerializedName("stock")
    @Expose
    public String stock;
    @SerializedName("service_type")
    @Expose
    public String serviceType;
    @SerializedName("money_back_title")
    @Expose
    public String moneyBackTitle;
    @SerializedName("money_back_des")
    @Expose
    public String moneyBackDes;
    @SerializedName("store_exchange_title")
    @Expose
    public String storeExchangeTitle;
    @SerializedName("store_exchange_des")
    @Expose
    public String storeExchangeDes;
    @SerializedName("lpg_title")
    @Expose
    public String lpgTitle;
    @SerializedName("lpg_des")
    @Expose
    public String lpgDes;
    @SerializedName("sg_title")
    @Expose
    public String sgTitle;
    @SerializedName("sg_des")
    @Expose
    public String sgDes;
    @SerializedName("return_available")
    @Expose
    public String returnAvailable;
    @SerializedName("return_policy")
    @Expose
    public String returnPolicy;
    @SerializedName("allow_gift")
    @Expose
    public String allowGift;
    @SerializedName("gift_price")
    @Expose
    public String giftPrice;
    @SerializedName("upload_video")
    @Expose
    public String uploadVideo;
    @SerializedName("colors")
    @Expose
    public String colors;
    @SerializedName("sizes")
    @Expose
    public String sizes;
    @SerializedName("cart_count")
    @Expose
    public String cartCount;
    @SerializedName("dataimages")
    @Expose
    public List<ProductImageResponse> dataimages = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("prodId", prodId).append("serviceId", serviceId).append("name", name).append("price", price).append("discountPrice", discountPrice).append("image", image).append("availability", availability).append("description", description).append("tags", tags).append("vendorName", vendorName).append("vendorEmail", vendorEmail).append("vendorPhone", vendorPhone).append("vendorCity", vendorCity).append("vendorArea", vendorArea).append("vendorId", vendorId).append("serviceName", serviceName).append("pricename", pricename).append("specifname", specifname).append("variationTitle", variationTitle).append("spname", spname).append("appointmentPrice", appointmentPrice).append("stock", stock).append("serviceType", serviceType).append("moneyBackTitle", moneyBackTitle).append("moneyBackDes", moneyBackDes).append("storeExchangeTitle", storeExchangeTitle).append("storeExchangeDes", storeExchangeDes).append("lpgTitle", lpgTitle).append("lpgDes", lpgDes).append("sgTitle", sgTitle).append("sgDes", sgDes).append("returnAvailable", returnAvailable).append("returnPolicy", returnPolicy).append("allowGift", allowGift).append("giftPrice", giftPrice).append("uploadVideo", uploadVideo).append("colors", colors).append("sizes", sizes).append("cartCount", cartCount).append("dataimages", dataimages).toString();
    }
}
