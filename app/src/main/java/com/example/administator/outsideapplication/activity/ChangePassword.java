package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.model.ChangePasswordResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePassword extends AppCompatActivity {
    ImageView back;
    Button btn_submit;
    EditText edt_newpassword, edt_oldpassword;
    ProgressDialog pd;
    String oldpass, newpass, user_id;
    ApiInterface apiInterface;
    AppController appController;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        appController = (AppController) getApplication();

        if (appController.isConnection()) {

            getData();

        }
        else {

            setContentView(R.layout.internet);
            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }

    }

    private void getData() {

        SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        user_id = sharedPreferences1.getString("user_id", "");
        edt_oldpassword = findViewById(R.id.edt_oldpassword);
        edt_newpassword = findViewById(R.id.edt_newpassword);
//        edt_cpassword=findViewById(R.id.edt_cpassword);
        btn_submit = findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldpass = edt_oldpassword.getText().toString();
                newpass = edt_newpassword.getText().toString();

                if (oldpass.equals("")) {
                    Toast.makeText(ChangePassword.this, "Please Enter Old Password", Toast.LENGTH_SHORT).show();
                }
                else if (newpass.equals("")) {
                    Toast.makeText(ChangePassword.this, "Please Enter new Password", Toast.LENGTH_SHORT).show();
                }
                else {
                    pd = new ProgressDialog(ChangePassword.this);
                    pd.setMessage("Loading....");
                    pd.show();

                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<ChangePasswordResponse> changePasswordResponseCall = apiInterface.ChangePassword(user_id, oldpass, newpass);
                    changePasswordResponseCall.enqueue(new Callback<ChangePasswordResponse>() {
                        @Override
                        public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                            if (response.isSuccessful()) ;
                            ChangePasswordResponse changePasswordResponse = response.body();
                            if (changePasswordResponse.status == 1) {
                                pd.dismiss();
                                Toast.makeText(ChangePassword.this, changePasswordResponse.message, Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(ChangePassword.this, MainActivity.class));
                            } else if (changePasswordResponse.status == 0) {
                                pd.dismiss();
                                Toast.makeText(ChangePassword.this, changePasswordResponse.message, Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(ChangePassword.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }
}
