package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.model.SignupOtpResponse;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpScreen extends AppCompatActivity {
    EditText signup_otp;
    String otp, mobile, code, user_id;
    ApiInterface apiInterface, apiInterface1;
    Button proceed;
    TextView resend;
//    SmsVerifyCatcher smsVerifyCatcher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_screen);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        signup_otp = findViewById(R.id.signup_otp);
        proceed = findViewById(R.id.proceed);
        resend = findViewById(R.id.resend);

        mobile = getIntent().getStringExtra("MOBILE");
        user_id = getIntent().getStringExtra("USER_ID");


//        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
//            @Override
//            public void onSmsCatch(String message) {
//                code = parseCode(message);
//                signup_otp.setText(code);
//            }
//        });


//        final ProgressDialog progressDialog1 = new ProgressDialog(OtpScreen.this);
//        progressDialog1.setMessage("wait...");
//        progressDialog1.show();
//        smsVerifyCatcher.onStart();

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                otp = signup_otp.getText().toString();

                final ProgressDialog progressDialog = new ProgressDialog(OtpScreen.this);
                progressDialog.setMessage("wait...");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<SignupOtpResponse> call = apiInterface.SignUpOtpVerify(user_id, otp);
                call.enqueue(new Callback<SignupOtpResponse>() {
                    @Override
                    public void onResponse(Call<SignupOtpResponse> call, Response<SignupOtpResponse> response) {
                        if (response.isSuccessful()) ;
                        SignupOtpResponse signupOtpResponse = response.body();
                        if (signupOtpResponse.status == 1) {
                            progressDialog.dismiss();
                            Toast.makeText(OtpScreen.this, signupOtpResponse.message, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.putExtra("signup", "login");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else if (signupOtpResponse.status == 0) {
                            progressDialog.dismiss();
                            Toast.makeText(OtpScreen.this, signupOtpResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<SignupOtpResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(OtpScreen.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });


            }
        });

    }
//
//    private String parseCode(String message) {
//        Pattern p = Pattern.compile("\\b\\d{6}\\b");
//        Matcher m = p.matcher(message);
//        String code = "";
//        while (m.find()) {
//            code = m.group(0);
//        }
//        return code;
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        smsVerifyCatcher.onStart();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        smsVerifyCatcher.onStop();
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }
}
