package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.model.ForgotPasswordDataResponse;
import com.example.administator.outsideapplication.model.ForgotPasswordResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassword extends AppCompatActivity {
    ImageView back;
    TextInputEditText edit_password;
    Button submit;
    AppController appController;
    ApiInterface apiInterface;
    String mail;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        back=findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
       
        appController = (AppController) getApplication();
        if (appController.isConnection()) {

            init();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }
        
    }

    private void init() {
        edit_password=findViewById(R.id.edit_password);
        submit=findViewById(R.id.submit);
        
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                mail=edit_password.getText().toString();
                
                if (mail.equals("")){
                    Toast.makeText(ForgotPassword.this, " Enter Email or Mobilenumber", Toast.LENGTH_SHORT).show();
                }
                else {
                    getData();
                }
            }
        });
        
    }

    private void getData() {

        final ProgressDialog progressDialog=new ProgressDialog(ForgotPassword.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();
        apiInterface= ApiClient.getClient().create(ApiInterface.class);

        Call<ForgotPasswordResponse> call=apiInterface.ForgotPassword(mail);
        call.enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {
                if (response.isSuccessful());
                ForgotPasswordResponse forgotPasswordResponse=response.body();
                if (forgotPasswordResponse.status == 1){
                    progressDialog.dismiss();
                    ForgotPasswordDataResponse forgotPasswordDataResponse=forgotPasswordResponse.data;

                    String mobile=forgotPasswordDataResponse.phone;
                    String fu_id=forgotPasswordDataResponse.userId;

                    Intent intent=new Intent(ForgotPassword.this,ForgotPasswordOtpScreen.class);
                    intent.putExtra("Mobile",mobile);
                    intent.putExtra("FU_id",fu_id);
                    startActivity(intent);

                }
                else if (forgotPasswordResponse.status == 0){
                    progressDialog.dismiss();
                    Toast.makeText(ForgotPassword.this, forgotPasswordResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ForgotPassword.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
