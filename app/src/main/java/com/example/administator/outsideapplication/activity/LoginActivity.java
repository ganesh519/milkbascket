package com.example.administator.outsideapplication.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.fragments.FragmentLogin;
import com.example.administator.outsideapplication.fragments.FragmentSignup;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    ImageView back;
    RecyclerView recycler_notification;
    Context context;
    TabLayout tabLayout;
    public static ViewPager viewPager;
    ViewPagerAdapterNotifications viewPagerAdapter;
    TextView login_txt;
    String Signup;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        viewPager = (ViewPager) findViewById(R.id.viewPager_login);
        viewPagerAdapter = new ViewPagerAdapterNotifications(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new FragmentLogin(), "Login");
        viewPagerAdapter.addFragment(new FragmentSignup(), "Signup");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs_login);
        tabLayout.setupWithViewPager(viewPager);

        login_txt = findViewById(R.id.login_txt);

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Signup.equals("ProductDetailsActivity")){
                   finishAffinity();
                }
                else {
                    finish();
                }
            }
        });


        Signup = getIntent().getStringExtra("signup");

        if (!Signup.equals("")) {
            if (Signup.equals("Sign")) {
                login_txt.setText("SIGNUP");
                viewPager.setCurrentItem(1);
            } else {
                login_txt.setText("LOGIN");
                viewPager.setCurrentItem(0);
            }
        } else {
            login_txt.setText("LOGIN");
            viewPager.setCurrentItem(0);
        }
    }

    public class ViewPagerAdapterNotifications extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapterNotifications(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

    }

    public static void setPage(int signup) {

        viewPager.setCurrentItem(signup);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (Signup.equals("ProductDetailsActivity")){
           finishAffinity();
        }
        else {
            finish();
        }
    }

    private void getMethod() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);
        alertDialog.setTitle("Exit");
        alertDialog.setMessage("Are you sure you want close this application?");


        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(DialogInterface dialog, int which) {
//                startActivity(new Intent(ctx, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
//                sessionManager.logoutUser();
                finishAffinity();
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }
}
