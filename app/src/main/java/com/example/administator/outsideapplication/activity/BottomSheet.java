package com.example.administator.outsideapplication.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.fragments.BottomSheetFragment;

public class BottomSheet extends AppCompatActivity {
    public static final String TAG = "bottom_sheet";
    Button btn_sheet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottom_sheet);
        btn_sheet=findViewById(R.id.btn_sheet);
        btn_sheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BottomSheetFragment fragment = new BottomSheetFragment();
                fragment.show(getSupportFragmentManager(), TAG);
            }
        });


    }
}
