package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class OrderDetailsDataResponse {
    @SerializedName("booking_details")
    @Expose
    public List<OrderBookingDetailResponse> bookingDetails = null;
    @SerializedName("booking_data")
    @Expose
    public OrderBookingDataResponse bookingData;
    @SerializedName("address_data")
    @Expose
    public OrderAddressDataResonse addressData;
    @SerializedName("bookinghistory")
    @Expose
    public List<BookinghistoryResponse> bookinghistory = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("bookingDetails", bookingDetails).append("bookingData", bookingData).append("addressData", addressData).append("bookinghistory", bookinghistory).toString();
    }


}
