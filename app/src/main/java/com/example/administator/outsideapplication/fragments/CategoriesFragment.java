package com.example.administator.outsideapplication.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.ConnectivityReceiver;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.activity.MainActivity;
import com.example.administator.outsideapplication.adapters.CategoryRecyclerAdapter;
import com.example.administator.outsideapplication.model.CategoriesResponse;
import com.example.administator.outsideapplication.model.HomeDataResponse;
import com.example.administator.outsideapplication.model.HomeResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoriesFragment extends Fragment {
    RecyclerView recycler_ProductListing;
    ApiInterface apiInterface;
    AppController appController;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_listing, container, false);

        getActivity().setTitle("Categories");
        recycler_ProductListing = view.findViewById(R.id.recycler_ProductListing);
        MainActivity.img_cart.setVisibility(View.GONE);


        boolean isConnected = ConnectivityReceiver.isConnected();
        if (isConnected) {
            getData();

        } else {
            View view1 = inflater.inflate(R.layout.internet, null);
            Button tryButton = view1.findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }

        return view;
    }

    private void getData() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("CityandArea", Context.MODE_PRIVATE);
        String city_id = sharedPreferences.getString("City_id", "");
        String area_id = sharedPreferences.getString("Area", "");

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("wait...");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<HomeResponse> call = apiInterface.HomeData(city_id, area_id);
        call.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                if (response.isSuccessful()) ;
                HomeResponse homeResponse = response.body();
                if (homeResponse.status == 1) {
                    progressDialog.dismiss();
                    HomeDataResponse homeDataResponse = homeResponse.data;
                    List<CategoriesResponse> categoriesResponse = homeDataResponse.services;
                    if (categoriesResponse != null) {
                        recycler_ProductListing.setAdapter(new CategoryRecyclerAdapter(categoriesResponse, getContext()));
                        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
                        recycler_ProductListing.setLayoutManager(gridLayoutManager);
                        recycler_ProductListing.setHasFixedSize(true);
                        recycler_ProductListing.setNestedScrollingEnabled(false);
                    }
                } else if (homeResponse.status == 0) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), homeResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }
}
