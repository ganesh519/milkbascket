package com.example.administator.outsideapplication.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.CommonClass;
import com.example.administator.outsideapplication.ConnectionDetector;
import com.example.administator.outsideapplication.ConnectivityReceiver;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.activity.LoginActivity;
import com.example.administator.outsideapplication.activity.MainActivity;
import com.example.administator.outsideapplication.activity.OtpScreen;
import com.example.administator.outsideapplication.model.SignUpDataResponse;
import com.example.administator.outsideapplication.model.SignUpResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.administator.outsideapplication.fragments.FragmentLogin.view;

public class FragmentSignup extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {
    EditText edit_name, edit_email, edit_mobile;
    Button bt_changepass_update;
    TextInputEditText edit_password, edit_com_password;

    String fname, lname, mobile, email, password, con_password;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String MobilePattern = "[0-9]{10}";
    String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}";
    ApiInterface apiInterface;
    private boolean isConnected;
    LinearLayout linear;
    String otp;
    TextView login;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.signup_fragment, null);

        edit_name = view.findViewById(R.id.edit_name);
        edit_email = view.findViewById(R.id.edit_email);
        edit_mobile = view.findViewById(R.id.edit_mobile);
        edit_password = view.findViewById(R.id.edit_password);
        edit_com_password = view.findViewById(R.id.edit_com_password);
        linear = view.findViewById(R.id.linear);

        login = view.findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.putExtra("signup", "login");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(intent);

            }
        });
        bt_changepass_update = view.findViewById(R.id.bt_changepass_update);
        bt_changepass_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });

        return view;
    }

    private void getData() {

        fname = edit_name.getText().toString();
//        lname = et_lname.getText().toString();
        mobile = edit_mobile.getText().toString();
        email = edit_email.getText().toString();
        password = edit_password.getText().toString();
        con_password = edit_com_password.getText().toString();

//        login();

        if (fname.equals("") || mobile.equals("") || email.equals("") || password.equals("") || con_password.equals("")) {

            Toast.makeText(getActivity(), "Please enter required details", Toast.LENGTH_SHORT).show();
        } else if (!email.matches(emailPattern)) {

            Toast.makeText(getActivity(), "Invalid Email Address", Toast.LENGTH_SHORT).show();
        } else if (!password.equals(con_password)) {
            Toast.makeText(getActivity(), "confirm Password is not matched", Toast.LENGTH_SHORT).show();
        } else if (!mobile.matches(MobilePattern)) {
            Toast.makeText(getContext(), "Please enter valid 10 digit mobile number", Toast.LENGTH_SHORT).show();
        } else {

            login();
        }
    }

    private void login() {

        String value = "1";
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SignUpResponse> call = apiInterface.Signup(fname, email, mobile, password, value, "");
        call.enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                if (response.isSuccessful()) ;
                SignUpResponse signUpResponse = response.body();
                if (signUpResponse.status == 1) {
                    progressDialog.dismiss();
                    SignUpDataResponse signUpDataResponse = signUpResponse.data;
                    otp = String.valueOf(signUpDataResponse.otp);
                    String user_id = String.valueOf(signUpDataResponse.userId);
                    Intent intent = new Intent(getActivity(), OtpScreen.class);
                    intent.putExtra("MOBILE", mobile);
                    intent.putExtra("OTP", otp);
                    intent.putExtra("USER_ID", user_id);
                    getActivity().startActivity(intent);

                } else if (signUpResponse.status == 0) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), signUpResponse.message, Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        // register connection status listener

        AppController.getInstance().setConnectivityListener(this);
    }


    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }

        snackBar(message, color);


    }


    // snackBar
    private void snackBar(String message, int color) {
        Snackbar snackbar = Snackbar.make(view.findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);

    }

}