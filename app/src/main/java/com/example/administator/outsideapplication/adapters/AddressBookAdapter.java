package com.example.administator.outsideapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.activity.AddressActivity;
import com.example.administator.outsideapplication.activity.Edit_address;
import com.example.administator.outsideapplication.activity.Payment_activity;
import com.example.administator.outsideapplication.model.AddressBookDataDetailsResponse;
import com.example.administator.outsideapplication.model.DeleteAddressReponse;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressBookAdapter extends RecyclerView.Adapter<AddressBookAdapter.Holder> {
    List<AddressBookDataDetailsResponse> addressBookDataDetailsResponse;
    Context context;
    private int lastPosition = -1;
    ApiInterface apiInterface;
    String address_id, fname, address, title, mobile, lnmae, email, Daddress, add_type, home,address1;
    SweetAlertDialog sd;
    String Activity;

    public AddressBookAdapter(List<AddressBookDataDetailsResponse> addressBookDataDetailsResponse, Context context, String activity) {
        this.addressBookDataDetailsResponse = addressBookDataDetailsResponse;
        this.context = context;
        this.Activity = activity;
    }

    @NonNull
    @Override
    public AddressBookAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_address, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddressBookAdapter.Holder holder, final int i) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("CityandArea", Context.MODE_PRIVATE);
        final String area = sharedPreferences.getString("area_name", "");
        String city = sharedPreferences.getString("city_name", "");
        final String city_id = sharedPreferences.getString("City_id", "");
        final String area_id = sharedPreferences.getString("Area", "");

        setAnimation(holder.itemView, i);
        holder.name.setText(addressBookDataDetailsResponse.get(i).firstname);
        holder.address.setText(addressBookDataDetailsResponse.get(i).address +  ","+ addressBookDataDetailsResponse.get(i).address1+ "," + area + "," + city);
        holder.mobile.setText(addressBookDataDetailsResponse.get(i).phone);

//        address_id = addressBookDataDetailsResponse.get(i).addressId;
        add_type = addressBookDataDetailsResponse.get(i)._default;
        mobile = addressBookDataDetailsResponse.get(i).phone;
        fname = addressBookDataDetailsResponse.get(i).firstname;
        home = addressBookDataDetailsResponse.get(i).title;
        lnmae = addressBookDataDetailsResponse.get(i).lastname;
        email = addressBookDataDetailsResponse.get(i).email;
        address = addressBookDataDetailsResponse.get(i).address;
        address1=addressBookDataDetailsResponse.get(i).address1;

        Daddress = String.valueOf(addressBookDataDetailsResponse.get(i)._default);


        if (addressBookDataDetailsResponse.get(i)._default.equals("1")) {
            holder.home.setText(addressBookDataDetailsResponse.get(i).title + " (Default)");
            holder.home.setTextColor(Color.parseColor("#018bc8"));
            holder.name.setText(addressBookDataDetailsResponse.get(i).firstname);
            holder.name.setTextColor(Color.parseColor("#018bc8"));
            holder.address.setText(addressBookDataDetailsResponse.get(i).address + "," + area + "," + city);
            holder.address.setTextColor(Color.parseColor("#018bc8"));
            holder.mobile.setText(addressBookDataDetailsResponse.get(i).phone);
            holder.mobile.setTextColor(Color.parseColor("#018bc8"));


            SharedPreferences sharedPreference = context.getSharedPreferences("AddressData", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreference.edit();
            editor.putString("ADDRESS", address);
            editor.putString("ADDRESS_ID", address_id);
            editor.putString("ADDRESS_TYPE", add_type);
            editor.putString("address_mobile", mobile);
            editor.putString("address_name", fname);
            editor.putString("address_home", home);
            editor.putString("address_lname", lnmae);
            editor.putString("address_email", email);
            editor.commit();

        } else {
            holder.home.setText(addressBookDataDetailsResponse.get(i).title);
        }


        if (Activity.equals("PaymentActivity") || Activity.equals("CartActivity")) {
            holder.btnChooseAddress.setVisibility(View.VISIBLE);

        }
        else if (Activity.equals("MyAccount")){
            holder.btnChooseAddress.setVisibility(View.GONE);

        }
        else {
            holder.btnChooseAddress.setVisibility(View.GONE);
        }
        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                add_type = addressBookDataDetailsResponse.get(i)._default;
                mobile = addressBookDataDetailsResponse.get(i).phone;
                fname = addressBookDataDetailsResponse.get(i).firstname;
                home = addressBookDataDetailsResponse.get(i).title;
                lnmae = addressBookDataDetailsResponse.get(i).lastname;
                email = addressBookDataDetailsResponse.get(i).email;
                address = addressBookDataDetailsResponse.get(i).address;
                address1=addressBookDataDetailsResponse.get(i).address1;

                Daddress = String.valueOf(addressBookDataDetailsResponse.get(i)._default);
                address_id = addressBookDataDetailsResponse.get(i).addressId;

                Intent intent = new Intent(context, Edit_address.class);
                Bundle bundle = new Bundle();
                bundle.putString("Fname", fname);
                bundle.putString("Address", address);
                bundle.putString("Title", home);
                bundle.putString("Mobile", mobile);
                bundle.putString("Lname", lnmae);
                bundle.putString("Email", email);
                bundle.putString("Default", Daddress);
                bundle.putString("Address_id", address_id);
                bundle.putString("Address1",address1);
                bundle.putString("Activity", Activity);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);

            }
        });

        holder.btnChooseAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                add_type = addressBookDataDetailsResponse.get(i)._default;
                mobile = addressBookDataDetailsResponse.get(i).phone;
                fname = addressBookDataDetailsResponse.get(i).firstname;
                home = addressBookDataDetailsResponse.get(i).title;
                lnmae = addressBookDataDetailsResponse.get(i).lastname;
                email = addressBookDataDetailsResponse.get(i).email;
                address = addressBookDataDetailsResponse.get(i).address;
                address1=addressBookDataDetailsResponse.get(i).address1;

                Daddress = String.valueOf(addressBookDataDetailsResponse.get(i)._default);
                address_id = addressBookDataDetailsResponse.get(i).addressId;

                Intent intent = new Intent(context,Payment_activity.class);
                Bundle bundle=new Bundle();
                bundle.putString("Fname", fname);
                bundle.putString("Address", address);
                bundle.putString("Title", home);
                bundle.putString("Mobile", mobile);
                bundle.putString("Lname", lnmae);
                bundle.putString("Email", email);
                bundle.putString("Default", Daddress);
                bundle.putString("Address_id", address_id);
                bundle.putString("Address1",address1);
                bundle.putString("choose",Activity);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });


        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                address_id = addressBookDataDetailsResponse.get(i).addressId;
                sd = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                sd.setTitleText("Are you sure?");
                sd.setContentText("This address will be delete from AddressList!");
                sd.setCancelText("No,cancel pls!");
                sd.setConfirmText("Yes,delete it!");
                sd.showCancelButton(true);
                sd.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(final SweetAlertDialog sweetAlertDialog) {
                        apiInterface = ApiClient.getClient().create(ApiInterface.class);
                        Call<DeleteAddressReponse> call = apiInterface.DeleteAddress(city_id, area_id, address_id);
                        call.enqueue(new Callback<DeleteAddressReponse>() {
                            @Override
                            public void onResponse(Call<DeleteAddressReponse> call, Response<DeleteAddressReponse> response) {
                                if (response.isSuccessful()) ;
                                DeleteAddressReponse deleteAddressReponse = response.body();
                                if (deleteAddressReponse.status == 1) {
                                    sweetAlertDialog.cancel();
                                    notifyDataSetChanged();
                                    Toast.makeText(context, deleteAddressReponse.message, Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(context, AddressActivity.class);
                                    intent.putExtra("Activity","AddressBook");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    context.startActivity(intent);

                                    SharedPreferences sharedPreference = v.getContext().getSharedPreferences("AddressData", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor2 = sharedPreference.edit();
                                    editor2.clear();
                                    editor2.commit();


                                } else if (deleteAddressReponse.status == 0) {
                                    sweetAlertDialog.cancel();
                                    Toast.makeText(context, deleteAddressReponse.message, Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<DeleteAddressReponse> call, Throwable t) {
                                sweetAlertDialog.cancel();
                                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                            }
                        });


                    }
                });

                sd.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                });
                sd.show();

            }
        });


    }

    @Override
    public int getItemCount() {
        return addressBookDataDetailsResponse.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView name, address, mobile, home, txt_remove;
        RelativeLayout relative_edit, relative_remove;
        Button txtEdit, btnDelete, btnChooseAddress;

        public Holder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            address = itemView.findViewById(R.id.address);
            mobile = itemView.findViewById(R.id.mobile);
            home = itemView.findViewById(R.id.home);

//            relative_remove = itemView.findViewById(R.id.relative_remove);
            txtEdit = itemView.findViewById(R.id.txtEdit);
            btnDelete = itemView.findViewById(R.id.btnDelete);
            btnChooseAddress = itemView.findViewById(R.id.btnChooseAddress);
        }
    }

    private void setAnimation(View viewToAnimate, int position) {

        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
