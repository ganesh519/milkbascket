package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class PaymentDataResponse {

    @SerializedName("cart")
    @Expose
    public List<PaymentCartResponse> cart = null;
    @SerializedName("cart_count")
    @Expose
    public Integer cartCount;
    @SerializedName("cart_total")
    @Expose
    public PaymentCartTotalResponse cartTotal;
    @SerializedName("selectaddress")
    @Expose
    public SelectDefaultaddressResponse selectaddress;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cart", cart).append("cartCount", cartCount).append("cartTotal", cartTotal).append("selectaddress", selectaddress).toString();
    }
}
