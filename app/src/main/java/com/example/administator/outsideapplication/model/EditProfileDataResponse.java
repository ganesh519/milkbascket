package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class EditProfileDataResponse {

    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("city_id")
    @Expose
    public Object cityId;
    @SerializedName("area_id")
    @Expose
    public Object areaId;
    @SerializedName("facebook")
    @Expose
    public String facebook;
    @SerializedName("twitter")
    @Expose
    public String twitter;
    @SerializedName("linkedin")
    @Expose
    public String linkedin;
    @SerializedName("whatsapp")
    @Expose
    public String whatsapp;
    @SerializedName("success")
    @Expose
    public Integer success;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("firstName", firstName).append("lastName", lastName).append("cityId", cityId).append("areaId", areaId).append("facebook", facebook).append("twitter", twitter).append("linkedin", linkedin).append("whatsapp", whatsapp).append("success", success).toString();
    }

}
