package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class MyOrderBookingResponse {


    @SerializedName("amount")
    @Expose
    public String amount;
    @SerializedName("booking_id")
    @Expose
    public String bookingId;
    @SerializedName("sub_total")
    @Expose
    public String subTotal;
    @SerializedName("additional_charges")
    @Expose
    public String additionalCharges;
    @SerializedName("gst")
    @Expose
    public String gst;
    @SerializedName("coupon_amount")
    @Expose
    public String couponAmount;
    @SerializedName("payment_mode")
    @Expose
    public String paymentMode;
    @SerializedName("date_time")
    @Expose
    public String dateTime;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("address_id")
    @Expose
    public String addressId;
    @SerializedName("booking_type")
    @Expose
    public String bookingType;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("amount", amount).append("bookingId", bookingId).append("subTotal", subTotal).append("additionalCharges", additionalCharges).append("gst", gst).append("couponAmount", couponAmount).append("paymentMode", paymentMode).append("dateTime", dateTime).append("status", status).append("addressId", addressId).append("bookingType", bookingType).toString();
    }


}
