package com.example.administator.outsideapplication.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.activity.CityActivity;
import com.example.administator.outsideapplication.activity.MainActivity;
import com.example.administator.outsideapplication.model.AreaDataDetailsResponse;
import com.example.administator.outsideapplication.model.AreaDataResponse;
import com.example.administator.outsideapplication.model.AreaResponse;
import com.example.administator.outsideapplication.model.Category_spinner;
import com.example.administator.outsideapplication.model.CityDataDetailsResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerCityAdapter extends RecyclerView.Adapter<RecyclerCityAdapter.Holder> {

    List<CityDataDetailsResponse> cityDataDetailsResponses;
    private final Context context;
    ApiInterface apiInterface;
    CustomListAdapter customSpinnerAdapter;
    List<Category_spinner> category_spinner;
    String selected = "";
    String city_name, city_id;
    View view;
    int row_index = 0;

    public RecyclerCityAdapter(List<CityDataDetailsResponse> cityDataDetailsResponses, Context context) {
        this.cityDataDetailsResponses = cityDataDetailsResponses;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerCityAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_city, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerCityAdapter.Holder holder, final int i) {
        holder.city_name.setText(cityDataDetailsResponses.get(i).cityName);
        category_spinner = new ArrayList<>();


        if (row_index == 0) {
            holder.city_name.setTextColor(Color.parseColor("#2596c8"));

            city_id = cityDataDetailsResponses.get(row_index).cityId;
            city_name = cityDataDetailsResponses.get(row_index).cityName;

            getData(view, city_id);
//            notifyDataSetChanged();

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                row_index = i;
                Log.e("POSTIONTHREE", "" + row_index + "----" + i);


                String city_id1 = cityDataDetailsResponses.get(i).cityId;
                city_name = cityDataDetailsResponses.get(i).cityName;

                getData(view, city_id1);

                notifyDataSetChanged();

            }
        });

        if (row_index == i) {

            Log.e("POSTIONONE", "" + row_index + "----" + i);
            holder.city_name.setTextColor(Color.parseColor("#2596c8"));


        } else {
            Log.e("POSTIONTWO", "" + row_index + "----" + i);
            holder.city_name.setTextColor(Color.parseColor("#000000"));
        }

    }

    @Override
    public int getItemCount() {
        return cityDataDetailsResponses.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView city_name;

        public Holder(@NonNull View itemView) {
            super(itemView);
            city_name = itemView.findViewById(R.id.city_name);
        }
    }

    private void getData(final View v, String city_id) {


        final ProgressDialog progressDialog = new ProgressDialog(v.getContext());
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AreaResponse> call = apiInterface.AreaData(city_id);
        call.enqueue(new Callback<AreaResponse>() {
            @Override
            public void onResponse(Call<AreaResponse> call, Response<AreaResponse> response) {
                if (response.isSuccessful()) ;
                AreaResponse areaResponse = response.body();
                if (areaResponse.status == 1) {
                    progressDialog.dismiss();
                    CityActivity.list_area.setVisibility(View.VISIBLE);
                    AreaDataResponse areaDataResponse = areaResponse.data;
                    List<AreaDataDetailsResponse> areaDataDetailsResponse = areaDataResponse.areas;

                    category_spinner = new ArrayList<>();
                    for (int i = 0; i < areaDataDetailsResponse.size(); i++) {
                        Category_spinner category = new Category_spinner();
                        category.setId(areaDataDetailsResponse.get(i).areaId);
                        category.setName(areaDataDetailsResponse.get(i).areaName);
                        category_spinner.add(category);
                    }


                    customSpinnerAdapter = new CustomListAdapter(v.getContext(), R.layout.spinner_rows, category_spinner, selected);
                    CityActivity.list_area.setAdapter(customSpinnerAdapter);

                    CityActivity.list_area.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            view.setSelected(true);
                            String area_id = category_spinner.get(position).getId();
                            selected = category_spinner.get(position).getName();

                            SharedPreferences sharedPreferences = v.getContext().getSharedPreferences("CityandArea", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("Area", area_id);
                            editor.putString("City_id", RecyclerCityAdapter.this.city_id);
                            editor.putString("City_name", city_name);
                            editor.putString("area_name", selected);
                            editor.putString("city_name", city_name);
                            editor.commit();

                            Intent intent = new Intent(v.getContext(), MainActivity.class);
//                                    intent.putExtra("area",area_id);
//                                    intent.putExtra("city",city_id);
                            v.getContext().startActivity(intent);


                        }
                    });

//                            CityActivity.recycler_area.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
//                            CityActivity.recycler_area.setAdapter(new RecyclerAreaAdapter(areaDataDetailsResponse, context));
//                            CityActivity.recycler_area.setHasFixedSize(true);
//                            CityActivity.recycler_area.setNestedScrollingEnabled(false);

                } else if (areaResponse.status == 0) {
                    progressDialog.dismiss();
                    CityActivity.list_area.setVisibility(View.VISIBLE);
                    Toast.makeText(v.getContext(), areaResponse.message, Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<AreaResponse> call, Throwable t) {
                progressDialog.dismiss();
                CityActivity.list_area.setVisibility(View.VISIBLE);
                Toast.makeText(v.getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }
}

