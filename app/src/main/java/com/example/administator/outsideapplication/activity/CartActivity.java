package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.adapters.AddressBookAdapter;
import com.example.administator.outsideapplication.adapters.CartProductsRecyclerAdapter;
import com.example.administator.outsideapplication.adapters.HorizontalAdapter;
import com.example.administator.outsideapplication.model.AddressBookDataDetailsResponse;
import com.example.administator.outsideapplication.model.AddressBookDataResponse;
import com.example.administator.outsideapplication.model.AddressBookResponse;
import com.example.administator.outsideapplication.model.CartDataDetailsresponse;
import com.example.administator.outsideapplication.model.CartDataResponse;
import com.example.administator.outsideapplication.model.CartResponse;
import com.example.administator.outsideapplication.model.CartTotalResponse;

import java.util.List;
import java.util.zip.Inflater;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity {
    Button checkout, empty_shopping;
    TextView view_details, Price_Details, txt_noitems, txt_total_price, txt_total, txtAmountTobepaid, txtDeliver, txtDiscount, txtSubTotal;
    ImageView back;
    ApiInterface apiInterface;
    String user_id;
    ProgressDialog progressDialog;
    RecyclerView cart_recycler;
    RelativeLayout Cart_empty, relative_hide;
    AppController appController;
    CartTotalResponse cartTotalResponse;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_listing);

//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        appController = (AppController) getApplication();
        if (appController.isConnection()) {

            init();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }
    }

    public void init() {

        checkout = findViewById(R.id.checkout);
        view_details = findViewById(R.id.view_details);
        Price_Details = findViewById(R.id.Price_Details);
        cart_recycler = findViewById(R.id.cart_recycler);

        txt_noitems = findViewById(R.id.txt_noitems);
        txt_total_price = findViewById(R.id.txt_total_price);
        txtSubTotal = findViewById(R.id.txtSubTotal);
        txtDiscount = findViewById(R.id.txtDiscount);
        txtDeliver = findViewById(R.id.txtDeliver);
        txtAmountTobepaid = findViewById(R.id.txtAmountTobepaid);
        txt_total = findViewById(R.id.txt_total);

        Cart_empty = findViewById(R.id.Cart_empty);
        empty_shopping = findViewById(R.id.empty_shopping);
        relative_hide = findViewById(R.id.relative_hide);

        progressDialog = new ProgressDialog(CartActivity.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        user_id = sharedPreferences1.getString("user_id", "");

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CartActivity.this, MainActivity.class));
            }
        });

        view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Price_Details.startAnimation(AnimationUtils.loadAnimation(CartActivity.this, R.anim.shake));

            }
        });
        getData();
        empty_shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CartActivity.this, MainActivity.class));
            }
        });


        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cartTotalResponse.addressTotal == 0){

                    Intent intent=new Intent(CartActivity.this,AddAddressActivity.class);
                    intent.putExtra("Activity","CartActivity");
                    startActivity(intent);

                }
                else if (cartTotalResponse.defaultAddress.equalsIgnoreCase("no")){
                        Intent intent=new Intent(CartActivity.this,AddressActivity.class);
                        intent.putExtra("Activity","CartActivity");
                        startActivity(intent);

                    }
                    else {
                        Intent intent=new Intent(CartActivity.this,Payment_activity.class);
                        intent.putExtra("choose","default");
                        startActivity(intent);
                    }
            }
        });

    }

    private void getData() {

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CartResponse> cartResponseCall = apiInterface.CartListing(user_id);
        cartResponseCall.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                if (response.isSuccessful()) ;
                CartResponse cartResponse = response.body();
                if (cartResponse.status == 1) {
                    progressDialog.dismiss();
                    Cart_empty.setVisibility(View.GONE);
                    relative_hide.setVisibility(View.VISIBLE);
                    CartDataResponse cartDataResponse = cartResponse.data;

                    txt_noitems.setText("(" + String.valueOf(cartDataResponse.cartCount + ")"));
                    cartTotalResponse = cartResponse.data.cartTotal;

                    txtDeliver.setText(String.valueOf("₹" + cartTotalResponse.deliveryCharges));
                    txtSubTotal.setText("₹" + cartTotalResponse.total);

                    txtDiscount.setText("₹" + cartTotalResponse.totalDiscount);

                    int subt = Integer.parseInt(cartTotalResponse.total);
//                    int discountT = Integer.parseInt(cartTotalResponse.totalDiscount);
//
//                    String total = String.valueOf(subt - discountT);

//                    int tot = Integer.parseInt(total);
                    int delivery = cartTotalResponse.deliveryCharges;


                    String final_total = String.valueOf(subt + delivery);

                    txt_total.setText("₹" + final_total);
                    txt_total_price.setText("₹" + final_total);
                    txtAmountTobepaid.setText("₹" + final_total);

                    List<CartDataDetailsresponse> cartDataDetailsresponse = cartDataResponse.cartData;
                    cart_recycler.setAdapter(new CartProductsRecyclerAdapter(cartDataDetailsresponse, CartActivity.this));
                    cart_recycler.setLayoutManager(new LinearLayoutManager(CartActivity.this, LinearLayoutManager.VERTICAL, false));
                    cart_recycler.setHasFixedSize(true);
                    cart_recycler.setNestedScrollingEnabled(false);

                } else if (cartResponse.status == 0) {
                    relative_hide.setVisibility(View.GONE);
                    Cart_empty.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    Toast.makeText(CartActivity.this, cartResponse.message, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {
                relative_hide.setVisibility(View.GONE);
                progressDialog.dismiss();
                Toast.makeText(CartActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        startActivity(new Intent(CartActivity.this, MainActivity.class));
    }
}
