package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.model.ForgotOtpResponse;
import com.example.administator.outsideapplication.model.SignupOtpResponse;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordOtpScreen extends AppCompatActivity {
    EditText signup_otp;
    String otp, mobile, code, user_id;
    ApiInterface apiInterface, apiInterface1;
    Button proceed;
    TextView resend;
//    SmsVerifyCatcher smsVerifyCatcher;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_screen);

        signup_otp = findViewById(R.id.signup_otp);
        proceed = findViewById(R.id.proceed);
        resend = findViewById(R.id.resend);

        mobile = getIntent().getStringExtra("Mobile");
        user_id = getIntent().getStringExtra("FU_id");



        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                otp = signup_otp.getText().toString();

                final ProgressDialog progressDialog=new ProgressDialog(ForgotPasswordOtpScreen.this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();

                apiInterface=ApiClient.getClient().create(ApiInterface.class);
                Call<ForgotOtpResponse> call=apiInterface.ForgotOtp(user_id,mobile,otp);
                call.enqueue(new Callback<ForgotOtpResponse>() {
                    @Override
                    public void onResponse(Call<ForgotOtpResponse> call, Response<ForgotOtpResponse> response) {
                        if (response.isSuccessful());
                        ForgotOtpResponse forgotOtpResponse=response.body();

                        if (forgotOtpResponse.status == 1){
                            progressDialog.dismiss();
                            Intent intent = new Intent(ForgotPasswordOtpScreen.this, LoginActivity.class);
                            intent.putExtra("signup", "login");
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                    Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                        else if (forgotOtpResponse.status == 0){
                            progressDialog.dismiss();
                            Toast.makeText(ForgotPasswordOtpScreen.this, forgotOtpResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ForgotOtpResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(ForgotPasswordOtpScreen.this,t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });



            }
        });
    }
//    private String parseCode(String message) {
//        Pattern p = Pattern.compile("\\b\\d{6}\\b");
//        Matcher m = p.matcher(message);
//        String code = "";
//        while (m.find()) {
//            code = m.group(0);
//        }
//        return code;
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        smsVerifyCatcher.onStart();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        smsVerifyCatcher.onStop();
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }

}
