package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class PlaceOrderDataResponse {

    @SerializedName("payment_typeid")
    @Expose
    public String paymentTypeid;
    @SerializedName("payment_type")
    @Expose
    public String paymentType;
    @SerializedName("order_id")
    @Expose
    public Integer orderId;
    @SerializedName("payment_url")
    @Expose
    public String paymentUrl;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("paymentTypeid", paymentTypeid).append("paymentType", paymentType).append("orderId", orderId).append("paymentUrl", paymentUrl).toString();
    }


}
