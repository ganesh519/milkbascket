package com.example.administator.outsideapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.activity.MyOrders;
import com.example.administator.outsideapplication.activity.OrdersListActivity;
import com.example.administator.outsideapplication.model.MyOrderBookingResponse;
import com.github.thunder413.datetimeutils.DateTimeStyle;
import com.github.thunder413.datetimeutils.DateTimeUtils;

import java.util.List;
import java.util.StringTokenizer;

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.Holder> {
    List<MyOrderBookingResponse> myOrderBookingDataResponse;
    Context context;
    private int lastPosition = -1;

    public MyOrdersAdapter(List<MyOrderBookingResponse> myOrderBookingDataResponse, MyOrders myOrders) {
        this.myOrderBookingDataResponse = myOrderBookingDataResponse;
        this.context = myOrders;
    }


    @NonNull
    @Override
    public MyOrdersAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.orderidlayout, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyOrdersAdapter.Holder holder, final int i) {
        setAnimation(holder.itemView, i);
//        holder.txt_amount.setText(myOrderBookingDataResponse.get(i).amount);
//        holder.txt_ptype.setText(myOrderBookingDataResponse.get(i).paymentMode);
//        holder.txt_date.setText(myOrderBookingDataResponse.get(i).dateTime);
//        holder.txt_status.setText(myOrderBookingDataResponse.get(i).status);

        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate random color
        int color1 = generator.getRandomColor();

        holder.cardView.setCardBackgroundColor(color1);

        holder.txtIdTitle.setText("Amount : ");
        holder.txtOrderTitle.setText("ORDER ID : ");
        holder.txtpaymentTitle.setText("Payment Type : ");

        holder.txtOrderId.setText(myOrderBookingDataResponse.get(i).bookingId);
        holder.txtPaymentStatus.setText(myOrderBookingDataResponse.get(i).paymentMode);
        holder.txtId.setText("Rs " + myOrderBookingDataResponse.get(i).amount);

        String date = myOrderBookingDataResponse.get(i).dateTime;
        String fullDate = DateTimeUtils.formatWithStyle(date, DateTimeStyle.MEDIUM); // June 13, 2017

        StringTokenizer stringTokenizer = new StringTokenizer(fullDate);
        String monthDate = stringTokenizer.nextToken(",");
        String year = stringTokenizer.nextToken(",");

        StringTokenizer monthdateToken = new StringTokenizer(monthDate);
        String mm = monthdateToken.nextToken(" ");
        String dd = monthdateToken.nextToken(" ");

        Log.e("TOKEN", "" + mm + dd);

        holder.txtdd.setText(dd);
        holder.txtmm.setText(mm);
        holder.txtyy.setText(year);


        holder.txtViewItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, OrdersListActivity.class);
                intent.putExtra("orderId", myOrderBookingDataResponse.get(i).bookingId);
                intent.putExtra("Activity", "MyOrder");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return myOrderBookingDataResponse.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView txtIdTitle, txtOrderTitle, txtpaymentTitle, txtdd, txtmm, txtyy, txtId, txtOrderId, txtPaymentStatus, txtViewItems;
        CardView cardView;

        public Holder(@NonNull View itemView) {
            super(itemView);
//            txt_status=itemView.findViewById(R.id.txt_status);
//            txt_date=itemView.findViewById(R.id.txt_date);
//            txt_ptype=itemView.findViewById(R.id.txt_ptype);
//            txt_amount=itemView.findViewById(R.id.txt_amount);
//

            txtPaymentStatus = itemView.findViewById(R.id.txtPaymentStatus);
            txtIdTitle = itemView.findViewById(R.id.txtIdTitle);
            txtOrderTitle = itemView.findViewById(R.id.txtOrderTitle);
            txtpaymentTitle = itemView.findViewById(R.id.txtpaymentTitle);
            txtOrderId = itemView.findViewById(R.id.txtOrderId);
            txtId = itemView.findViewById(R.id.txtId);
            txtyy = itemView.findViewById(R.id.txtyy);
            txtmm = itemView.findViewById(R.id.txtmm);
            txtdd = itemView.findViewById(R.id.txtdd);
            cardView = itemView.findViewById(R.id.cardView);

            txtViewItems = itemView.findViewById(R.id.txtViewItems);


        }
    }

    private void setAnimation(View viewToAnimate, int position) {

        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
