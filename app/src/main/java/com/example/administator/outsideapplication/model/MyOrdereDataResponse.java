package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class MyOrdereDataResponse {
    @SerializedName("bookings")
    @Expose
    public List<MyOrderBookingResponse> bookings = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("bookings", bookings).toString();
    }

}
