package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class TermsDataResponse {

    @SerializedName("page")
    @Expose
    public TermsDataDetailsResponse page;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("page", page).toString();
    }
}
