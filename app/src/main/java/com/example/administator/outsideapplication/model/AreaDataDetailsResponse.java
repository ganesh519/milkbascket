package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class AreaDataDetailsResponse {


    @SerializedName("area_id")
    @Expose
    public String areaId;
    @SerializedName("city_id")
    @Expose
    public String cityId;
    @SerializedName("area_name")
    @Expose
    public String areaName;
    @SerializedName("sort_order")
    @Expose
    public String sortOrder;
    @SerializedName("langlat")
    @Expose
    public String langlat;
    @SerializedName("category")
    @Expose
    public String category;
    @SerializedName("sdfgd")
    @Expose
    public boolean isAdded;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("areaId", areaId).append("cityId", cityId).append("areaName", areaName).append("sortOrder", sortOrder).append("langlat", langlat).append("category", category).toString();
    }
}
