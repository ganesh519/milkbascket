package com.example.administator.outsideapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.activity.Productdetails_Activity;
import com.example.administator.outsideapplication.model.ProductsDataResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.Holder> {
    List<ProductsDataResponse> productsDataResponse;
    Context context;
    private int lastPosition = -1;


    public HorizontalAdapter(List<ProductsDataResponse> productsDataResponse, Context context) {
        this.productsDataResponse = productsDataResponse;
        this.context = context;
    }

    @NonNull
    @Override
    public HorizontalAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_horizontal, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HorizontalAdapter.Holder holder, final int i) {
        setAnimation(holder.itemView, i);
        Picasso.get().load("https://www.testocar.in/boloadmin/" + productsDataResponse.get(i).image).placeholder(R.drawable.default_loading).into(holder.image);
        holder.name.setText(productsDataResponse.get(i).title);

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String product_id = productsDataResponse.get(i).prodId;
                Intent intent = new Intent(context, Productdetails_Activity.class);
                intent.putExtra("Product_id", product_id);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return productsDataResponse.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name;

        public Holder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            image = itemView.findViewById(R.id.image);
        }
    }

    private void setAnimation(View viewToAnimate, int position) {

        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
