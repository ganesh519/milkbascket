package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.PrefManager;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.adapters.CancelOrderAdapter;
import com.example.administator.outsideapplication.adapters.MyOrdersAdapter;
import com.example.administator.outsideapplication.adapters.OrdersListAdapter;
import com.example.administator.outsideapplication.model.CancelOrderResponse;
import com.example.administator.outsideapplication.model.MyOrderItem;
import com.example.administator.outsideapplication.model.OrderBookingDetailResponse;
import com.example.administator.outsideapplication.model.OrderDetailsDataResponse;
import com.example.administator.outsideapplication.model.OrderDetailsResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CancelOrderActivity extends AppCompatActivity {
    String[] reasonArray = {"Select Reason", "Wrong Address", "Delivery Issue", "Different Product", "Poor Quality", "Late Delivery", "Order Misplaced", "Other Issue"};
    AppController appController;
    @BindView(R.id.btnCancel)
    Button btnCancel;

    RecyclerView myorderRecycler;

    Button selectallButton;
    Spinner reasonSpinner;
    ProgressBar progressBar;
    private PrefManager pref;
    CancelOrderAdapter cancelOrderAdapter;
    List<MyOrderItem> orderModelList = new ArrayList<>();
    String UserID, orderId, email;

    String strItemId = "";
    String categoryItems = "";
    boolean flagSelectAll = false;
    String buttonValue = "SelectAll";
    String reason;
    String status;
    SharedPreferences preferences;
    int checkboxCount;
    ProgressDialog pd;
    ApiInterface apiInterface;
    String order_id;
    ImageView back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cancel_order);

        order_id=getIntent().getStringExtra("orderId");

        reasonSpinner=findViewById(R.id.reasonSpinner);
        myorderRecycler=findViewById(R.id.myorderRecycler);
        btnCancel=findViewById(R.id.btnCancel);
        back=findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        appController = (AppController) getApplication();
        if (appController.isConnection()) {

            prepareCancelOrderListData(order_id);
            spinner();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });

        }

    }
    private void spinner() {

        // spinner value

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,reasonArray);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        reasonSpinner.setAdapter(aa);
//        ArrayAdapter<String> adapter = new ArrayAdapter<>(CancelOrderActivity.this, android.R.layout.simple_spinner_item, reasonArray);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        //Setting the ArrayAdapter data on the Spinner
//        reasonSpinner.setAdapter(adapter);

        reasonSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position > 0) {
                    reason = reasonArray[position];
                } else {

                    reason = "Select Reason";
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void prepareCancelOrderListData(final String order_id) {

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (reason.equalsIgnoreCase("Select Reason")) {
                    Snackbar.make(myorderRecycler,"Please select Reason *",Snackbar.LENGTH_SHORT).show();
                }
                else {
                    pd = new ProgressDialog(CancelOrderActivity.this);
                    pd.setMessage("Loading....");
                    pd.show();

                    apiInterface= ApiClient.getClient().create(ApiInterface.class);
                    Call<CancelOrderResponse> cancelOrderResponseCall=apiInterface.CancelOrder(order_id,reason,"wwww");
                    cancelOrderResponseCall.enqueue(new Callback<CancelOrderResponse>() {
                        @Override
                        public void onResponse(Call<CancelOrderResponse> call, Response<CancelOrderResponse> response) {
                            if (response.isSuccessful());
                            CancelOrderResponse cancelOrderResponse=response.body();
                            if (cancelOrderResponse.status == 1){
                                pd.dismiss();
//                                Toast.makeText(CancelOrderActivity.this, cancelOrderResponse.message, Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(CancelOrderActivity.this,MyOrders.class);
                                intent.putExtra("Activity","CancelOrderActivity");
                                startActivity(intent);

                            }
                            else if (cancelOrderResponse.status == 0){
                                pd.dismiss();
                                Toast.makeText(CancelOrderActivity.this, cancelOrderResponse.message, Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<CancelOrderResponse> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(CancelOrderActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }

            }
        });



//        Call<OrderDetailsResponse> call = apiInterface.OrderDetails(order_id);
//        call.enqueue(new Callback<OrderDetailsResponse>() {
//            @Override
//            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
//                if (response.isSuccessful());
//                OrderDetailsResponse orderDetailsResponse=response.body();
//                if (orderDetailsResponse.status == 1){
//                    pd.dismiss();
//                    OrderDetailsDataResponse orderDetailsDataResponse = orderDetailsResponse.data;
//                    List<OrderBookingDetailResponse> orderBookingDetailResponse = orderDetailsDataResponse.bookingDetails;
//
//                    cancelOrderAdapter = new CancelOrderAdapter(orderBookingDetailResponse, CancelOrderActivity.this);
//                    LinearLayoutManager gridLayoutManager = new LinearLayoutManager(CancelOrderActivity.this);
//                    myorderRecycler.setLayoutManager(gridLayoutManager);
//                    myorderRecycler.setHasFixedSize(true);
//                    myorderRecycler.setNestedScrollingEnabled(false);
//                    myorderRecycler.setAdapter(cancelOrderAdapter);
//                }
//                else if (orderDetailsResponse.status == 0){
//                    pd.dismiss();
//                    Toast.makeText(CancelOrderActivity.this, orderDetailsResponse.message, Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
//                pd.dismiss();
//                Toast.makeText(CancelOrderActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//
//            }
//        });



    }
}
