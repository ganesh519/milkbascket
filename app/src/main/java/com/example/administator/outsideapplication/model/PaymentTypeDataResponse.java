package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class PaymentTypeDataResponse {


    @SerializedName("payment_type")
    @Expose
    public List<PaymentTypeDataDetailsResponse> paymentType = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("paymentType", paymentType).toString();
    }
}
