package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class SignupOtpDataResponse {


    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("password")
    @Expose
    public String password;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("city_id")
    @Expose
    public Object cityId;
    @SerializedName("area_id")
    @Expose
    public Object areaId;
    @SerializedName("date_time")
    @Expose
    public String dateTime;
    @SerializedName("photo")
    @Expose
    public String photo;
    @SerializedName("facebook")
    @Expose
    public String facebook;
    @SerializedName("twitter")
    @Expose
    public String twitter;
    @SerializedName("linkedin")
    @Expose
    public String linkedin;
    @SerializedName("whatsapp")
    @Expose
    public String whatsapp;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("refference_code")
    @Expose
    public String refferenceCode;
    @SerializedName("otp_status")
    @Expose
    public String otpStatus;
    @SerializedName("otp")
    @Expose
    public String otp;
    @SerializedName("city_name")
    @Expose
    public Object cityName;
    @SerializedName("area_name")
    @Expose
    public Object areaName;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userId", userId).append("firstName", firstName).append("lastName", lastName).append("email", email).append("password", password).append("phone", phone).append("cityId", cityId).append("areaId", areaId).append("dateTime", dateTime).append("photo", photo).append("facebook", facebook).append("twitter", twitter).append("linkedin", linkedin).append("whatsapp", whatsapp).append("status", status).append("refferenceCode", refferenceCode).append("otpStatus", otpStatus).append("otp", otp).append("cityName", cityName).append("areaName", areaName).toString();
    }
}
