package com.example.administator.outsideapplication.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.example.administator.outsideapplication.R;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("CityandArea", Context.MODE_PRIVATE);
                String city_id = sharedPreferences.getString("City_id", "");
                String area_id = sharedPreferences.getString("Area", "");

                if (city_id.equals("") & area_id.equals("")) {
                    Intent i = new Intent(SplashActivity.this, CityActivity.class);
                    i.putExtra("Activity", "Splash");
                    startActivity(i);
                    finish();
                } else {

                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();

                }


            }

        }, SPLASH_TIME_OUT);
    }
}
