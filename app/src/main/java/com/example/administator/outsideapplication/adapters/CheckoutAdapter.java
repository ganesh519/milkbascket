package com.example.administator.outsideapplication.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.activity.CheckoutActivity;
import com.example.administator.outsideapplication.model.CartDataDetailsresponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CheckoutAdapter extends RecyclerView.Adapter<CheckoutAdapter.Holder> {
    List<CartDataDetailsresponse> cartDataDetailsresponse;
    Context context;

    public CheckoutAdapter(List<CartDataDetailsresponse> cartDataDetailsresponse, CheckoutActivity checkoutActivity) {
        this.cartDataDetailsresponse = cartDataDetailsresponse;
        this.context = checkoutActivity;
    }

    @NonNull
    @Override
    public CheckoutAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_checkout, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CheckoutAdapter.Holder holder, int i) {

        Picasso.get().load("https://www.testocar.in/boloadmin/" + cartDataDetailsresponse.get(i).image).placeholder(R.drawable.default_loading).into(holder.p_image);


    }

    @Override
    public int getItemCount() {
        return cartDataDetailsresponse.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView txt_date;
        ImageView p_image;


        public Holder(@NonNull View itemView) {
            super(itemView);

            p_image = itemView.findViewById(R.id.p_image);
            txt_date = itemView.findViewById(R.id.txt_date);
        }
    }
}
