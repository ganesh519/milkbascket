package com.example.administator.outsideapplication.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.ConnectivityReceiver;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.activity.MainActivity;
import com.example.administator.outsideapplication.adapters.ProductListingAdapter;
import com.example.administator.outsideapplication.model.Album;
import com.example.administator.outsideapplication.model.ProductListingDataDetailsResponse;
import com.example.administator.outsideapplication.model.ProductListingDataResponse;
import com.example.administator.outsideapplication.model.ProductListingResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListingFragment extends Fragment {
    ApiInterface apiInterface;
    RecyclerView recycler_ProductListing;
    ProductListingAdapter productListingAdapter;
    TextView text_nodata;
    AppController appController;
    String service;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_listing, container, false);

        service = getArguments().getString("Service_name");
        String name=getArguments().getString("category_name");
        getActivity().setTitle(name);

        recycler_ProductListing = view.findViewById(R.id.recycler_ProductListing);

        MainActivity.img_cart.setVisibility(View.GONE);
        text_nodata = view.findViewById(R.id.text_nodata);

        boolean isConnected = ConnectivityReceiver.isConnected();
        if (isConnected) {

            getData();

        } else {
            View view1 = inflater.inflate(R.layout.internet, null);
            Button tryButton = view1.findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }

        return view;
    }

    private void getData() {


        String city_id = MainActivity.city_id;
        String area_id = MainActivity.area_id;

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProductListingResponse> call = apiInterface.ProductListing(city_id, area_id, service);
        call.enqueue(new Callback<ProductListingResponse>() {
            @Override
            public void onResponse(Call<ProductListingResponse> call, Response<ProductListingResponse> response) {
                if (response.isSuccessful()) ;
                ProductListingResponse productListingResponse = response.body();
                if (productListingResponse.status == 1) {
                    text_nodata.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    ProductListingDataResponse productListingDataResponse = productListingResponse.data;
                    List<ProductListingDataDetailsResponse> productListingDataDetailsResponse = productListingDataResponse.products;
                    if (productListingDataDetailsResponse != null) {
                        productListingAdapter = new ProductListingAdapter(productListingDataDetailsResponse, getActivity());
                        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
                        recycler_ProductListing.setLayoutManager(gridLayoutManager);
                        recycler_ProductListing.setHasFixedSize(true);
                        recycler_ProductListing.setNestedScrollingEnabled(false);
                        recycler_ProductListing.setAdapter(productListingAdapter);
                    } else {
                        recycler_ProductListing.setVisibility(View.GONE);
                        text_nodata.setVisibility(View.VISIBLE);
                    }

                } else if (productListingResponse.status == 0) {
//                    recycler_ProductListing.setVisibility(View.GONE);
                    text_nodata.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), productListingResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProductListingResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
