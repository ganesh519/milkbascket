package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.adapters.MyOrdersAdapter;
import com.example.administator.outsideapplication.model.MyOrderBookingResponse;
import com.example.administator.outsideapplication.model.MyOrdereDataResponse;
import com.example.administator.outsideapplication.model.MyOrdereResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrders extends AppCompatActivity {
    RecyclerView recycler_myorders;
    ApiInterface apiInterface;
    MyOrdersAdapter myOrdersAdapter;
    ProgressDialog pd;
    String user_id,activity;
    ImageView back;
    AppController appController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_orders);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        recycler_myorders = findViewById(R.id.recycler_myorders);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity.equals("CancelOrderActivity")){
                    startActivity(new Intent(MyOrders.this, MainActivity.class));

                }
                if (activity.equals("MyAccount")){
                    startActivity(new Intent(MyOrders.this, MainActivity.class));
                }
                if (activity.equals("MyOrderList")){
                    startActivity(new Intent(MyOrders.this, MainActivity.class));
                }
            }
        });

        appController = (AppController) getApplication();
        if (appController.isConnection()) {

            getData();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }
    }

    private void getData() {

        SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        user_id = sharedPreferences1.getString("user_id", "");
        String city_id = MainActivity.city_id;
        String area_id = MainActivity.area_id;

        pd = new ProgressDialog(MyOrders.this);
        pd.setMessage("Loading....");
        pd.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<MyOrdereResponse> call = apiInterface.MyOrders(city_id, area_id, user_id);
        call.enqueue(new Callback<MyOrdereResponse>() {
            @Override
            public void onResponse(Call<MyOrdereResponse> call, Response<MyOrdereResponse> response) {
                if (response.isSuccessful()) ;
                MyOrdereResponse myOrdereResponse = response.body();
                if (myOrdereResponse.status == 1) {
                    pd.dismiss();
                    MyOrdereDataResponse myOrdereDataResponse = myOrdereResponse.data;
                    List<MyOrderBookingResponse> myOrderBookingDataResponse = myOrdereDataResponse.bookings;

                    myOrdersAdapter = new MyOrdersAdapter(myOrderBookingDataResponse, MyOrders.this);
                    LinearLayoutManager gridLayoutManager = new LinearLayoutManager(MyOrders.this);
                    recycler_myorders.setLayoutManager(gridLayoutManager);
                    recycler_myorders.setHasFixedSize(true);
                    recycler_myorders.setNestedScrollingEnabled(false);
                    recycler_myorders.setAdapter(myOrdersAdapter);
                } else if (myOrdereResponse.status == 0) {
                    pd.dismiss();
                    Toast.makeText(MyOrders.this, myOrdereResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MyOrdereResponse> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MyOrders.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        if (getIntent().getExtras() != null) {
            activity = getIntent().getStringExtra("Activity");
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (activity.equals("CancelOrderActivity")){
            startActivity(new Intent(MyOrders.this, MainActivity.class));

        }
        if (activity.equals("MyAccount")){
            startActivity(new Intent(MyOrders.this, MainActivity.class));
        }
        if (activity.equals("MyOrderList")){
            startActivity(new Intent(MyOrders.this, MainActivity.class));
        }
    }
}
