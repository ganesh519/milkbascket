package com.example.administator.outsideapplication.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;


import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.activity.OrdersListActivity;
import com.example.administator.outsideapplication.model.OrderBookingDetailResponse;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;


public class OrdersListAdapter extends RecyclerView.Adapter<OrdersListAdapter.MyViewHolder> {
    private Context mContext;
    List<OrderBookingDetailResponse> orderBookingDetailResponse;
    /*private static AdapterCallbackOrders itemListener;*/
    private Activity parentActivity;
    String ratingvalue = "";
    Dialog dialog;

    public OrdersListAdapter(List<OrderBookingDetailResponse> orderBookingDetailResponse, OrdersListActivity ordersListActivity) {
        this.mContext = mContext;
        this.orderBookingDetailResponse = orderBookingDetailResponse;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView orderImage;
        TextView txtOrderName, txtOrderID, txtOrderPrice, txtStatus, txtReview, txtQty;
        LinearLayout reviewLayout;


        public MyViewHolder(View itemView) {
            super(itemView);

            orderImage = (ImageView) itemView.findViewById(R.id.imgOrder);
            txtOrderName = (TextView) itemView.findViewById(R.id.txtOrderName);
            txtOrderID = (TextView) itemView.findViewById(R.id.txtOrderID);
            txtOrderPrice = (TextView) itemView.findViewById(R.id.txtOrderPrice);
//            txtStatus = (TextView) itemView.findViewById(R.id.txtStatus);
            reviewLayout = (LinearLayout) itemView.findViewById(R.id.reviewLayout);
            txtReview = (TextView) itemView.findViewById(R.id.txtReview);
            txtQty = (TextView) itemView.findViewById(R.id.txtQty);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.myorderlistmodel1, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {


        Picasso.get().load("https://www.testocar.in/boloadmin/" + orderBookingDetailResponse.get(position).productImage).placeholder(R.drawable.default_loading).into(holder.orderImage);

        holder.txtOrderID.setText("OrderId : " + orderBookingDetailResponse.get(position).productId);
        holder.txtOrderPrice.setText("Rs " + orderBookingDetailResponse.get(position).price);
        holder.txtQty.setText("Quantity : " + orderBookingDetailResponse.get(position).quantity);
        holder.txtOrderName.setText("Quantity : " + orderBookingDetailResponse.get(position).productName);

//        if (orderListBean.getOrder_status().equalsIgnoreCase("confirm")) {
//            holder.txtStatus.setText("Status : " + "confirmed");
//        } else if (orderListBean.getOrder_status().equalsIgnoreCase("return")) {
//
//            holder.txtStatus.setText("Status : " + "returned");
//
//        } else {
//            holder.txtStatus.setText("Status : " + orderListBean.getOrder_status());
//        }
//
//
//        if (orderListBean.getOrder_status().equalsIgnoreCase("delivered")) {
//
//            holder.reviewLayout.setVisibility(View.VISIBLE);
//
//            try {
//
//                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
//
//                Calendar c = Calendar.getInstance();
//                c.setTime(dateFormat.parse(orderListBean.getDate_of_order()));
//                c.add(Calendar.HOUR, 24);
//
//                Date resultdate = new Date(c.getTimeInMillis());
//                //Get current time
//                Date CurrentTime = dateFormat.parse(dateFormat.format(new Date()));
//
//                if (CurrentTime.compareTo(resultdate) > 0) {
//
//
//                    holder.txtStatus.setText("Status : " + "Completed");
//
//                }
//               /* else {
//                    holder.reviewLayout.setVisibility(View.GONE);
//                }*/
//
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//        } else {
//            holder.reviewLayout.setVisibility(View.GONE);
//        }

//        if (orderListBean.getCategory().equalsIgnoreCase("homekitchen")) {
//            holder.reviewLayout.setVisibility(View.GONE);
//        }


//        holder.txtReview.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//               // itemListener.onMethodCallback(v, position);
//
//                dialog =new Dialog(parentActivity);
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//                dialog.setContentView(R.layout.review_dilogue_layout);
//
//                RatingBar ratingBar = (RatingBar)dialog.findViewById(R.id.ratingBar);
//                Button btnSubmit =(Button)dialog.findViewById(R.id.btnSubmit);
//                final EditText etReview =(EditText)dialog.findViewById(R.id.etReview);
//
//                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
//                    @Override
//                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
//                        ratingvalue = String.valueOf(rating);
//                    }
//                });
//
//                btnSubmit.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                       PrefManager pref = new PrefManager(parentActivity);
//
//                        HashMap<String, String> profile = pref.getUserDetails();
//                        String userId = profile.get("id");
//                        String username = profile.get("name");
//                        String email = profile.get("email");
//
//                        String review =etReview.getText().toString();
//
//                        if (review.isEmpty() || ratingvalue.equalsIgnoreCase("")){
//
//                            Toast.makeText(mContext, "Please Give Your Review", Toast.LENGTH_SHORT).show();
//                        } else{
//
//
//                            prepareReviewData(view,orderListBean.getCategory(),userId,orderListBean.getProduct_id(),username,email,ratingvalue,review);
//
//                        }
//                    }
//                });
//
//
//                dialog.show();
//
//            }
//        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return orderBookingDetailResponse.size();
    }


    private void prepareReviewData(final View view, final String module, final String userid, final String productId, final String username, final String email, final String rating, final String message) {


//        Call<ReviewResponse> call = RetrofitClient.getInstance().getApi().reviewOrder( module, userid, productId, username, email, rating, message);
//
//        call.enqueue(new Callback<ReviewResponse>() {
//            @Override
//            public void onResponse(Call<ReviewResponse> call, retrofit2.Response<ReviewResponse> response) {
//
//                ReviewResponse reviewResponse = response.body();
//
//                if (response.isSuccessful()) {
//
//                    if ((reviewResponse != null ? reviewResponse.getStatus() : 0) ==1){
//
//                        Snackbar.make(view,reviewResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
//
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                dialog.dismiss();
//
//                            }
//                        }, 2000);
//
//                    }
//
//
//
//
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<ReviewResponse> call, Throwable t) {
//
//
//            }
//        });
    }


}
