package com.example.administator.outsideapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.activity.Productdetails_Activity;
import com.example.administator.outsideapplication.model.Album;
import com.example.administator.outsideapplication.model.ProductListingDataDetailsResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ProductListingAdapter extends RecyclerView.Adapter<ProductListingAdapter.Holder> {
    List<ProductListingDataDetailsResponse> productListingDataDetailsResultResponse;
    Context context;
    private int lastPosition = -1;


    public ProductListingAdapter(List<ProductListingDataDetailsResponse> productListingDataDetailsResponse, Context context) {
        this.productListingDataDetailsResultResponse = productListingDataDetailsResponse;
        this.context = context;
    }

    @NonNull
    @Override
    public ProductListingAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_productlisting, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListingAdapter.Holder holder, final int i) {

        setAnimation(holder.itemView, i);
        holder.prd_name.setText(productListingDataDetailsResultResponse.get(i).name);
        holder.prd_dprice.setText("Rs " + productListingDataDetailsResultResponse.get(i).price);
        holder.prd_dprice.setPaintFlags(holder.prd_dprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.prd_price.setText("Rs " + productListingDataDetailsResultResponse.get(i).discountPrice);

        Picasso.get().load("https://www.testocar.in/boloadmin/" + productListingDataDetailsResultResponse.get(i).image).placeholder(R.drawable.default_loading).into(holder.product_image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, Productdetails_Activity.class);
                intent.putExtra("Product_id", productListingDataDetailsResultResponse.get(i).prodId);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return productListingDataDetailsResultResponse.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView product_image;
        TextView prd_price, prd_dprice, prd_name;

        public Holder(@NonNull View itemView) {
            super(itemView);
            prd_name = itemView.findViewById(R.id.prd_name);
            prd_price = itemView.findViewById(R.id.prd_price);
            prd_dprice = itemView.findViewById(R.id.prd_dprice);
            product_image = itemView.findViewById(R.id.product_image);
        }
    }

    private void setAnimation(View viewToAnimate, int position) {

        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
