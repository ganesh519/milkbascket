package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CartProductDeleteDataResponse {

    @SerializedName("cart_data")
    @Expose
    public Boolean cartData;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cartData", cartData).toString();
    }

}
