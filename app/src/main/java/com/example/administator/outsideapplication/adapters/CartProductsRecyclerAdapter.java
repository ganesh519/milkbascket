package com.example.administator.outsideapplication.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.NetworkChecking;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.activity.CartActivity;
import com.example.administator.outsideapplication.activity.Productdetails_Activity;
import com.example.administator.outsideapplication.model.AddCartResponse;
import com.example.administator.outsideapplication.model.CartDataDetailsresponse;
import com.example.administator.outsideapplication.model.CartProductDeleteResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartProductsRecyclerAdapter extends RecyclerView.Adapter<CartProductsRecyclerAdapter.Holder> {
    List<CartDataDetailsresponse> cartDataDetailsresponse;
    Context context;
    int count = 1;
    String cart_id, user_id, pid;
    SweetAlertDialog sd;
    ApiInterface apiInterface;
    private boolean checkInternet;

    public CartProductsRecyclerAdapter(List<CartDataDetailsresponse> cartDataDetailsresponse, CartActivity cartActivity) {
        this.cartDataDetailsresponse = cartDataDetailsresponse;
        this.context = cartActivity;
    }
    @NonNull
    @Override
    public CartProductsRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cart, viewGroup, false);
        return new Holder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull final CartProductsRecyclerAdapter.Holder holder, final int i) {

        Picasso.get().load("https://www.testocar.in/boloadmin/" + cartDataDetailsresponse.get(i).image).placeholder(R.drawable.default_loading).into(holder.product_img);

        holder.product_name.setText(cartDataDetailsresponse.get(i).title);
        holder.txt_dprice.setText("₹" + cartDataDetailsresponse.get(i).actualPrice);
        holder.txt_dprice.setPaintFlags(holder.txt_dprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.price_txt.setText("₹" + cartDataDetailsresponse.get(i).discountPrice);

        holder.cart_qty_txt.setText(cartDataDetailsresponse.get(i).qty);

        SharedPreferences sharedPreferences1 = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        user_id = sharedPreferences1.getString("user_id", "");

        holder.cart_add_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                pid = cartDataDetailsresponse.get(i).prodId;
                String qtystr = holder.cart_qty_txt.getText().toString();
                int qty = Integer.parseInt(qtystr);
                if (qty != 0)
                    qty++;
                final String value = String.valueOf(qty);
                String action = "add";
                holder.progressbar_addincrease.setVisibility(View.VISIBLE);
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AddCartResponse> cartResponseCall = apiInterface.AddToCart(value, user_id, pid, action);

                cartResponseCall.enqueue(new Callback<AddCartResponse>() {
                    @Override
                    public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                        if (response.isSuccessful()) ;
                        AddCartResponse addCartResponse = response.body();
                        if (addCartResponse.status == 1) {

//                    int Dec_qun=addCartResponse.data.count;
                            holder.progressbar_addincrease.setVisibility(View.GONE);
                            ((CartActivity) context).init();
                            holder.cart_qty_txt.setText(value);

                        } else if (addCartResponse.status == 0) {
                            holder.progressbar_addincrease.setVisibility(View.GONE);
                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<AddCartResponse> call, Throwable t) {
                        holder.progressbar_addincrease.setVisibility(View.GONE);
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        holder.cart_minus_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pid = cartDataDetailsresponse.get(i).prodId;
                final String qtystr = holder.cart_qty_txt.getText().toString();
                int qty = Integer.parseInt(qtystr);
                if (qty > 1)
                    qty--;
                final String value = String.valueOf(qty);
                String action = "dec";
                holder.progressbar_addincrease.setVisibility(View.VISIBLE);
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AddCartResponse> cartResponseCall = apiInterface.AddToCart(value, user_id, pid, action);
                final int finalQty = qty;
                cartResponseCall.enqueue(new Callback<AddCartResponse>() {
                    @Override
                    public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                        if (response.isSuccessful()) ;
                        AddCartResponse addCartResponse = response.body();
                        if (addCartResponse.status == 1) {
                            holder.progressbar_addincrease.setVisibility(View.GONE);
                            ((CartActivity) context).init();
                            holder.cart_qty_txt.setText(value);
                            int qq = Integer.valueOf(qtystr);
                            if (qq == 1) {
                                sd = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);

                                sd.setTitleText("Are you sure?");
                                sd.setContentText("This product will be delete from cart!");
                                sd.setCancelText("No,cancel pls!");
                                sd.setConfirmText("Yes,delete it!");
                                sd.showCancelButton(true);
                                sd.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(final SweetAlertDialog sweetAlertDialog) {
                                        cart_id = cartDataDetailsresponse.get(i).cartId;
                                        getData(cart_id);
                                    }
                                });

                                sd.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.cancel();
                                    }

                                });
                                sd.show();
                            }


                        } else if (addCartResponse.status == 0) {
                            holder.progressbar_addincrease.setVisibility(View.GONE);
                            Toast.makeText(context, addCartResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddCartResponse> call, Throwable t) {
                        holder.progressbar_addincrease.setVisibility(View.GONE);
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        holder.delete_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    sd = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                    sd.setTitleText("Are you sure?");
                    sd.setContentText("This product will be delete from cart!");
                    sd.setCancelText("No,cancel pls!");
                    sd.setConfirmText("Yes,delete it!");
                    sd.showCancelButton(true);
//                    sd.getProgressHelper().setBarColor(Color.parseColor("#2596c8"));
                    sd.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(final SweetAlertDialog sweetAlertDialog) {

                            cart_id = cartDataDetailsresponse.get(i).cartId;
                            getData(cart_id);
                        }
                    });

                    sd.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    });
                    sd.show();
                } else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartDataDetailsresponse.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        ImageView product_img, delete_img, cart_minus_img, cart_add_img;
        TextView cart_qty_txt, price_txt, txt_dprice, product_name;
        ProgressBar progressbar_addincrease;

        public Holder(@NonNull View itemView) {
            super(itemView);

            product_name = itemView.findViewById(R.id.product_name);
            txt_dprice = itemView.findViewById(R.id.txt_dprice);
            price_txt = itemView.findViewById(R.id.price_txt);
            cart_qty_txt = itemView.findViewById(R.id.cart_qty_txt);

            cart_add_img = itemView.findViewById(R.id.cart_add_img);
            cart_minus_img = itemView.findViewById(R.id.cart_minus_img);
            product_img = itemView.findViewById(R.id.product_img);
            delete_img = itemView.findViewById(R.id.delete_img);
            progressbar_addincrease = itemView.findViewById(R.id.progressbar_addincrease);
        }
    }
    private void getData(String cart_id) {

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CartProductDeleteResponse> cartProductDeleteResponseCall = apiInterface.CartProductDelete(cart_id, "delete");
        cartProductDeleteResponseCall.enqueue(new Callback<CartProductDeleteResponse>() {
            @Override
            public void onResponse(Call<CartProductDeleteResponse> call, Response<CartProductDeleteResponse> response) {
                if (response.isSuccessful()) ;
                CartProductDeleteResponse cartProductDeleteResponse = response.body();
                if (cartProductDeleteResponse.status == 1) {
                    sd.cancel();
                    ((CartActivity) context).init();
//                    Toast.makeText(context, cartProductDeleteResponse.message, Toast.LENGTH_SHORT).show();
                } else if (cartProductDeleteResponse.status == 0) {
                    sd.cancel();
                    Toast.makeText(context, cartProductDeleteResponse.message, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<CartProductDeleteResponse> call, Throwable t) {
                sd.cancel();
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}