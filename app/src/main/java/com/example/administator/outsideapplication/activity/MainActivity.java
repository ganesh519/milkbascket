package com.example.administator.outsideapplication.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.fragments.CategoriesFragment;
import com.example.administator.outsideapplication.fragments.HomeFragment;
import com.example.administator.outsideapplication.model.CartCountDataResponse;
import com.example.administator.outsideapplication.model.CartCountResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    FragmentManager fragmentManager;
    Fragment currentFragment;
    FragmentTransaction fragmentTransaction;
    public static String city_id;
    public static String area_id;
    LinearLayout linear_home, linear_categories, linear_cart, linear_aacount, linear_contact, linear_refund, linear_privacy,
            linear_terms, linear_about, linear_share, linear_logout;
    public static DrawerLayout drawer;
    LinearLayout nav_header_without_Login;
    RelativeLayout Login_withou, signup_withou;
    TextView name, email, city, city1;
    RelativeLayout relative_city, relative_city1;
    public static RelativeLayout img_cart;
    String user_id;
    public static TextView txt_count;
    public static ApiInterface apiInterface;
    AppController appController;
    private Boolean exit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Dukhaan");
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        img_cart = findViewById(R.id.img_cart);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        appController = (AppController) getApplication();
        if (appController.isConnection()) {

            getData();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }


    }

    private void getData() {

        initxml();
    }

    private void initxml() {

//        city_id=getIntent().getStringExtra("city");
//         area_id=getIntent().getStringExtra("area");

        SharedPreferences sharedPreference1 = getApplicationContext().getSharedPreferences("CityandArea", Context.MODE_PRIVATE);
        String cityname = sharedPreference1.getString("City_name", "");
        city_id = sharedPreference1.getString("City_id", "");
        area_id = sharedPreference1.getString("Area", "");


        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String name1 = sharedPreferences.getString("Name", "");
        String phone = sharedPreferences.getString("Phone", "");


        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout, new HomeFragment(), "homee");
        fragmentTransaction.commit();

        linear_home = findViewById(R.id.linear_home);
        linear_categories = findViewById(R.id.linear_categories);
        linear_cart = findViewById(R.id.linear_cart);
        linear_aacount = findViewById(R.id.linear_aacount);
        linear_contact = findViewById(R.id.linear_contact);
        linear_refund = findViewById(R.id.linear_refund);
        linear_privacy = findViewById(R.id.linear_privacy);
        linear_terms = findViewById(R.id.linear_terms);
        linear_about = findViewById(R.id.linear_about);
        linear_share = findViewById(R.id.linear_share);
        linear_logout = findViewById(R.id.linear_logout);

        nav_header_without_Login = findViewById(R.id.nav_header_without_Login);
        signup_withou = findViewById(R.id.signup_withou);
        Login_withou = findViewById(R.id.Login_withou);

        email = findViewById(R.id.email);
        name = findViewById(R.id.name);

        relative_city = findViewById(R.id.relative_city);
        relative_city1 = findViewById(R.id.relative_city1);
        city = findViewById(R.id.city);
        city1 = findViewById(R.id.city1);
        txt_count = findViewById(R.id.txt_count);

        SharedPreferences sharedPreference = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        user_id = sharedPreference.getString("user_id", "");
        if (!TextUtils.isEmpty(user_id)) {
            linear_home.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fl_main_framelayout, new HomeFragment(), "homee");
                    fragmentTransaction.commit();
                    drawer.closeDrawer(GravityCompat.START);
                }
            });

            linear_categories.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fl_main_framelayout, new CategoriesFragment(), "category");
                    fragmentTransaction.commit();
                }
            });
            linear_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                    startActivity(new Intent(MainActivity.this, CartActivity.class));
                }
            });

            linear_contact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                    startActivity(new Intent(MainActivity.this, Contact_us.class));

                }
            });

            linear_aacount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                    startActivity(new Intent(MainActivity.this, MyAccount.class));

                }
            });

            linear_refund.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                }
            });

            linear_privacy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                }
            });

            linear_terms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                    startActivity(new Intent(MainActivity.this, Terms_conditions.class));
                }
            });

            linear_about.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                    startActivity(new Intent(MainActivity.this, About_us.class));
                }
            });
            linear_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                    try {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, "application");
                        String sAux = "\nPlease install this application \n\n";
                        sAux = sAux + "https://play.google.com/store/apps/details?id=com.milkbasket.app&hl=en";
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "choose one"));
                    } catch (Exception e) {
                        //e.toString();
                    }
                }
            });

            linear_logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);

                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.clear();
                    editor.commit();

                    SharedPreferences sharedPreferences1 = v.getContext().getSharedPreferences("CityandArea", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor1 = sharedPreferences1.edit();
                    editor1.clear();
                    editor1.commit();

                    SharedPreferences sharedPreference = v.getContext().getSharedPreferences("AddressData", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor2 = sharedPreference.edit();
                    editor2.clear();
                    editor2.commit();

                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    intent.putExtra("signup", "login");
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK |
                            Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                }
            });

            name.setText(name1);
            email.setText(phone);
            city.setText(cityname);
            relative_city.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(MainActivity.this, CityActivity.class);
                    i.putExtra("Activity","MainActivity");
                    startActivity(i);
                    finish();
                }
            });

        }
        else {

            linear_cart.setVisibility(View.GONE);
            linear_aacount.setVisibility(View.GONE);
            linear_logout.setVisibility(View.GONE);

            nav_header_without_Login.setVisibility(View.VISIBLE);
            Login_withou.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    intent.putExtra("signup", "login");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    drawer.closeDrawer(GravityCompat.START);
//                    finish();
                    startActivity(intent);

                }
            });
            signup_withou.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    intent.putExtra("signup", "Sign");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    drawer.closeDrawer(GravityCompat.START);
//                    finish();
                    startActivity(intent);


                }
            });
            linear_home.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fl_main_framelayout, new HomeFragment(), "homee");
                    fragmentTransaction.commit();
                    drawer.closeDrawer(GravityCompat.START);
                }
            });

            linear_categories.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fl_main_framelayout, new CategoriesFragment(), "category");
                    fragmentTransaction.commit();
                }
            });

            linear_contact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                    startActivity(new Intent(MainActivity.this, Contact_us.class));

                }
            });
            linear_refund.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                }
            });

            linear_privacy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                }
            });

            linear_terms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                    startActivity(new Intent(MainActivity.this, Terms_conditions.class));
                }
            });

            linear_about.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                    startActivity(new Intent(MainActivity.this, About_us.class));
                }
            });
            linear_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(GravityCompat.START);
                    try {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, "application");
                        String sAux = "\nPlease install this application \n\n";
                        sAux = sAux + "https://play.google.com/store/apps/details?id=com.milkbasket.app&hl=en";
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "choose one"));
                    } catch (Exception e) {
                        //e.toString();
                    }
                }
            });

            city1.setText(cityname);
            relative_city1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(MainActivity.this, CityActivity.class);
                    i.putExtra("Activity", "MainActivity");
                    startActivity(i);
                    finish();

                }
            });

        }

        if (user_id.equals("")) {
            img_cart.setVisibility(View.GONE);
        } else {
            img_cart.setVisibility(View.VISIBLE);
            CartCount(user_id);
        }
    }

    @Override
    public void onBackPressed() {
        currentFragment = fragmentManager.findFragmentById(R.id.fl_main_framelayout);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (currentFragment.getTag().equals("homee")) {

//            showAlert();
            if (exit) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                moveTaskToBack(true);
            } else {
                Toast.makeText(getApplicationContext(), "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
                exit = true;
            }
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showAlert() {


        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        alertDialog.setTitle("Exit");
        alertDialog.setMessage("Are you sure you want close this application?");


        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(DialogInterface dialog, int which) {
//                startActivity(new Intent(ctx, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
//                sessionManager.logoutUser();
                finishAffinity();
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    public void CartCount(String user_id) {
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CartCountResponse> cartCountResponseCall = apiInterface.CartCount(user_id);
        cartCountResponseCall.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()) ;
                CartCountResponse cartCountResponse = response.body();
                if (cartCountResponse.status == 1) {
                    CartCountDataResponse cartCountDataResponse = cartCountResponse.data;
                    final int count = cartCountDataResponse.cartCount;
                    txt_count.setText(String.valueOf(count));

                    img_cart.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (count == 0) {
                                Toast.makeText(MainActivity.this, "No Products in Cart", Toast.LENGTH_SHORT).show();
                            } else {
                                startActivity(new Intent(MainActivity.this, CartActivity.class));
                            }
                        }
                    });
                } else if (cartCountResponse.status == 0) {
                    Toast.makeText(MainActivity.this, cartCountResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

}
