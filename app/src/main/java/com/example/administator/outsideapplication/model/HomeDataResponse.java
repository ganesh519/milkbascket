package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class HomeDataResponse {
    @SerializedName("services")
    @Expose
    public List<CategoriesResponse> services = null;
    @SerializedName("sliders")
    @Expose
    public List<SliderResponse> sliders = null;
    @SerializedName("home_banners")
    @Expose
    public List<HomeBannerResponse> homeBanners = null;
    @SerializedName("hp_products")
    @Expose
    public List<ProductsRespose> hpProducts = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("services", services).append("sliders", sliders).append("homeBanners", homeBanners).append("hpProducts", hpProducts).toString();
    }
}
