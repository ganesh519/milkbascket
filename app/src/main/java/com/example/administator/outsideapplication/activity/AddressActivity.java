package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.adapters.AddressBookAdapter;
import com.example.administator.outsideapplication.model.AddressBookDataDetailsResponse;
import com.example.administator.outsideapplication.model.AddressBookDataResponse;
import com.example.administator.outsideapplication.model.AddressBookResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressActivity extends AppCompatActivity {
    ApiInterface apiInterface;
    RecyclerView recycler_address;
    RelativeLayout relative_add;
    ImageView back;
    String Activity;
    TextView text_address;
    AppController appController;
    String activity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        relative_add = findViewById(R.id.relative_add);
        relative_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(AddressActivity.this,AddAddressActivity.class);
                intent.putExtra("Activity",activity);
                finish();
            }
        });
        text_address = findViewById(R.id.text_address);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity.equals("AddAddressActivity")){
                    startActivity(new Intent(AddressActivity.this,MainActivity.class));

                }
                if (activity.equals("MyAccount")){
                    finish();

                }
            }
        });

        appController = (AppController) getApplication();

        if (appController.isConnection()) {

            getData();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }


    }

    private void getData() {


        if (getIntent().getExtras() != null) {
            activity = getIntent().getStringExtra("Activity");

        }

        SharedPreferences sharedPreferences = AddressActivity.this.getSharedPreferences("CityandArea", Context.MODE_PRIVATE);
        String city_id = sharedPreferences.getString("City_id", "");
        String area_id = sharedPreferences.getString("Area", "");

        SharedPreferences sharedPreferences1 = AddressActivity.this.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreferences1.getString("user_id", "");


        recycler_address = findViewById(R.id.recycler_address);
        final ProgressDialog progressDialog = new ProgressDialog(AddressActivity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddressBookResponse> call = apiInterface.AddressBook(city_id, area_id, user_id);
        call.enqueue(new Callback<AddressBookResponse>() {
            @Override
            public void onResponse(Call<AddressBookResponse> call, Response<AddressBookResponse> response) {
                if (response.isSuccessful()) ;
                AddressBookResponse addressBookResponse = response.body();
                if (addressBookResponse.status == 1) {
                    progressDialog.dismiss();
                    AddressBookDataResponse addressBookDataResponse = addressBookResponse.data;
                    List<AddressBookDataDetailsResponse> addressBookDataDetailsResponse = addressBookDataResponse.address;
                    recycler_address.setLayoutManager(new LinearLayoutManager(AddressActivity.this, LinearLayoutManager.VERTICAL, false));
                    recycler_address.setAdapter(new AddressBookAdapter(addressBookDataDetailsResponse, AddressActivity.this, activity));
                    recycler_address.setHasFixedSize(true);
                    recycler_address.setNestedScrollingEnabled(false);

                } else if (addressBookResponse.status == 0) {
                    text_address.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
//                    Toast.makeText(AddressActivity.this, addressBookResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddressBookResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(AddressActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if (activity.equals("PaymentActivity")) {
//            Intent intent = new Intent(AddressActivity.this, Payment_activity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(intent);
//        }
//        if (activity.equals("AddAddressActivity")){
//            startActivity(new Intent(AddressActivity.this,MainActivity.class));
//
//        }
//        if (activity.equals("MyAccount")){
//            finish();
//
//        }
    }
}
