package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CartDataDetailsresponse {


    @SerializedName("prod_id")
    @Expose
    public String prodId;
    @SerializedName("vendor_id")
    @Expose
    public String vendorId;
    @SerializedName("service_id")
    @Expose
    public String serviceId;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("stock")
    @Expose
    public String stock;
    @SerializedName("tags")
    @Expose
    public String tags;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("variation_title")
    @Expose
    public String variationTitle;
    @SerializedName("actual_price")
    @Expose
    public String actualPrice;
    @SerializedName("discount_price")
    @Expose
    public String discountPrice;
    @SerializedName("availability")
    @Expose
    public String availability;
    @SerializedName("delivery_category")
    @Expose
    public String deliveryCategory;
    @SerializedName("allow_apointment")
    @Expose
    public String allowApointment;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("image2")
    @Expose
    public String image2;
    @SerializedName("image3")
    @Expose
    public String image3;
    @SerializedName("image4")
    @Expose
    public String image4;
    @SerializedName("image5")
    @Expose
    public String image5;
    @SerializedName("more_images")
    @Expose
    public String moreImages;
    @SerializedName("video_up")
    @Expose
    public String videoUp;
    @SerializedName("feature_product")
    @Expose
    public String featureProduct;
    @SerializedName("special_product")
    @Expose
    public String specialProduct;
    @SerializedName("sort_order")
    @Expose
    public String sortOrder;
    @SerializedName("p_pricename")
    @Expose
    public String pPricename;
    @SerializedName("p_specifname")
    @Expose
    public String pSpecifname;
    @SerializedName("p_spname")
    @Expose
    public String pSpname;
    @SerializedName("appointment_price")
    @Expose
    public String appointmentPrice;
    @SerializedName("variation_price")
    @Expose
    public String variationPrice;
    @SerializedName("variationv2")
    @Expose
    public String variationv2;
    @SerializedName("variation_titlev2")
    @Expose
    public String variationTitlev2;
    @SerializedName("variation_pricev2")
    @Expose
    public String variationPricev2;
    @SerializedName("variationv3")
    @Expose
    public String variationv3;
    @SerializedName("variation_titlev3")
    @Expose
    public String variationTitlev3;
    @SerializedName("variation_pricev3")
    @Expose
    public String variationPricev3;
    @SerializedName("variationv4")
    @Expose
    public String variationv4;
    @SerializedName("variation_titlev4")
    @Expose
    public String variationTitlev4;
    @SerializedName("variation_pricev4")
    @Expose
    public String variationPricev4;
    @SerializedName("variationv5")
    @Expose
    public String variationv5;
    @SerializedName("variation_titlev5")
    @Expose
    public String variationTitlev5;
    @SerializedName("variation_pricev5")
    @Expose
    public String variationPricev5;
    @SerializedName("allow_commision")
    @Expose
    public String allowCommision;
    @SerializedName("commision_type")
    @Expose
    public String commisionType;
    @SerializedName("commision_value")
    @Expose
    public String commisionValue;
    @SerializedName("allow_gift")
    @Expose
    public String allowGift;
    @SerializedName("gift_price")
    @Expose
    public String giftPrice;
    @SerializedName("money_back_title")
    @Expose
    public String moneyBackTitle;
    @SerializedName("money_back_des")
    @Expose
    public String moneyBackDes;
    @SerializedName("store_exchange_title")
    @Expose
    public String storeExchangeTitle;
    @SerializedName("store_exchange_des")
    @Expose
    public String storeExchangeDes;
    @SerializedName("lpg_title")
    @Expose
    public String lpgTitle;
    @SerializedName("lpg_des")
    @Expose
    public String lpgDes;
    @SerializedName("sg_title")
    @Expose
    public String sgTitle;
    @SerializedName("sg_des")
    @Expose
    public String sgDes;
    @SerializedName("return_available")
    @Expose
    public String returnAvailable;
    @SerializedName("return_policy")
    @Expose
    public String returnPolicy;
    @SerializedName("allow_price")
    @Expose
    public String allowPrice;
    @SerializedName("color_variation")
    @Expose
    public String colorVariation;
    @SerializedName("size_variation")
    @Expose
    public String sizeVariation;
    @SerializedName("sub_total")
    @Expose
    public String subTotal;
    @SerializedName("cart_id")
    @Expose
    public String cartId;
    @SerializedName("qty")
    @Expose
    public String qty;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("prodId", prodId).append("vendorId", vendorId).append("serviceId", serviceId).append("title", title).append("stock", stock).append("tags", tags).append("description", description).append("variationTitle", variationTitle).append("actualPrice", actualPrice).append("discountPrice", discountPrice).append("availability", availability).append("deliveryCategory", deliveryCategory).append("allowApointment", allowApointment).append("status", status).append("image", image).append("image2", image2).append("image3", image3).append("image4", image4).append("image5", image5).append("moreImages", moreImages).append("videoUp", videoUp).append("featureProduct", featureProduct).append("specialProduct", specialProduct).append("sortOrder", sortOrder).append("pPricename", pPricename).append("pSpecifname", pSpecifname).append("pSpname", pSpname).append("appointmentPrice", appointmentPrice).append("variationPrice", variationPrice).append("variationv2", variationv2).append("variationTitlev2", variationTitlev2).append("variationPricev2", variationPricev2).append("variationv3", variationv3).append("variationTitlev3", variationTitlev3).append("variationPricev3", variationPricev3).append("variationv4", variationv4).append("variationTitlev4", variationTitlev4).append("variationPricev4", variationPricev4).append("variationv5", variationv5).append("variationTitlev5", variationTitlev5).append("variationPricev5", variationPricev5).append("allowCommision", allowCommision).append("commisionType", commisionType).append("commisionValue", commisionValue).append("allowGift", allowGift).append("giftPrice", giftPrice).append("moneyBackTitle", moneyBackTitle).append("moneyBackDes", moneyBackDes).append("storeExchangeTitle", storeExchangeTitle).append("storeExchangeDes", storeExchangeDes).append("lpgTitle", lpgTitle).append("lpgDes", lpgDes).append("sgTitle", sgTitle).append("sgDes", sgDes).append("returnAvailable", returnAvailable).append("returnPolicy", returnPolicy).append("allowPrice", allowPrice).append("colorVariation", colorVariation).append("sizeVariation", sizeVariation).append("subTotal", subTotal).append("cartId", cartId).append("qty", qty).toString();
    }

}
