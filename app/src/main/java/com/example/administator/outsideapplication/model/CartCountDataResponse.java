package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CartCountDataResponse {
    @SerializedName("cart_count")
    @Expose
    public Integer cartCount;
    @SerializedName("cart_total")
    @Expose
    public CartTotalResponse cartTotal;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cartCount", cartCount).append("cartTotal", cartTotal).toString();
    }


}
