package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.model.ProfileDataResponse;
import com.example.administator.outsideapplication.model.ProfileResponse;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyAccount extends AppCompatActivity {

    ApiInterface apiInterface;
    ImageView back;
    ImageView imageview;
    TextView email, name, mobile;
    RelativeLayout edit_profile, relative_address, relative_cpassword, relative_cmobile, relative_logout, my_orders;
    AppController appController;
    String Name,Phone,Email,lname,photo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_account);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        email = findViewById(R.id.email);
        name = findViewById(R.id.name);
        mobile = findViewById(R.id.mobile);
        imageview = findViewById(R.id.imageview);
        back = findViewById(R.id.back);
        edit_profile = findViewById(R.id.edit_profile);
        relative_address = findViewById(R.id.relative_address);
        relative_cpassword = findViewById(R.id.relative_cpassword);
        relative_cmobile = findViewById(R.id.relative_cmobile);
        relative_logout = findViewById(R.id.relative_logout);
        my_orders = findViewById(R.id.my_orders);

        appController = (AppController) getApplication();
        if (appController.isConnection()) {

            getData();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }

        relative_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.commit();

                SharedPreferences sharedPreferences1 = v.getContext().getSharedPreferences("CityandArea", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor1 = sharedPreferences1.edit();
                editor1.clear();
                editor1.commit();

                SharedPreferences sharedPreference = v.getContext().getSharedPreferences("AddressData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor2 = sharedPreference.edit();
                editor2.clear();
                editor2.commit();

                Intent intent = new Intent(MyAccount.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });

        relative_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccount.this, AddressActivity.class);
                intent.putExtra("Activity", "MyAccount");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });
        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MyAccount.this,EditProfie.class);
                intent.putExtra("firstname",Name);
                intent.putExtra("mobile",Phone);
                intent.putExtra("e_mail",Email);
                intent.putExtra("lastname",lname);
                intent.putExtra("image",photo);
                startActivity(intent);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        relative_cpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyAccount.this, ChangePassword.class));

            }
        });
        my_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent intent=new Intent(MyAccount.this, MyOrders.class);
              intent.putExtra("Activity","MyAccount");
              startActivity(intent);

            }
        });
        relative_cmobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyAccount.this, ChangeMobileNumber.class));
            }
        });

    }

    private void getData() {

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("CityandArea", Context.MODE_PRIVATE);
        String city_id = sharedPreferences.getString("City_id", "");
        String area_id = sharedPreferences.getString("Area", "");

        SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreferences1.getString("user_id", "");

        final ProgressDialog progressDialog = new ProgressDialog(MyAccount.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProfileResponse> call = apiInterface.Profile(city_id, area_id, user_id);
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                if (response.isSuccessful()) ;
                ProfileResponse profileResponse = response.body();
                if (profileResponse.status == 1) {
                    progressDialog.dismiss();
                    ProfileDataResponse profileDataResponse = profileResponse.data;
                    Name = profileDataResponse.firstName;
                    Phone = profileDataResponse.phone;
                    Email = profileDataResponse.email;
                    lname = profileDataResponse.lastName;
                    name.setText(Name);
                    mobile.setText(Phone);
                    email.setText(Email);

                    photo = "https://www.testocar.in/boloadmin/" + profileDataResponse.image;

                    Picasso.get().load(photo).placeholder(R.drawable.default_loading).into(imageview);


                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("Profile", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("firstname", Name);
                    editor.putString("lastname", lname);
                    editor.putString("e_mail", Email);
                    editor.putString("mobile", Phone);
                    editor.putString("image", photo);
                    editor.commit();

                } else if (profileResponse.status == 0) {
                    progressDialog.dismiss();
                    Toast.makeText(MyAccount.this, profileResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MyAccount.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
