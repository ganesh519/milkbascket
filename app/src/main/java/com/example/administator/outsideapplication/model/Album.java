package com.example.administator.outsideapplication.model;

public class Album {
    private int img_id;
    private String names;
    private String price;
    private String dprice;

    public String getDprice() {
        return dprice;
    }

    public void setDprice(String dprice) {
        this.dprice = dprice;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public Album(int img_id, String names, String price,String dprice){
        setImg_id(img_id);
        setNames(names);
        setPrice(price);
        setDprice(dprice);

    }

    public int getImg_id() {
        return img_id;
    }

    public void setImg_id(int img_id) {
        this.img_id = img_id;
    }

}
