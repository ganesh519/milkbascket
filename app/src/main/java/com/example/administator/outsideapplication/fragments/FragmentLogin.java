package com.example.administator.outsideapplication.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.CommonClass;
import com.example.administator.outsideapplication.ConnectionDetector;
import com.example.administator.outsideapplication.ConnectivityReceiver;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.activity.ForgotPassword;
import com.example.administator.outsideapplication.activity.LoginActivity;
import com.example.administator.outsideapplication.activity.Login_Mobile;
import com.example.administator.outsideapplication.activity.MainActivity;
import com.example.administator.outsideapplication.model.LoginDataResponse;
import com.example.administator.outsideapplication.model.LoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentLogin extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {
    EditText edit_email;
    TextInputEditText edt_password;
    Button submit;
    private boolean isConnected;
    LinearLayout linear;
    String email_id, Pass_word;
    ProgressDialog pd;
    ApiInterface apiInterface;
    TextView login_mobile, signup, forgot_password;
    private static ViewPager viewPager;
    public static View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.login_fragment, null);
        submit = view.findViewById(R.id.bt_login_login);
        edit_email = view.findViewById(R.id.edit_email);
        edt_password = view.findViewById(R.id.edt_password);
        linear = view.findViewById(R.id.linear);

        login_mobile = view.findViewById(R.id.login_mobile);
        signup = view.findViewById(R.id.signup);
        forgot_password = view.findViewById(R.id.forgot_password);
        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ForgotPassword.class));
            }
        });
        login_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Login_Mobile.class));

            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.putExtra("signup", "Sign");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(intent);


            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
//                isConnected = ConnectionDetector.isConnected();
//                if (isConnected) {
//                    getData();
//                } else {
//                    CommonClass.showSnack(isConnected, linear);
//                }
            }
        });

        return view;
    }

    private void getData() {
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading....");
        pd.show();

        email_id = edit_email.getText().toString().trim();
        Pass_word = edt_password.getText().toString().trim();

        if (email_id.equals("") || Pass_word.equals("")) {
            pd.dismiss();
            Toast.makeText(getActivity(), "Enter Username and password", Toast.LENGTH_SHORT).show();
        } else if (email_id.equals("")) {
            pd.dismiss();
            Toast.makeText(getActivity(), "Invalid Username", Toast.LENGTH_SHORT).show();
        } else {
            getLogin(email_id, Pass_word);
        }

    }

    private void getLogin(String email_id, String pass_word) {

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("CityandArea", Context.MODE_PRIVATE);
        final String city_id = sharedPreferences.getString("City_id", "");
        final String area_id = sharedPreferences.getString("Area", "");

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LoginResponse> call = apiInterface.Login(city_id, area_id, email_id, pass_word);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) ;
                LoginResponse loginResponse = response.body();
                if (loginResponse.status == 1) {
                    LoginDataResponse loginDataResponse = loginResponse.data;
                    String user_id = loginDataResponse.userId;
                    String name = loginDataResponse.firstName;
                    String email = loginDataResponse.phone;
                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("user_id", user_id);
                    editor.putString("Name", name);
                    editor.putString("Phone", email);
                    editor.commit();

                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.putExtra("area", area_id);
                    intent.putExtra("city", city_id);
                    startActivity(intent);
                    edit_email.setText("");
                    edt_password.setText("");
                    pd.dismiss();
                } else if (loginResponse.status == 0) {
                    pd.dismiss();
                    Toast.makeText(getActivity(), loginResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        // register connection status listener

        AppController.getInstance().setConnectivityListener(this);
    }


    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }

        snackBar(message, color);


    }


    // snackBar
    private void snackBar(String message, int color) {
        Snackbar snackbar = Snackbar.make(view.findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);

    }
}
