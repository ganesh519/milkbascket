package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ProductListingDataDetailsResponse {
    @SerializedName("prod_id")
    @Expose
    public String prodId;
    @SerializedName("service_id")
    @Expose
    public String serviceId;
    @SerializedName("vendor_id")
    @Expose
    public String vendorId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("discount_price")
    @Expose
    public String discountPrice;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("image2")
    @Expose
    public String image2;
    @SerializedName("image3")
    @Expose
    public String image3;
    @SerializedName("image4")
    @Expose
    public String image4;
    @SerializedName("image5")
    @Expose
    public String image5;
    @SerializedName("availability")
    @Expose
    public String availability;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("rate_total")
    @Expose
    public String rateTotal;
    @SerializedName("rate_count")
    @Expose
    public String rateCount;
    @SerializedName("service_type")
    @Expose
    public String serviceType;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("variation_title")
    @Expose
    public String variationTitle;
    @SerializedName("variation_titlev2")
    @Expose
    public String variationTitlev2;
    @SerializedName("variation_titlev3")
    @Expose
    public String variationTitlev3;
    @SerializedName("variation_titlev4")
    @Expose
    public String variationTitlev4;
    @SerializedName("variation_titlev5")
    @Expose
    public String variationTitlev5;
    @SerializedName("allow_appointment")
    @Expose
    public String allowAppointment;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("prodId", prodId).append("serviceId", serviceId).append("vendorId", vendorId).append("name", name).append("price", price).append("discountPrice", discountPrice).append("image", image).append("image2", image2).append("image3", image3).append("image4", image4).append("image5", image5).append("availability", availability).append("description", description).append("rateTotal", rateTotal).append("rateCount", rateCount).append("serviceType", serviceType).append("type", type).append("variationTitle", variationTitle).append("variationTitlev2", variationTitlev2).append("variationTitlev3", variationTitlev3).append("variationTitlev4", variationTitlev4).append("variationTitlev5", variationTitlev5).append("allowAppointment", allowAppointment).toString();
    }
}
