package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.adapters.MyOrdersAdapter;
import com.example.administator.outsideapplication.adapters.OrdersListAdapter;
import com.example.administator.outsideapplication.model.BookinghistoryResponse;
import com.example.administator.outsideapplication.model.OrderBookingDataResponse;
import com.example.administator.outsideapplication.model.OrderBookingDetailResponse;
import com.example.administator.outsideapplication.model.OrderDetailsDataResponse;
import com.example.administator.outsideapplication.model.OrderDetailsResponse;
import com.kofigyan.stateprogressbar.StateProgressBar;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CALL_PHONE;

public class OrdersListActivity extends AppCompatActivity {
    RecyclerView recycler_view;
    ApiInterface apiInterface;
    ProgressDialog pd;
    OrdersListAdapter ordersListAdapter;
    String order_id;
    TextView txtSubtotal, txtDscount, txtGrndtotal, txtDeliverCharges, txtamntPaid, txtPayMode,order_status,d_name;
    ImageView back,d_image,d_call;
    LinearLayout layoutParent,d_linear;
    AppController appController;
    String activity;
    Button btnCancel;
    StateProgressBar your_state_progress_bar_id;
    String[] descriptionData = {"OrderPlace", "InProgress", "Shipped", "delivered"};
    NestedScrollView nested;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_list);
        nested=findViewById(R.id.nested);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity.equals("PaymentActivity")) {
                    startActivity(new Intent(OrdersListActivity.this, MainActivity.class));
                }
                if (activity.equals("MyOrder")) {
                    Intent intent=new Intent(OrdersListActivity.this,MyOrders.class);
                    intent.putExtra("Activity","MyOrderList");
                    startActivity(intent);
                }

            }
        });
        recycler_view = findViewById(R.id.recycler_view);

        layoutParent = findViewById(R.id.layoutParent);

        txtPayMode = findViewById(R.id.txtPayMode);
        txtamntPaid = findViewById(R.id.txtamntPaid);
        txtDeliverCharges = findViewById(R.id.txtDeliverCharges);
        txtSubtotal = findViewById(R.id.txtSubtotal);
        order_status=findViewById(R.id.order_status);

        d_linear=findViewById(R.id.d_linear);
        d_call=findViewById(R.id.d_call);
        d_name=findViewById(R.id.d_name);
        d_image=findViewById(R.id.d_image);

        btnCancel=findViewById(R.id.btnCancel);
        your_state_progress_bar_id=findViewById(R.id.your_state_progress_bar_id);

        order_id = getIntent().getStringExtra("orderId");

        appController = (AppController) getApplication();
        if (appController.isConnection()) {

            getData();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }


        if (getIntent().getExtras() != null) {
            activity = getIntent().getStringExtra("Activity");
        }

    }

    private void getData() {
        pd = new ProgressDialog(OrdersListActivity.this);
        pd.setMessage("Loading....");
        pd.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderDetailsResponse> call = apiInterface.OrderDetails(order_id);
        call.enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                if (response.isSuccessful()) ;
                OrderDetailsResponse orderDetailsResponse = response.body();
                if (orderDetailsResponse.status == 1) {
                    nested.setVisibility(View.VISIBLE);
                    pd.dismiss();
                    layoutParent.setVisibility(View.VISIBLE);
                    OrderDetailsDataResponse orderDetailsDataResponse = orderDetailsResponse.data;
                    List<OrderBookingDetailResponse> orderBookingDetailResponse = orderDetailsDataResponse.bookingDetails;

                    OrderBookingDataResponse orderBookingDataResponse = orderDetailsDataResponse.bookingData;
                    txtPayMode.setText("Payment Mode : " + orderBookingDataResponse.paymentMode);
                    txtSubtotal.setText(orderBookingDataResponse.subTotal);
                    txtDeliverCharges.setText(orderBookingDataResponse.additionalCharges);

                    order_status.setText(" Order status : " + orderBookingDataResponse.status);
                    String sstatus=orderBookingDataResponse.status;
                    if (sstatus.equalsIgnoreCase("Booked")){

                        descriptionData = new String[]{"OrderPlace", "InProgress", "Shipped", "delivered"};
                        your_state_progress_bar_id.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                        btnCancel.setVisibility(View.VISIBLE);
                        btnCancel.setText("cancel");

                    }
                    else if (sstatus.equalsIgnoreCase("Cancel Request"))
                    {
                        descriptionData = new String[]{"OrderPlace", "Cancel Request", "Shipped", "delivered"};
                        your_state_progress_bar_id.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                        btnCancel.setVisibility(View.GONE);

                        Toast.makeText(OrdersListActivity.this, "Our representative will contact soon..", Toast.LENGTH_LONG).show();

                    }
                    else if (sstatus.equalsIgnoreCase("On Transit")){
                        descriptionData = new String[]{"OrderPlace", "InProgress", "Shipped", "delivered"};
                        your_state_progress_bar_id.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
                        btnCancel.setVisibility(View.GONE);
                        d_linear.setVisibility(View.VISIBLE);

                        d_name.setText(orderBookingDataResponse.helperName);

                        Picasso.get().load("https://www.testocar.in/boloadmin/"+orderBookingDataResponse.helperPhoto).placeholder(R.drawable.default_loading).into(d_image);
                        final String d_no=orderBookingDataResponse.helperPhone;

                        d_call.setOnClickListener(new View.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onClick(View v) {
                                String uri = "tel:" + d_no.trim() ;
                                Intent intent = new Intent(Intent.ACTION_CALL);
                                intent.setData(Uri.parse(uri));
                                if (ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                                    startActivity(intent);
                                } else {
                                    requestPermissions(new String[]{CALL_PHONE}, 1);
                                }

                            }
                        });

                    }
                    else if (sstatus.equalsIgnoreCase("Delivered")) {
                        descriptionData = new String[]{"OrderPlace", "InProgress", "Shipped", "delivered"};
                        your_state_progress_bar_id.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);
                        btnCancel.setVisibility(View.GONE);
                    }
                    else if (sstatus.equalsIgnoreCase("Cancelled")){
                        descriptionData = new String[]{"OrderPlace", "Order Cancelled", "Shipped", "delivered"};
                        your_state_progress_bar_id.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                        btnCancel.setVisibility(View.GONE);

                    }
                    else if (sstatus.equalsIgnoreCase("Vendor Confirmed")){
                        descriptionData = new String[]{"OrderPlace", "Order Confirmed", "Shipped", "delivered"};
                        your_state_progress_bar_id.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                        btnCancel.setVisibility(View.GONE);
                    }

                    your_state_progress_bar_id.setStateDescriptionData(descriptionData);
                    // yourStateProgressBarId.setStateSize(40f);
                    your_state_progress_bar_id.enableAnimationToCurrentState(true);
                    your_state_progress_bar_id.setStateNumberTextSize(18f);
                    //yourStateProgressBarId.setStateLineThickness(5f);
                    your_state_progress_bar_id.setMaxDescriptionLine(2);
                    your_state_progress_bar_id.setDescriptionTopSpaceIncrementer(10f);
                    your_state_progress_bar_id.setStateDescriptionSize(10f);
                    your_state_progress_bar_id.setJustifyMultilineDescription(true);
                    your_state_progress_bar_id.setDescriptionLinesSpacing(5f);
                    your_state_progress_bar_id.setMaxStateNumber(StateProgressBar.StateNumber.FOUR);


                    float subt = Float.parseFloat(orderBookingDataResponse.subTotal);
                    float discountT = Float.parseFloat(orderBookingDataResponse.additionalCharges);

                    String total = String.valueOf(subt + discountT);
                    txtamntPaid.setText(total);

                    ordersListAdapter = new OrdersListAdapter(orderBookingDetailResponse, OrdersListActivity.this);
                    LinearLayoutManager gridLayoutManager = new LinearLayoutManager(OrdersListActivity.this);
                    recycler_view.setLayoutManager(gridLayoutManager);
                    recycler_view.setHasFixedSize(true);
                    recycler_view.setNestedScrollingEnabled(false);
                    recycler_view.setAdapter(ordersListAdapter);

                } else if (orderDetailsResponse.status == 0) {
                    nested.setVisibility(View.GONE);
                    layoutParent.setVisibility(View.GONE);
                    pd.dismiss();
                    Toast.makeText(OrdersListActivity.this, orderDetailsResponse.message, Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                nested.setVisibility(View.GONE);
                pd.dismiss();
                layoutParent.setVisibility(View.GONE);
                Toast.makeText(OrdersListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrdersListActivity.this, CancelOrderActivity.class);
                intent.putExtra("orderId", order_id);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (activity.equals("PaymentActivity")) {
            startActivity(new Intent(OrdersListActivity.this, MainActivity.class));
        }
        if (activity.equals("MyOrder")) {
           Intent intent=new Intent(OrdersListActivity.this,MyOrders.class);
           intent.putExtra("Activity","MyOrderList");
           startActivity(intent);

        }

    }


}
