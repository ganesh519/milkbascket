package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class AddAddressDataResponse {
    @SerializedName("address_id")
    @Expose
    public Integer addressId;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("addressId", addressId).toString();
    }

}
