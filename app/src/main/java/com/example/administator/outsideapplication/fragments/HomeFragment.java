package com.example.administator.outsideapplication.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.ConnectivityReceiver;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.activity.CartActivity;
import com.example.administator.outsideapplication.activity.MainActivity;
import com.example.administator.outsideapplication.adapters.CategoryRecyclerAdapter;
import com.example.administator.outsideapplication.adapters.DownAdapter;
import com.example.administator.outsideapplication.adapters.HorizontalAdapter;
import com.example.administator.outsideapplication.adapters.SliderAdapter;
import com.example.administator.outsideapplication.model.CartCountDataResponse;
import com.example.administator.outsideapplication.model.CartCountResponse;
import com.example.administator.outsideapplication.model.CategoriesResponse;
import com.example.administator.outsideapplication.model.HomeBannerResponse;
import com.example.administator.outsideapplication.model.HomeDataResponse;
import com.example.administator.outsideapplication.model.HomeResponse;
import com.example.administator.outsideapplication.model.ProductsDataResponse;
import com.example.administator.outsideapplication.model.ProductsRespose;
import com.example.administator.outsideapplication.model.SliderResponse;

import com.github.demono.AutoScrollViewPager;
import com.rd.PageIndicatorView;
import com.squareup.picasso.Picasso;

import java.util.List;



import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {
    ApiInterface apiInterface;
    AutoScrollViewPager viewPager, down_pager;

    RecyclerView home_recycler, recycler_category;
    ImageView banner;
    ScrollView home_scroll;
    String user_id;
    AppController appController;
    PageIndicatorView pageIndicatorView, down_pageIndicatorView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, null);
        getActivity().setTitle("Basil");

        recycler_category = view.findViewById(R.id.recycler_category);
        home_recycler = view.findViewById(R.id.home_recycler);
//        indicator=view.findViewById(R.id.indicator);
        viewPager = view.findViewById(R.id.pager);
        pageIndicatorView = view.findViewById(R.id.pageIndicatorView);
//        banner=view.findViewById(R.id.banner);
        down_pager = view.findViewById(R.id.down_pager);
//        down_indicator=view.findViewById(R.id.down_indicator);
        down_pageIndicatorView = view.findViewById(R.id.down_pageIndicatorView);
        home_scroll = view.findViewById(R.id.home_scroll);


//        String city_id=MainActivity.city_id;
//        String area_id=MainActivity.area_id;
//        appController = (AppController) ;
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (isConnected) {

            getData();

        } else {
            View view1 = inflater.inflate(R.layout.internet, null);
            Button tryButton = view1.findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }

        return view;
    }

    private void getData() {

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("CityandArea", Context.MODE_PRIVATE);
        String city_id = sharedPreferences.getString("City_id", "");
        String area_id = sharedPreferences.getString("Area", "");

//        MainActivity.img_cart.setVisibility(View.VISIBLE);
        SharedPreferences sharedPreference = getContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        user_id = sharedPreference.getString("user_id", "");
        if (user_id.equals("")) {
            MainActivity.img_cart.setVisibility(View.GONE);
        } else {
            MainActivity.img_cart.setVisibility(View.VISIBLE);
            CartCount(user_id);
        }

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("wait...");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<HomeResponse> call = apiInterface.HomeData(city_id, area_id);
        call.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                if (response.isSuccessful()) ;
                HomeResponse homeResponse = response.body();
                if (homeResponse.status == 1) {
                    home_scroll.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    HomeDataResponse homeDataResponse = homeResponse.data;
                    List<SliderResponse> sliderResponse = homeDataResponse.sliders;
                    SliderAdapter sliderAdapter = new SliderAdapter(sliderResponse, getActivity());
                    viewPager.setAdapter(sliderAdapter);
                    viewPager.startAutoScroll();

                    List<ProductsRespose> productsRespose = homeDataResponse.hpProducts;
                    if (productsRespose != null) {
                        for (int i = 0; i < productsRespose.size(); i++) {
                            List<ProductsDataResponse> productsDataResponse = productsRespose.get(i).categorieslevelone;
                            home_recycler.setAdapter(new HorizontalAdapter(productsDataResponse, getContext()));
                            home_recycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                            home_recycler.setHasFixedSize(true);
                            home_recycler.setNestedScrollingEnabled(false);
                        }

                    }

                    List<CategoriesResponse> categoriesResponse = homeDataResponse.services;
                    if (categoriesResponse != null) {
                        recycler_category.setAdapter(new CategoryRecyclerAdapter(categoriesResponse, getContext()));
                        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
                        recycler_category.setLayoutManager(gridLayoutManager);
                        recycler_category.setHasFixedSize(true);
                        recycler_category.setNestedScrollingEnabled(false);
                    }

                    List<HomeBannerResponse> homeBannerResponse = homeDataResponse.homeBanners;
                    DownAdapter downAdapter = new DownAdapter(homeBannerResponse, getActivity());
                    down_pager.setAdapter(downAdapter);
                    down_pager.startAutoScroll();

                } else if (homeResponse.status == 0) {
                    home_scroll.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), homeResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                home_scroll.setVisibility(View.GONE);
                progressDialog.dismiss();
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void CartCount(String user_id) {
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CartCountResponse> cartCountResponseCall = apiInterface.CartCount(user_id);
        cartCountResponseCall.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful());
                CartCountResponse cartCountResponse = response.body();
                if (cartCountResponse.status == 1) {
                    CartCountDataResponse cartCountDataResponse = cartCountResponse.data;
                    int count = cartCountDataResponse.cartCount;
                    MainActivity.txt_count.setText(String.valueOf(count));
                    if (count > 0) {
                        MainActivity.img_cart.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                startActivity(new Intent(getContext(), CartActivity.class));

                            }
                        });
                    } else {
                        Toast.makeText(getActivity(), "No Products in Cart", Toast.LENGTH_SHORT).show();
                    }
                } else if (cartCountResponse.status == 0) {
                    Toast.makeText(getActivity(), cartCountResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

}
