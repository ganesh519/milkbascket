package com.example.administator.outsideapplication.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.fragments.HomeFragment;
import com.example.administator.outsideapplication.fragments.ProductListingFragment;
import com.example.administator.outsideapplication.model.CategoriesResponse;
import com.squareup.picasso.Picasso;

import java.util.List;


public class CategoryRecyclerAdapter extends RecyclerView.Adapter<CategoryRecyclerAdapter.Holder> {

    List<CategoriesResponse> allCategoryResponse;
    Context context;
    FragmentManager fragmentManager;
    Fragment currentFragment;
    FragmentTransaction fragmentTransaction;

    public CategoryRecyclerAdapter(List<CategoriesResponse> allCategoryResponse, Context context) {
        this.allCategoryResponse = allCategoryResponse;
        this.context = context;

    }

    @NonNull
    @Override
    public CategoryRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        view = inflater.inflate(R.layout.grid_layout, parent, false);
        Holder holder = new Holder(view, viewType);
        return holder;

    }

    @Override
    public void onBindViewHolder(@NonNull CategoryRecyclerAdapter.Holder holder, final int position) {

        Picasso.get().load("https://www.testocar.in/boloadmin/" + allCategoryResponse.get(position).image).placeholder(R.drawable.default_loading).into(holder.product_image);
        holder.name.setText(allCategoryResponse.get(position).title);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String service = allCategoryResponse.get(position).serviceId;
                String c_name=allCategoryResponse.get(position).title;
                Bundle bundle = new Bundle();
                bundle.putString("Service_name", service);
                bundle.putString("category_name", c_name);

                AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();
                ProductListingFragment productListingFragment = new ProductListingFragment();
                appCompatActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fl_main_framelayout, productListingFragment, "").addToBackStack("").commit();
                productListingFragment.setArguments(bundle);


//                fragmentManager=v.getContext().getSupportFragmentManager();
////        HomeFragment fragment = new HomeFragment();
//                fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.fl_main_framelayout,new HomeFragment(), "homee");
////        fragmentTransaction.addToBackStack(null);
//                fragmentTransaction.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return allCategoryResponse.size();
    }


    class Holder extends RecyclerView.ViewHolder {
        ImageView product_image;
        TextView name;

        public Holder(View itemView, int viewType) {

            super(itemView);
            product_image = (ImageView) itemView.findViewById(R.id.product_image);
            name = itemView.findViewById(R.id.name);

        }
    }

}
