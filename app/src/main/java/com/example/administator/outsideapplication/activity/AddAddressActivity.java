package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.model.AddAddressDataResponse;
import com.example.administator.outsideapplication.model.AddAddressResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAddressActivity extends AppCompatActivity {
    ImageView back;
    TextInputEditText edt_title, edt_fname, edt_lname, edt_email, edt_phone, edt_address,edt_address1;
    CheckBox checkBox;
    Button submit;
    ApiInterface apiInterface;
    int type;
    String activity_tag;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_address);

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        submit = findViewById(R.id.submit);

        checkBox = findViewById(R.id.checkbox);

        edt_address = findViewById(R.id.edt_address);
        edt_phone = findViewById(R.id.edt_phone);
        edt_email = findViewById(R.id.edt_email);
        edt_lname = findViewById(R.id.edt_lname);
        edt_fname = findViewById(R.id.edt_fname);
        edt_title = findViewById(R.id.edt_title);
        edt_address1=findViewById(R.id.edt_address1);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    type = 1;
                } else {
                    type = 0;
                }
            }
        });

        if (getIntent() != null){
            activity_tag=getIntent().getStringExtra("Activity");
        }
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String title = edt_title.getText().toString();
                String fname = edt_fname.getText().toString();
                String lname = edt_lname.getText().toString();
                String email = edt_email.getText().toString();
                String phone = edt_phone.getText().toString();
                String address = edt_address.getText().toString();
                String address1 = edt_address1.getText().toString();

                if (title.equals("")) {
                    Toast.makeText(AddAddressActivity.this, "Please Enter details", Toast.LENGTH_SHORT).show();
                } else if (fname.equals("")) {
                    Toast.makeText(AddAddressActivity.this, "Please Enter Firstname", Toast.LENGTH_SHORT).show();
                } else if (lname.equals("")) {
                    Toast.makeText(AddAddressActivity.this, "Please Enter Lastname", Toast.LENGTH_SHORT).show();
                } else if (email.equals("")) {
                    Toast.makeText(AddAddressActivity.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
                } else if (phone.equals("")) {
                    Toast.makeText(AddAddressActivity.this, "Please Enter Mobile number", Toast.LENGTH_SHORT).show();
                } else if (address.equals("")) {
                    Toast.makeText(AddAddressActivity.this, "Please Enter Address line1", Toast.LENGTH_SHORT).show();
                } else if (address1.equals("")) {
                    Toast.makeText(AddAddressActivity.this, "Please Enter Address line2", Toast.LENGTH_SHORT).show();
                } else {

                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("CityandArea", Context.MODE_PRIVATE);
                    final String city_id = sharedPreferences.getString("City_id", "");
                    final String area_id = sharedPreferences.getString("Area", "");

                    SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                    final String user_id = sharedPreferences1.getString("user_id", "");

                    final ProgressDialog progressDialog = new ProgressDialog(AddAddressActivity.this);
                    progressDialog.setMessage("Loading....");
                    progressDialog.show();

                    apiInterface= ApiClient.getClient().create(ApiInterface.class);

                    String type1=String.valueOf(type);

                    Call<AddAddressResponse> call=apiInterface.AddAddress(user_id,city_id,area_id,title,fname,lname,email,phone,address,address1,type1);
                    call.enqueue(new Callback<AddAddressResponse>() {
                        @Override
                        public void onResponse(Call<AddAddressResponse> call, Response<AddAddressResponse> response) {
                            if (response.isSuccessful());
                            AddAddressResponse addAddressResponse=response.body();
                            if (addAddressResponse.status == 1){
                                progressDialog.dismiss();
                                AddAddressDataResponse addAddressDataResponse=addAddressResponse.data;
                                Toast.makeText(AddAddressActivity.this, addAddressResponse.message, Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(AddAddressActivity.this,AddressActivity.class);
                                intent.putExtra("Activity",activity_tag);
                                startActivity(intent);
                            }
                            else if (addAddressResponse.status == 0){
                                progressDialog.dismiss();
                                Toast.makeText(AddAddressActivity.this, addAddressResponse.message, Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(AddAddressActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            }
        });
    }
}
