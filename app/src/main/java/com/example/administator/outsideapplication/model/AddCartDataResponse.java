package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class AddCartDataResponse {
    @SerializedName("cart_data")
    @Expose
    public Integer cartData;
    @SerializedName("count")
    @Expose
    public Integer count;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cartData", cartData).append("count", count).toString();
    }


}
