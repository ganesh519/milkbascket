package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.model.AboutDataDetailsResponse;
import com.example.administator.outsideapplication.model.AboutDataResponse;
import com.example.administator.outsideapplication.model.AboutResponse;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class About_us extends AppCompatActivity {
    ImageView image, back;
    TextView about_text;
    ApiInterface apiInterface;
    LinearLayout linear;
    AppController appController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        about_text = findViewById(R.id.about_text);
        image = findViewById(R.id.image);
        linear = findViewById(R.id.linear);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        appController = (AppController) getApplication();

        if (appController.isConnection()) {

            getData();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }

    }

    private void getData() {


        final ProgressDialog progressDialog = new ProgressDialog(About_us.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AboutResponse> call = apiInterface.About_us();
        call.enqueue(new Callback<AboutResponse>() {
            @Override
            public void onResponse(Call<AboutResponse> call, Response<AboutResponse> response) {
                if (response.isSuccessful()) ;
                AboutResponse aboutResponse = response.body();
                if (aboutResponse.status == 1) {
                    linear.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    AboutDataResponse aboutDataResponse = aboutResponse.data;
                    AboutDataDetailsResponse aboutDataDetailsResponse = aboutDataResponse.page;

                    Picasso.get().load("https://www.testocar.in/boloadmin/" + aboutDataDetailsResponse.image).error(R.drawable.no_image).into(image);
                    String data = aboutDataDetailsResponse.description;
                    String result = Html.fromHtml(data).toString();
                    about_text.setText(result);


                } else if (aboutResponse.status == 0) {
                    linear.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    Toast.makeText(About_us.this, aboutResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AboutResponse> call, Throwable t) {
                linear.setVisibility(View.GONE);
                progressDialog.dismiss();
                Toast.makeText(About_us.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }


}
