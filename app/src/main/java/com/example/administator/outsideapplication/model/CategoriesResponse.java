package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CategoriesResponse {

    @SerializedName("service_id")
    @Expose
    public String serviceId;
    @SerializedName("total_products")
    @Expose
    public String totalProducts;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("service_type")
    @Expose
    public String serviceType;
    @SerializedName("sort_order")
    @Expose
    public String sortOrder;
    @SerializedName("new")
    @Expose
    public String _new;
    @SerializedName("image")
    @Expose
    public String image;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("serviceId", serviceId).append("totalProducts", totalProducts).append("title", title).append("type", type).append("serviceType", serviceType).append("sortOrder", sortOrder).append("_new", _new).append("image", image).toString();
    }


}
