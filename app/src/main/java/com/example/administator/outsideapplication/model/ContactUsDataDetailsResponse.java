package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ContactUsDataDetailsResponse {


    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("meta_title")
    @Expose
    public String metaTitle;
    @SerializedName("meta_keywords")
    @Expose
    public String metaKeywords;
    @SerializedName("meta_description")
    @Expose
    public String metaDescription;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("latitude")
    @Expose
    public String latitude;
    @SerializedName("longitude")
    @Expose
    public String longitude;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("description", description).append("metaTitle", metaTitle).append("metaKeywords", metaKeywords).append("metaDescription", metaDescription).append("image", image).append("latitude", latitude).append("longitude", longitude).toString();
    }

}
