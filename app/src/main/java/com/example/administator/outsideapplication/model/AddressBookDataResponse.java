package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class AddressBookDataResponse {


    @SerializedName("address")
    @Expose
    public List<AddressBookDataDetailsResponse> address = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("address", address).toString();
    }

}
