package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class PaymentCartTotalResponse {

    @SerializedName("total_formatted")
    @Expose
    public String totalFormatted;
    @SerializedName("total")
    @Expose
    public String total;
    @SerializedName("total_discount")
    @Expose
    public String totalDiscount;
    @SerializedName("total_discount_formatted")
    @Expose
    public String totalDiscountFormatted;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("qty")
    @Expose
    public String qty;
    @SerializedName("delivery_charges")
    @Expose
    public Integer deliveryCharges;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("totalFormatted", totalFormatted).append("total", total).append("totalDiscount", totalDiscount).append("totalDiscountFormatted", totalDiscountFormatted).append("userId", userId).append("qty", qty).append("deliveryCharges", deliveryCharges).toString();
    }

}
