package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CityDataDetailsResponse {


    @SerializedName("city_id")
    @Expose
    public String cityId;
    @SerializedName("city_name")
    @Expose
    public String cityName;
    @SerializedName("sort_order")
    @Expose
    public String sortOrder;
    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("sdfgd")
    @Expose
    public boolean isAdded;




    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cityId", cityId).append("cityName", cityName).append("sortOrder", sortOrder).append("image", image).toString();
    }
}
