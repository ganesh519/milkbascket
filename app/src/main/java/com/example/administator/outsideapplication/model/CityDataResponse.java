package com.example.administator.outsideapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class CityDataResponse {


    @SerializedName("city")
    @Expose
    public List<CityDataDetailsResponse> city = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("city", city).toString();
    }

}
