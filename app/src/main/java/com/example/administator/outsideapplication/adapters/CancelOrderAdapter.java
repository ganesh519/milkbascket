package com.example.administator.outsideapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.activity.CancelOrderActivity;
import com.example.administator.outsideapplication.model.MyOrderItem;
import com.example.administator.outsideapplication.model.OrderBookingDetailResponse;
import com.squareup.picasso.Picasso;


import java.util.List;

public class CancelOrderAdapter extends RecyclerView.Adapter<CancelOrderAdapter.MyViewHolder> {

    private Context context;
    List<OrderBookingDetailResponse> orderBookingDetailResponse;

    public CancelOrderAdapter(List<OrderBookingDetailResponse> orderBookingDetailResponse, CancelOrderActivity context) {
     this.orderBookingDetailResponse=orderBookingDetailResponse;
     this.context=context;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        CheckBox checkBox;
        ImageView orderImage;
        TextView txtOrderName,txtOrderID,txtOrderPrice,txtStatus,txtQty;

        public MyViewHolder(View itemView) {
            super(itemView);
            checkBox = (CheckBox) itemView.findViewById(R.id.rowCheckBox);
            orderImage = (ImageView)itemView.findViewById(R.id.imgOrder);
            txtOrderName = (TextView)itemView.findViewById(R.id.txtOrderName);
            txtOrderID = (TextView)itemView.findViewById(R.id.txtOrderID);
            txtOrderPrice = (TextView)itemView.findViewById(R.id.txtOrderPrice);
//            txtStatus = (TextView)itemView.findViewById(R.id.txtStatus);
            txtQty=(TextView)itemView.findViewById(R.id.txtQty);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.myorderlistmodel, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        Picasso.get().load("https://www.testocar.in/boloadmin/" + orderBookingDetailResponse.get(position).productImage).placeholder(R.drawable.default_loading).into(holder.orderImage);
        holder.txtOrderName.setText(orderBookingDetailResponse.get(position).productName);
        holder.txtOrderPrice.setText(" "+orderBookingDetailResponse.get(position).totalPrice);
        holder.txtQty.setText(orderBookingDetailResponse.get(position).quantity);
        holder.txtOrderID.setText(orderBookingDetailResponse.get(position).vendorId);
//        holder.txtStatus.setText(orderBookingDetailResponse.get(position).);

//        Glide.with(context).load(list.get(position).getImage_path()).into(holder.orderImage);
//        holder.txtOrderName.setText(list.get(position).getProduct_name());
//        holder.txtOrderID.setText("OrderId : "+list.get(position).getOrder_id());
//        holder.txtOrderPrice.setText("\u20B9"+list.get(position).getUnit_price_incl_tax());
//        holder.txtQty.setText("Quantity : "+list.get(position).getQuantity());
//       // holder.txtStatus.setText("Status : "+list.get(position).getOrder_status());
//
//        if (list.get(position).getOrder_status().equalsIgnoreCase("confirm")){
//            holder.txtStatus.setText("Status : "+"confirmed");
//        }else if (list.get(position).getOrder_status().equalsIgnoreCase("return")){
//
//            holder.txtStatus.setText("Status : "+"returned");
//
//        }else{
//            holder.txtStatus.setText("Status : " + list.get(position).getOrder_status());
//        }

        /*if (list.get(position).getOrder_status().equalsIgnoreCase("cancel_by_customer") || list.get(position).getOrder_status().equalsIgnoreCase("cancel_by_seller")){

            holder.txtStatus.setText("Status : "+"Cancel");
        }else {
            holder.txtStatus.setText("Status : " + list.get(position).getOrder_status());
        }*/
        holder.checkBox.setTag(position);


        holder.checkBox.setOnCheckedChangeListener(null);
//        holder.checkBox.setChecked(list.get(position).isChecked());

        //if true, your checkbox will be selected, else unselected


        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //set your object's last status
                //list.get(position).setSelected(isChecked);
//                if (list.size()>0) {
//
//                    boolean newState = !list.get(position).isChecked();
//                    list.get(position).checked = newState;
//
//                    int chcekedvalue =0;
//                    if (isChecked){
//
//                        chcekedvalue =1;
//                    }
//
//
//                    Log.e("CHECKEDVALUE ",""+chcekedvalue);
//
//                    Intent intent = new Intent("checkBoxTrue");
//                    intent.putExtra("chckboxCount", chcekedvalue);
//                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
//
//                }
            }
        });


    }


    public void selectAllItem(boolean isSelectedAll) {
//        try {
//            if (list != null) {
//                for (int index = 0; index < list.size(); index++) {
//                    list.get(index).setChecked(isSelectedAll);
//                }
//            }
//            notifyDataSetChanged();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }



    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return orderBookingDetailResponse.size();
    }
}
