package com.example.administator.outsideapplication.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.activity.Payment_activity;
import com.example.administator.outsideapplication.model.PaymentTypeDataDetailsResponse;

import java.util.List;

public class PaymentTpyeRecyclerAdapter extends RecyclerView.Adapter<PaymentTpyeRecyclerAdapter.Holder> {
    Context context;
    public int lastSelectedPosition = -1;
    private String name, payname;
    public static String cash;
    List<PaymentTypeDataDetailsResponse> paymentTypeDataDetailsResponse;

    public PaymentTpyeRecyclerAdapter(List<PaymentTypeDataDetailsResponse> paymentTypeDataDetailsResponse, Context payment_activity) {
        this.paymentTypeDataDetailsResponse = paymentTypeDataDetailsResponse;
        this.context = payment_activity;

    }

    @NonNull
    @Override
    public PaymentTpyeRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_paymentmethods, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentTpyeRecyclerAdapter.Holder holder, int i) {

        holder.offer_name.setText(paymentTypeDataDetailsResponse.get(i).name);
        holder.offer_select.setChecked(lastSelectedPosition == i);

    }

    @Override
    public int getItemCount() {
        return paymentTypeDataDetailsResponse.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView offer_name;
        RadioButton offer_select;

        public Holder(@NonNull View itemView) {
            super(itemView);

            offer_select = itemView.findViewById(R.id.offer_select);
            offer_name = itemView.findViewById(R.id.offer_name);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                    cash = paymentTypeDataDetailsResponse.get(lastSelectedPosition).id;
                }
            };
            itemView.setOnClickListener(clickListener);
            offer_select.setOnClickListener(clickListener);
        }
    }
}
