package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.model.EditProfileResponse;
import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfie extends AppCompatActivity {
    RelativeLayout relative_cpassword;
    ImageView imageview, edit_image, back;
    EditText edit_fname, edit_mobile, edit_email, edit_lname;
    String photo, user_id, city_id, area_id;
    File file = null;
    String MobilePattern = "[0-9]{10}";
    MultipartBody.Part body = null;
    RelativeLayout relative_save;
    private ArrayList<Image> images;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editprofile);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        edit_lname = findViewById(R.id.edit_lname);
        edit_fname = findViewById(R.id.edit_fname);
        edit_mobile = findViewById(R.id.edit_mobile);
        edit_email = findViewById(R.id.edit_email);

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imageview = findViewById(R.id.imageview);
        edit_image = findViewById(R.id.edit_image);
        edit_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.with(EditProfie.this)                         //  Initialize ImagePicker with activity or fragment context
                        .setToolbarColor("#212121")         //  Toolbar color
                        .setStatusBarColor("#000000")       //  StatusBar color (works with SDK >= 21  )
                        .setToolbarTextColor("#FFFFFF")     //  Toolbar text color (Title and Done button)
                        .setToolbarIconColor("#FFFFFF")     //  Toolbar icon color (Back and Camera button)
                        .setProgressBarColor("#4CAF50")     //  ProgressBar color
                        .setBackgroundColor("#212121")      //  Background color
                        .setCameraOnly(false)               //  Camera mode
                        .setMultipleMode(false)              //  Select multiple images or single image
                        .setFolderMode(true)                //  Folder mode
                        .setShowCamera(true)                //  Show camera button
                        .setFolderTitle("Albums")           //  Folder title (works with FolderMode = true)
                        .setImageTitle("Galleries")         //  Image title (works with FolderMode = false)
                        .setDoneTitle("Done")               //  Done button title
                        .setLimitMessage("You have reached selection limit")    // Selection limit message
                        .setMaxSize(10)                     //  Max images can be selected
                        .setSavePath("ImagePicker")         //  Image capture folder name
                        .setSelectedImages(images)          //  Selected images
                        .setAlwaysShowDoneButton(true)      //  Set always show done button in multiple mode
                        .setKeepScreenOn(true)              //  Keep screen on when selecting images
                        .start();                           //  Start ImagePicker
            }

        });

        relative_cpassword = findViewById(R.id.relative_cpassword);
        relative_cpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EditProfie.this, ChangePassword.class));
            }
        });
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        user_id = sharedPreferences.getString("user_id", "");

        SharedPreferences sharedPreference = getApplicationContext().getSharedPreferences("CityandArea", Context.MODE_PRIVATE);
        city_id = sharedPreference.getString("City_id", "");
        area_id = sharedPreference.getString("Area", "");

//        SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("Profile", Context.MODE_PRIVATE);
        String fname = getIntent().getStringExtra("firstname");
        String lname =getIntent().getStringExtra("lastname");
        String email =getIntent().getStringExtra("e_mail");
        String phone = getIntent().getStringExtra("mobile");
        photo = getIntent().getStringExtra("image");

        edit_lname.setText(lname);
        edit_fname.setText(fname);
        edit_email.setText(email);
        edit_mobile.setText(phone);


        if (photo.equals("")) {
            imageview.setImageResource(R.drawable.no_image);
        } else {

            Picasso.get().load(photo).resize(160, 200).into(imageview);
        }

        relative_save = findViewById(R.id.relative_save);
        relative_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getProfile();
            }
        });

    }

    private void getProfile() {

        String f_name = edit_fname.getText().toString();
        String l_name = edit_lname.getText().toString();
        String e_mail = edit_email.getText().toString();
        String mobile = edit_mobile.getText().toString();

        if (f_name.equals("")) {

            Toast.makeText(EditProfie.this, "Enter First name", Toast.LENGTH_SHORT).show();
        } else if (l_name.equals("")) {
            Toast.makeText(EditProfie.this, "Enter Last name", Toast.LENGTH_SHORT).show();

        } else {
            getPProfile(f_name, l_name, e_mail, mobile);
        }
    }

    private void getPProfile(String f_name, String l_name, String e_mail, String mobile) {

        final ProgressDialog progressDialog = new ProgressDialog(EditProfie.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        RequestBody cust_id = RequestBody.create(MediaType.parse("text/plain"), user_id);
        RequestBody city = RequestBody.create(MediaType.parse("text/plain"), city_id);
        RequestBody area = RequestBody.create(MediaType.parse("text/plain"), area_id);

        RequestBody firstnam = RequestBody.create(MediaType.parse("text/plain"), f_name);
        RequestBody lastnam = RequestBody.create(MediaType.parse("text/plain"), l_name);
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), e_mail);
        RequestBody Mobile = RequestBody.create(MediaType.parse("text/plain"), mobile);

        MultipartBody.Part body = null;
        if (file != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);

        }

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<EditProfileResponse> call = apiInterface.EditProfile(body, firstnam, lastnam, city, area, cust_id);
        call.enqueue(new Callback<EditProfileResponse>() {
            @Override
            public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                if (response.isSuccessful()) ;
                EditProfileResponse editProfileResponse = response.body();
                if (editProfileResponse.status == 1) {
                    progressDialog.dismiss();
                    Toast.makeText(EditProfie.this, editProfileResponse.message, Toast.LENGTH_SHORT).show();
                    finish();
                } else if (editProfileResponse.status == 0) {
                    progressDialog.dismiss();
                    Toast.makeText(EditProfie.this, editProfileResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(EditProfie.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);

            try {
                if (images != null) {
                    for (int i = 0; i < images.size(); i++) {
                        Uri uri = Uri.fromFile(new File(images.get(0).getPath()));
                        Picasso.get().load(uri).into(imageview);
//                        Picasso.with(getApplicationContext()).load(uri).into(profile_image);
                        file = null;
                        String filepath = images.get(0).getPath();
                        if (filepath != null && !filepath.equals("")) {
                            file = new File(filepath);
                        }
                    }
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
