package com.example.administator.outsideapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.adapters.CartProductsRecyclerAdapter;
import com.example.administator.outsideapplication.adapters.CheckoutAdapter;
import com.example.administator.outsideapplication.model.CartDataDetailsresponse;
import com.example.administator.outsideapplication.model.CartDataResponse;
import com.example.administator.outsideapplication.model.CartResponse;
import com.example.administator.outsideapplication.model.CartTotalResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutActivity extends AppCompatActivity {
    ImageView back;
    Button checkout;
    RecyclerView recycler_products;
    ApiInterface apiInterface;
    ProgressDialog progressDialog;
    String user_id;
    TextView total_Price, Totalamount, subTotal, txt_name, txt_home, total_address, txt_mobile, txt_edit, txt_hideaddress;
    RelativeLayout relative_hide;
    LinearLayout linear_hide;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout_activity);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        subTotal = findViewById(R.id.subTotal);

        recycler_products = findViewById(R.id.recycler_products);
        Totalamount = findViewById(R.id.Totalamount);
        total_Price = findViewById(R.id.total_Price);
        txt_mobile = findViewById(R.id.txt_mobile);
        total_address = findViewById(R.id.total_address);
        txt_home = findViewById(R.id.txt_home);
        txt_name = findViewById(R.id.txt_name);
        txt_edit = findViewById(R.id.txt_edit);
        relative_hide = findViewById(R.id.relative_hide);
        linear_hide = findViewById(R.id.linear_hide);
        txt_hideaddress = findViewById(R.id.txt_hideaddress);

        SharedPreferences sharedPreference = getApplicationContext().getSharedPreferences("AddressData", Context.MODE_PRIVATE);
        final String name = sharedPreference.getString("address_name", "");
        final String T_address = sharedPreference.getString("ADDRESS", "");
        final String address_id = sharedPreference.getString("ADDRESS_ID", "");
        final String address_type = sharedPreference.getString("ADDRESS_TYPE", "");
        final String mobile = sharedPreference.getString("address_mobile", "");
        final String home = sharedPreference.getString("address_home", "");
        final String lname = sharedPreference.getString("address_lname", "");
        final String email = sharedPreference.getString("address_email", "");


        if (T_address.equals("") && name.equals("") && mobile.equals("") && home.equals("") && lname.equals("") && email.equals("")) {
            linear_hide.setVisibility(View.GONE);
            txt_hideaddress.setVisibility(View.VISIBLE);
        } else {
            txt_name.setText(name);
            total_address.setText(T_address);
            txt_home.setText(home);
            txt_mobile.setText(mobile);
        }

        txt_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckoutActivity.this, Edit_address.class);
                Bundle bundle = new Bundle();
                bundle.putString("Fname", name);
                bundle.putString("Address", T_address);
                bundle.putString("Title", home);
                bundle.putString("Mobile", mobile);
                bundle.putString("Lname", lname);
                bundle.putString("Email", email);
                bundle.putString("Default", address_type);
                bundle.putString("Address_id", address_id);
                intent.putExtras(bundle);
                intent.putExtra("Activity", "CheckoutActivity");
                startActivity(intent);
            }
        });

        SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        user_id = sharedPreferences1.getString("user_id", "");

        progressDialog = new ProgressDialog(CheckoutActivity.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CartResponse> cartResponseCall = apiInterface.CartListing(user_id);
        cartResponseCall.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                if (response.isSuccessful()) ;
                CartResponse cartResponse = response.body();
                if (cartResponse.status == 1) {
                    relative_hide.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();

                    CartDataResponse cartDataResponse = cartResponse.data;
                    CartTotalResponse cartTotalResponse = cartResponse.data.cartTotal;
                    subTotal.setText("₹" + cartTotalResponse.total);
                    Totalamount.setText("₹" + cartTotalResponse.total);
                    total_Price.setText("₹" + cartTotalResponse.total);

                    List<CartDataDetailsresponse> cartDataDetailsresponse = cartDataResponse.cartData;
                    recycler_products.setAdapter(new CheckoutAdapter(cartDataDetailsresponse, CheckoutActivity.this));
                    recycler_products.setLayoutManager(new LinearLayoutManager(CheckoutActivity.this, LinearLayoutManager.VERTICAL, false));
                    recycler_products.setHasFixedSize(true);
                    recycler_products.setNestedScrollingEnabled(false);

                } else if (cartResponse.status == 0) {
                    relative_hide.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    Toast.makeText(CheckoutActivity.this, cartResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {
                relative_hide.setVisibility(View.GONE);
                progressDialog.dismiss();
                Toast.makeText(CheckoutActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        checkout = findViewById(R.id.checkout);
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CheckoutActivity.this, Payment_activity.class));
            }
        });
    }
}
