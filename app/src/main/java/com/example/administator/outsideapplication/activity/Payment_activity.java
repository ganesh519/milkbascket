package com.example.administator.outsideapplication.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administator.outsideapplication.ApiClient;
import com.example.administator.outsideapplication.ApiInterface;
import com.example.administator.outsideapplication.AppController;
import com.example.administator.outsideapplication.R;
import com.example.administator.outsideapplication.adapters.MyOrdersAdapter;
import com.example.administator.outsideapplication.adapters.PaymentTpyeRecyclerAdapter;
import com.example.administator.outsideapplication.model.PaymentCartTotalResponse;
import com.example.administator.outsideapplication.model.PaymentDataResponse;
import com.example.administator.outsideapplication.model.PaymentResponse;
import com.example.administator.outsideapplication.model.PaymentTypeDataDetailsResponse;
import com.example.administator.outsideapplication.model.PaymentTypeDataResponse;
import com.example.administator.outsideapplication.model.PaymentTypeResponse;
import com.example.administator.outsideapplication.model.PlaceOrderDataResponse;
import com.example.administator.outsideapplication.model.PlaceOrderResponse;
import com.example.administator.outsideapplication.model.SelectDefaultaddressResponse;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Payment_activity extends AppCompatActivity {
    ImageView back;
    TextView txtUserName, txtmobile, txtemail, txtAddress, txtSubTotal, txtDiscount, txtDeliver, txtAmountTobepaid, txtChangeAddress;
    ApiInterface apiInterface;
    RadioGroup radioGroup;
    RadioButton rbPaymentMode;
    Button btnOrder;
    RelativeLayout bottamlayout;
    String address_id, Daddress, addres, f_name, titlee, mobilee, l_name, emaill, address1;
    RecyclerView recycler_paymentoptions;
    PaymentTpyeRecyclerAdapter paymentTpyeRecyclerAdapter;
    String cash, type;
    String type1, user_id, payment_id;
    RelativeLayout relative_hide;
    AppController appController;
    TextView pselect_address;
    LinearLayout linear_address;
    SweetAlertDialog sd;
    NestedScrollView nested;
    String text, Choose,city,area;
    int ADDRESSREQUESTCODE = 100;
    SelectDefaultaddressResponse selectDefaultaddressResponse;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Payment_activity.this,CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });

//        relative_hide=findViewById(R.id.relative_hide);
        nested = findViewById(R.id.nested);
        txtAddress = findViewById(R.id.txtAddress);
        txtemail = findViewById(R.id.txtemail);
        txtmobile = findViewById(R.id.txtmobile);
        txtUserName = findViewById(R.id.txtUserName);

        btnOrder = findViewById(R.id.btnOrder);
        bottamlayout = findViewById(R.id.bottamlayout);

        txtChangeAddress = findViewById(R.id.txtChangeAddress);

        recycler_paymentoptions = findViewById(R.id.recycler_paymentoptions);

        txtSubTotal = findViewById(R.id.txtSubTotal);
        txtDiscount = findViewById(R.id.txtDiscount);
        txtDeliver = findViewById(R.id.txtDeliver);
        txtAmountTobepaid = findViewById(R.id.txtAmountTobepaid);

//        pselect_address = findViewById(R.id.pselect_address);
        linear_address = findViewById(R.id.linear_address);
        appController = (AppController) getApplication();
        if (appController.isConnection()) {

            getData();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }

        txtChangeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Payment_activity.this, AddressActivity.class);
                intent.putExtra("Activity", "PaymentActivity");
                startActivity(intent);
            }
        });

    }

    private void getData() {

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PaymentTypeResponse> call1 = apiInterface.PaymentType();
        call1.enqueue(new Callback<PaymentTypeResponse>() {
            @Override
            public void onResponse(Call<PaymentTypeResponse> call, Response<PaymentTypeResponse> response) {
                if (response.isSuccessful()) ;
                PaymentTypeResponse paymentTypeResponse = response.body();
                if (paymentTypeResponse.status == 1) {
                    PaymentTypeDataResponse paymentTypeDataResponse = paymentTypeResponse.data;
                    List<PaymentTypeDataDetailsResponse> paymentTypeDataDetailsResponse = paymentTypeDataResponse.paymentType;
                    paymentTpyeRecyclerAdapter = new PaymentTpyeRecyclerAdapter(paymentTypeDataDetailsResponse, Payment_activity.this);
                    LinearLayoutManager gridLayoutManager = new LinearLayoutManager(Payment_activity.this);
                    recycler_paymentoptions.setLayoutManager(gridLayoutManager);
                    recycler_paymentoptions.setHasFixedSize(true);
                    recycler_paymentoptions.setNestedScrollingEnabled(false);
                    recycler_paymentoptions.setAdapter(paymentTpyeRecyclerAdapter);

                } else if (paymentTypeResponse.status == 0) {
                    Toast.makeText(Payment_activity.this, paymentTypeResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PaymentTypeResponse> call, Throwable t) {
                Toast.makeText(Payment_activity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

        SharedPreferences sharedPreferences = Payment_activity.this.getSharedPreferences("CityandArea", Context.MODE_PRIVATE);
        area = sharedPreferences.getString("area_name", "");
        city = sharedPreferences.getString("city_name", "");
        final String city_id = sharedPreferences.getString("City_id", "");
        final String area_id = sharedPreferences.getString("Area", "");

        SharedPreferences sharedPreferences1 = Payment_activity.this.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        String user_id = sharedPreferences1.getString("user_id", "");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Daddress = bundle.getString("Default");
            address_id = bundle.getString("Address_id");
            addres = bundle.getString("Address");
            f_name = bundle.getString("Fname");
            titlee = bundle.getString("Title");
            mobilee = bundle.getString("Mobile");
            l_name = bundle.getString("Lname");
            emaill = bundle.getString("Email");
            address1 = bundle.getString("Address1");
            Choose = bundle.getString("choose");

            getPaymentdata(user_id);

        }

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PaymentData();
            }
        });
    }

    private void getPaymentdata(String user_id) {

        final ProgressDialog progressDialog = new ProgressDialog(Payment_activity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<PaymentResponse> call = apiInterface.PaymentData(user_id);
        call.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
                if (response.isSuccessful()) ;
                PaymentResponse paymentResponse = response.body();
                if (paymentResponse.status == 1) {
                    progressDialog.dismiss();
                    nested.setVisibility(View.VISIBLE);
                    PaymentDataResponse paymentDataResponse = paymentResponse.data;
                    selectDefaultaddressResponse = paymentDataResponse.selectaddress;
                    Log.e("AddressRespionse ", "" + selectDefaultaddressResponse);
                    Log.e("AddressRespionse1 ", "" + paymentDataResponse.selectaddress);

                    PaymentCartTotalResponse paymentCartTotalResponse = paymentDataResponse.cartTotal;

                    txtSubTotal.setText("₹" + paymentCartTotalResponse.total);
//                    txtAmountTobepaid.setText("र"+paymentCartTotalResponse.total);
                    txtDiscount.setText("₹" + paymentCartTotalResponse.totalDiscount);
                    txtDeliver.setText(String.valueOf("₹" + paymentCartTotalResponse.deliveryCharges));
                    int subt = Integer.parseInt(paymentCartTotalResponse.total);
//                    int discountT = Integer.parseInt(paymentCartTotalResponse.totalDiscount);
//
//                    String total = String.valueOf(subt - discountT);
//
//                    int tot = Integer.parseInt(total);
                    int delivery = paymentCartTotalResponse.deliveryCharges;
                    String final_total = String.valueOf(subt + delivery);

                    txtAmountTobepaid.setText("₹" + final_total);

                    if (Choose.equals("CartActivity") || Choose.equals("PaymentActivity")) {
//                        pselect_address.setVisibility(View.GONE);
//                        linear_address.setVisibility(View.VISIBLE);
                        txtUserName.setText(f_name);
                        txtmobile.setText("Mobile : " + mobilee);
                        txtemail.setText("Email : " + emaill);
                        txtAddress.setText(addres +","+ address1 + "," + area + "," + city+".");
                        btnOrder.setVisibility(View.VISIBLE);
                    }
                    else if (paymentDataResponse.selectaddress != null || !paymentDataResponse.selectaddress.toString().isEmpty()) {
                        if (selectDefaultaddressResponse.firstname != null && selectDefaultaddressResponse.phone != null &&
                                selectDefaultaddressResponse.email != null && selectDefaultaddressResponse.address != null &&
                                selectDefaultaddressResponse.area != null && selectDefaultaddressResponse.city != null) {
                            txtUserName.setText(selectDefaultaddressResponse.firstname);
                            txtmobile.setText("Mobile : " + selectDefaultaddressResponse.phone);
                            txtemail.setText("Email : " + selectDefaultaddressResponse.email);
                            txtAddress.setText(selectDefaultaddressResponse.address + selectDefaultaddressResponse.address1 + "," + selectDefaultaddressResponse.area + "," + selectDefaultaddressResponse.city);

                            address_id = selectDefaultaddressResponse.addressId;

                            btnOrder.setVisibility(View.VISIBLE);

                        } else {
//                            pselect_address.setVisibility(View.VISIBLE);
                            linear_address.setVisibility(View.GONE);
                        }

                    } else {
//                        pselect_address.setVisibility(View.VISIBLE);
                        linear_address.setVisibility(View.GONE);

                    }



                } else if (paymentResponse.status == 0) {
                    progressDialog.dismiss();
                    nested.setVisibility(View.GONE);
                    Toast.makeText(Payment_activity.this, paymentResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                nested.setVisibility(View.GONE);
                progressDialog.dismiss();
                Toast.makeText(Payment_activity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("address", "" + t.getMessage());
            }
        });
    }

    private void PaymentData() {

        String id = PaymentTpyeRecyclerAdapter.cash;
        payment_id = id;

        if (payment_id == null) {
            Toast.makeText(Payment_activity.this, "Please Select Payment method ", Toast.LENGTH_SHORT).show();

        }  else {
            PaymentContinue();
        }
    }

    private void PaymentContinue() {

        SharedPreferences sharedPreferences1 = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        user_id = sharedPreferences1.getString("user_id", "");

        final ProgressDialog progressDialog = new ProgressDialog(Payment_activity.this);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PlaceOrderResponse> call = apiInterface.PlaceOrder(payment_id, address_id, user_id);
        call.enqueue(new Callback<PlaceOrderResponse>() {
            @Override
            public void onResponse(Call<PlaceOrderResponse> call, Response<PlaceOrderResponse> response) {
                if (response.isSuccessful()) ;
                PlaceOrderResponse placeOrderResponse = response.body();
                if (placeOrderResponse.status == 1) {
                    progressDialog.dismiss();
                    final PlaceOrderDataResponse placeOrderDataResponse = placeOrderResponse.data;

                    type = placeOrderDataResponse.paymentType;

                    final String o_id = String.valueOf(placeOrderDataResponse.orderId);
                    if (type.equals("cod")) {

                        sd = new SweetAlertDialog(Payment_activity.this, SweetAlertDialog.SUCCESS_TYPE);
                        sd.setTitleText("Thank You!!");
                        sd.setContentText("Order has been confirmed.!");
                        sd.setConfirmText("OK");
                        sd.showCancelButton(true);
                        sd.setCanceledOnTouchOutside(false);
                        sd.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(final SweetAlertDialog sweetAlertDialog) {
                                Intent order_detail = new Intent(Payment_activity.this, OrdersListActivity.class);
                                order_detail.putExtra("orderId", o_id);
                                order_detail.putExtra("Activity", "PaymentActivity");
                                startActivity(order_detail);
                                finish();
                            }
                        });
                        sd.show();
//                        final AlertDialog.Builder builder = new AlertDialog.Builder(Payment_activity.this);
//                        View view = getLayoutInflater().inflate(R.layout.alert_success, null);
//                        builder.setView(view);
//                        Button button = view.findViewById(R.id.ok);
//                        AlertDialog b = builder.create();
//                        b.show();
//                        button.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                PaymentTpyeRecyclerAdapter.cash="";
//                                startActivity(new Intent(Payment_activity.this, MyOrders.class));
//                                finish();
//                            }
//                        });

                    } else if (type.equals("payu")) {
                        PaymentTpyeRecyclerAdapter.cash = "";
//                        cash=cashonDeliveryResponse.cash;
                        String web_url = placeOrderDataResponse.paymentUrl;
                        Intent intent = new Intent(Payment_activity.this, Payu_WebView.class);
                        intent.putExtra("cash_key", web_url);
                        startActivity(intent);
                    }
                } else if (placeOrderResponse.status == 0) {
                    progressDialog.dismiss();
                    Toast.makeText(Payment_activity.this, placeOrderResponse.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PlaceOrderResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Payment_activity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }




//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == 100) {
//            if (resultCode == Activity.RESULT_OK) {
//
//                Daddress = data.getStringExtra("Default");
//                address_id = data.getStringExtra("Address_id");
//                addres = data.getStringExtra("Address");
//                f_name = data.getStringExtra("Fname");
//                titlee = data.getStringExtra("Title");
//                mobilee = data.getStringExtra("Mobile");
//                l_name = data.getStringExtra("Lname");
//                emaill = data.getStringExtra("Email");
//                address1 = data.getStringExtra("Address1");
//                Choose = data.getStringExtra("choose");
//
//                pselect_address.setVisibility(View.GONE);
//                linear_address.setVisibility(View.VISIBLE);
//                txtUserName.setText(f_name);
//                txtmobile.setText("Mobile : " + mobilee);
//                txtemail.setText("Email : " + emaill);
//                txtAddress.setText(addres + address1 + "," + area + "," + city);
//
//            }
//
//        }
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(Payment_activity.this,CartActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }
}
